module.exports = {
  i18n: {
    defaultLocale: 'ru',
    locales: ['en-US', 'ru'],
  },
};
