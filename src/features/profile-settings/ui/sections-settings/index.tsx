import {
  Box, Typography, Container, Paper,
} from '@material-ui/core';

import { IUseProfileSettingsReturn, useProfileSettings } from 'features/profile-settings/libs/useProfileSettings';
import { useTranslation } from 'next-i18next';
import { useCallback, FC, memo } from 'react';

import { updateCurrentUser } from 'shared/api/user';

import { EditableText } from 'shared/ui/uikit/editable-text';
import { LoaderFullScreen } from 'shared/ui/uikit/loader-fullscreen';

export interface ISectionSettings {
  profileSettings: IUseProfileSettingsReturn,
}

export const SectionSettings: FC<ISectionSettings> = memo(({ profileSettings }) => {
  const { t } = useTranslation();

  const updateFieldHandler = useCallback((name: string, value: string) => {
    profileSettings.updateProfile({
      [name]: value,
    });
  }, []);

  if (!profileSettings.dirty && profileSettings.loaded) {
    return <LoaderFullScreen />;
  }

  if (!profileSettings.profile) return <div>no profile</div>;

  return (
    <>
      <Typography variant="h4">
        { t('common:Settings') }
      </Typography>
      <Box mt={4}>
        <Paper>
          <Box p={2}>
            <EditableText label={t('common:First name')} name="firstName" value={profileSettings.profile.firstName} onSave={updateFieldHandler} loading={!profileSettings.loaded} />
            <EditableText label={t('common:Last name')} name="lastName" value={profileSettings.profile.lastName} onSave={updateFieldHandler} loading={!profileSettings.loaded} />
            <EditableText label={t('common:Email')} name="email" value={profileSettings.profile.email} onSave={() => {}} disabled />
            <EditableText label={t('common:Phone')} name="phone" value={profileSettings.profile.phone} onSave={() => {}} disabled />
          </Box>
        </Paper>
      </Box>
    </>
  );
});
