import { Typography, Box } from '@material-ui/core';
import { IBookingHistoryGroup } from 'entities/bookings/libs/types';
import { BookingsHistory } from 'entities/bookings/ui/bookings-history';
import { FC, memo } from 'react';
import { useTranslation } from 'react-i18next';
import { IUseDataFetcherResult } from 'shared/hooks/useDataFetcher';

export interface ISectionHistory {
  bookingsHistory: IUseDataFetcherResult<IBookingHistoryGroup[]>
}

export const SectionHistory: FC<ISectionHistory> = memo(({ bookingsHistory }) => {
  const { t } = useTranslation();

  if (!bookingsHistory.dirty && !bookingsHistory.loaded) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <Typography variant="h4">
        { t('common:Bookings history') }
      </Typography>
      <Box mt={4}>
        <BookingsHistory bookingsHistory={bookingsHistory.data} />
      </Box>
    </>
  );
});
