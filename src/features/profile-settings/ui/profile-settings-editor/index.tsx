/* eslint-disable max-len */
import {
  Avatar, Box, Typography, Container, Paper,
} from '@material-ui/core';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import {
  Edit, Group, History, Settings,
} from '@material-ui/icons';
import { BookingsHistory } from 'entities/bookings/ui/bookings-history';

import { UserCard } from 'entities/user/ui/user-card';
import { useBookingsHistory } from 'features/bookings-history/libs/hooks/useBookingsHistory';
import { useProfileSettings } from 'features/profile-settings/libs/useProfileSettings';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import {
  useEffect, useCallback, useState, useMemo,
} from 'react';

import { useMobile } from 'shared/hooks/useMobile';
import { EditableText } from 'shared/ui/uikit/editable-text';
import { LoaderFullScreen } from 'shared/ui/uikit/loader-fullscreen';
import { TabsContainer } from 'shared/ui/uikit/tabs-container';
import { SectionHistory } from '../section-history';
import { SectionPartner } from '../section-partners';
import { SectionSettings } from '../sections-settings';

const useStyles = makeStyles((theme: Theme) => createStyles({
  paralax: {
    width: '100%',
    height: ({ isMobile }: { isMobile: boolean }) => (isMobile ? 150 : 250),
    backgroundImage: 'linear-gradient(251deg, #161c24, #212b36)',
    transition: '1s',
    '&:hover': {
      backgroundPosition: 'right center',
    },
  },
  avatar: {
    width: 150,
    height: 150,
    border: '4px solid #fff',
  },
  info: {
    margin: 'auto',
    position: 'relative',
    top: -75,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  setting: {

  },
  bookings: {
    marginTop: theme.spacing(4),
  },
}));

export const ProfileSettingsEditor = () => {
  const { t } = useTranslation();

  const { isMobile } = useMobile();
  const router = useRouter();
  const { section } = router.query;
  const classes = useStyles({ isMobile });

  const profileSettings = useProfileSettings();
  const bookingsHistory = useBookingsHistory();

  const handleChangeTab = useCallback((event, tab: number) => {
    router.push({
      query: {
        section: tabs[tab].value,
      },
    });
  }, []);

  const tabs = useMemo(() => ([
    {
      label: t('common:Settings'),
      value: 'settings',
      icon: <Settings />,
      Component: <SectionSettings profileSettings={profileSettings} />,
    },
    {
      label: t('common:History'),
      value: 'history',
      icon: <History />,
      Component: <SectionHistory bookingsHistory={bookingsHistory} />,
    },
    {
      label: t('common:Partners'),
      value: 'partners',
      icon: <Group />,
      Component: <SectionPartner />,
    },
  ]), [t, bookingsHistory, profileSettings]);

  const currentTab = useMemo(() => tabs.findIndex((tab) => tab.value === section), [section]);
  const activeTab = currentTab >= 0 ? currentTab : 0;

  if (!profileSettings.dirty && profileSettings.loaded) {
    return <LoaderFullScreen />;
  }

  if (!profileSettings.profile) return <div>no profile</div>;

  return (
    <div>
      <div className={classes.paralax} />
      <div className={classes.info}>
        <Avatar className={classes.avatar} src={profileSettings.profile.photo} />
        <Box mt={2}>
          <Typography variant="h4" component="h2">
            { `${profileSettings.profile.firstName} ${profileSettings.profile.lastName}` }
          </Typography>
        </Box>
      </div>
      <Container className={classes.setting} maxWidth="md">
        <TabsContainer
          ariaLabel="business profile tabs"
          data={tabs}
          value={activeTab}
          onChange={handleChangeTab}
          fullWidth
        />

        <Box mt={4}>
          {
              tabs[activeTab]?.Component
            }
        </Box>
      </Container>
    </div>
  );
};
