import { Typography, Box } from '@material-ui/core';
import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'shared/ui/uikit/link';

export const SectionPartner = memo(() => {
  const { t } = useTranslation();

  return (
    <>
      <Typography variant="h4">
        { t('common:Management') }
      </Typography>
      <Box mt={4}>
        <Link href={{
          pathname: 'partner/management/businesses',
        }}
        >
          Перейти в панель управления
        </Link>
      </Box>
    </>
  );
});
