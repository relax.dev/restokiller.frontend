import { IUser } from 'entities/user/libs/types';
import { useSnackbar } from 'notistack';
import { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { updateCurrentUser } from 'shared/api/user';

import { ProfileContext } from 'shared/providers/profile-provider';

export interface IUseProfileSettingsReturn {
  profile: IUser | null;
  loaded: boolean;
  dirty: boolean;
  error: string | null;
  fetchProfile: () => void;
  updateProfile: (data: Partial<IUser>) => void;
  uploadPhoto: (photo: File) => void;
}

export const useProfileSettings = (): IUseProfileSettingsReturn => {
  const profile = useContext(ProfileContext);
  const { t } = useTranslation();

  const { enqueueSnackbar } = useSnackbar();

  const updateProfile = (data: Partial<IUser>) => {
    updateCurrentUser(data).then(() => {
      profile.refresh();
      enqueueSnackbar(t('common:Updated'), { variant: 'success' });
    }).catch(() => {
      alert('2');
    });
  };

  const uploadPhoto = (photo: File) => {

  };

  return {
    profile: profile.data,
    loaded: profile.loaded,
    dirty: profile.dirty,
    error: profile.error,
    fetchProfile: profile.refresh,
    updateProfile,
    uploadPhoto,
  };
};
