import {
  useEffect,
  FC,
} from 'react';
import { Container, Typography, Box } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useAppSelector } from 'shared/hooks/useRedux';
import { BusinessesGrid } from 'entities/business/ui/businesses-grid';
import { businessesSelector, fetchOwnerBusinesses } from '../../businesses-management.slice';

export interface IBusinessesManagement {
  lng?: string,
}

export const BusinessesManagement: FC<IBusinessesManagement> = () => {
  const businesses = useAppSelector(businessesSelector);
  const dispatch = useDispatch();
  const { t } = useTranslation();

  useEffect(() => {
    dispatch(fetchOwnerBusinesses());
  }, []);

  return (
    <Container maxWidth="lg">
      <Typography variant="h4">
        { t('common:Your businesses') }
      </Typography>
      <Box mt={2}>
        <BusinessesGrid businesses={businesses} isManagement />
      </Box>
    </Container>
  );
};
