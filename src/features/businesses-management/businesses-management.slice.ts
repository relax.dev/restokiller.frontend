import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { getOwnerBusinesses } from 'shared/api/businesses';
import { addFetchableDataCase } from 'shared/helpers/store';
import { RootState } from 'shared/modules/store';
import { IBusiness } from 'entities/business/libs/types';
import { FetchableData } from 'shared/types/store';

export interface IBusinessesManagementState {
  businesses: FetchableData<IBusiness[]>
}

const initialState: IBusinessesManagementState = {
  businesses: {
    data: [],
    loaded: false,
    error: null,
  },
};

export const fetchOwnerBusinesses = createAsyncThunk(
  'businessesManagementSlice/fetchAllTables',
  async () => {
    const response = await getOwnerBusinesses();
    return response.data;
  },
);

export const businessesManagementSlice = createSlice({
  name: 'tables-management',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    addFetchableDataCase(builder, fetchOwnerBusinesses, 'businesses');
  },
});

export const businessesSelector = (state: RootState) => state.businessesManagement.businesses;

export default businessesManagementSlice.reducer;
