import { IBookingHistoryGroup } from 'entities/bookings/libs/types';
import { getAllBookingsForCurrentUser } from 'shared/api/bookings';
import { useDataFetcher } from 'shared/hooks/useDataFetcher';

export const useBookingsHistory = () => {
  const bookingsHistory = useDataFetcher<IBookingHistoryGroup[]>({
    api: getAllBookingsForCurrentUser,
    defaultData: [],
  });

  return bookingsHistory;
};
