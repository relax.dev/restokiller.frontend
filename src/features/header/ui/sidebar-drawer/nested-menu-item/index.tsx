import React, {
  FC, useState, useCallback,
} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {
  Collapse,
} from '@material-ui/core';
import {
  ExpandLess, ExpandMore,
} from '@material-ui/icons';
import { IMenuItemData } from '../menu-items';
import { MenuItem } from '../menu-item';

export interface INestedMenuItem {
  data: IMenuItemData;
}

export const NestedMenuItem: FC<INestedMenuItem> = ({ data }) => {
  const [open, setOpen] = useState(true);

  const toggleOpen = useCallback(() => {
    setOpen(!open);
  }, [open]);
  if (!data.children) return null;

  return (
    <>
      <ListItem button onClick={toggleOpen}>
        <ListItemIcon>
          { data.icon }
        </ListItemIcon>
        <ListItemText primary={data.label} />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          {
            data.children.map((item) => <MenuItem data={item} nested />)
        }
        </List>
      </Collapse>
    </>
  );
};
