import React, {
  useEffect,
} from 'react';
import clsx from 'clsx';
import {
  createStyles,
  makeStyles,
  useTheme,
  Theme,
} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import {
  ListSubheader,
} from '@material-ui/core';
import { useMobile } from 'shared/hooks/useMobile';
import { Header } from '../header';
import { menuItems } from './menu-items';
import { NestedMenuItem } from './nested-menu-item';
import { MenuItem } from './menu-item';

const drawerWidth = 300;

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    background: theme.palette.background.default,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(4, 0, 0, 0),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: 0,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: drawerWidth,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

export const SidebarDrawer = ({ children }: any) => {
  const classes = useStyles();
  const theme = useTheme();
  const { isMobile } = useMobile();

  const [open, setOpen] = React.useState(true);

  useEffect(() => {
    if (isMobile) {
      handleDrawerClose();
    }
  }, [isMobile]);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Header
        drawerOpen={isMobile ? false : open}
        handleDrawerOpen={handleDrawerOpen}
        handleDrawerClose={handleDrawerClose}
        className={clsx(classes.appBar, {
          [classes.appBarShift]: !isMobile && open,
        })}
      />
      <Drawer
        className={classes.drawer}
        variant={!isMobile ? 'persistent' : 'temporary'}
        anchor="left"
        open={open}
        onClose={handleDrawerClose}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>

        <List
          component="nav"
          aria-labelledby="nested-list-subheader"
          subheader={(
            <ListSubheader component="div" id="nested-list-subheader">
              Панель управления
            </ListSubheader>
          )}
        >
          {
            menuItems.map((item) => {
              const isNested = Boolean(item.children);

              return isNested ? <NestedMenuItem data={item} /> : <MenuItem data={item} />;
            })
          }
        </List>

      </Drawer>
      <main className={clsx(classes.content, {
        [classes.contentShift]: !isMobile && open,
      })}
      >
        {children}
      </main>
    </div>
  );
};
