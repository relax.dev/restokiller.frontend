import React, {
  FC,
} from 'react';
import clsx from 'clsx';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { useRouter } from 'next/router';
import Link from 'next/link';
import {
  createStyles,
  makeStyles,
  Theme,
} from '@material-ui/core/styles';
import { IMenuItemData } from '../menu-items';

export interface IMenuItem {
  nested?: boolean;
  data: IMenuItemData;
}

const useStyles = makeStyles((theme: Theme) => createStyles({
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

export const MenuItem: FC<IMenuItem> = ({ nested = false, data }) => {
  const classes = useStyles();

  const router = useRouter();

  console.log(router);
  
  return (
    <Link href={data.url || '/'}>
      <ListItem
        button
        selected={router.pathname === data.url}
        className={clsx({ [classes.nested]: nested })}
      >
        <ListItemIcon>
          { data.icon }
        </ListItemIcon>
        <ListItemText primary={data.label} />
      </ListItem>
    </Link>
  );
};
