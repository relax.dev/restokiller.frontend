import {
  Book,
  Business,
  Equalizer,
  ExpandLess,
  ExpandMore,
  Help,
  Menu,
  MenuBook,
  PlaylistAdd,
} from '@material-ui/icons';

import { ReactNode } from 'react';

export interface IMenuItemData {
  label: string;
  icon: ReactNode;
  url?: string;
  children?: IMenuItemData[];
}

export const menuItems: IMenuItemData[] = [
  {
    label: 'Заведения',
    url: '/partner/management/businesses',
    icon: <Business />,
  },
  {
    label: 'Регистрация заведения',
    url: '/partner/registration',
    icon: <PlaylistAdd />,
  },
  {
    label: 'Гостевая книга',
    icon: <MenuBook />,
    children: [
      {
        label: 'Бронирования',
        url: '/partner/bookings',
        icon: <Book />,
      },
      {
        label: 'Статистика',
        url: '/partner/management',
        icon: <Equalizer />,
      },
    ],
  },
  {
    label: 'Помощь',
    url: '/',
    icon: <Help />,
  },
];
