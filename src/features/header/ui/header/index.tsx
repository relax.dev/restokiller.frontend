import {
  AppBar,
  Avatar,
  Box,
  IconButton,
  Toolbar,
  Typography,
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';

import Link from 'next/link';
import clsx from 'clsx';
import { ChevronLeft } from '@material-ui/icons';
import { HeaderProfile } from 'entities/header/ui/header-profile';
import { If } from 'shared/ui/uikit/if';
import { useMobile } from 'shared/hooks/useMobile';
import { HeaderCountry } from '../../../countries/ui/header-country';

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    maxHeight: '64px',
  },
  title: {
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
  },
  content: {
    flexGrow: 1,
    display: 'flex',
  },
  logoText: {
    fontWeight: 'bold',
    fontSize: '18px',
    marginLeft: theme.spacing(1),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
}));

export const Header = ({
  children,
  className,
  drawerOpen,
  handleDrawerOpen,
  handleDrawerClose,
  ...props
}: any) => {
  const classes = useStyles();
  const { isMobile } = useMobile();

  return (
    <>
      <AppBar className={`${classes.root} ${className}`} position="fixed">
        <Toolbar>
          {!drawerOpen && handleDrawerOpen && (
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton)}
            >
              <MenuIcon />
            </IconButton>
          )}

          <Link href="/">
            <div className={classes.title}>
              <Avatar src="https://sun1-15.userapi.com/s/v1/ig2/MfFqFgtKEMp4lDNFr6LPznEfKC4-evn7xAYUqXvhgoCypF4QFX9FWYMPDtzGX2Ppp2p5fedKAo9cBI--NGCsSpuy.jpg?size=200x0&quality=96&crop=69,64,378,378&ava=1" />
              <If condition={!isMobile}>
                <Typography variant="h5" className={classes.logoText}>
                  Relax.dev
                </Typography>

              </If>
            </div>
          </Link>

          <div className={classes.content}>
            <Box ml={3} display="flex">
              <HeaderCountry />
            </Box>
          </div>

          <HeaderProfile />
        </Toolbar>
      </AppBar>
      <Toolbar />
    </>
  );
};
