import React, { useState } from 'react';
import {
  Box,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Container,
  Typography,
} from '@material-ui/core';

import { Form, Formik } from 'formik';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { boolean } from 'yup/lib/locale';
import { useSnackbar } from 'notistack';
import { FullScreenDialog } from 'shared/ui/uikit/dialog-fullscreen';
import { Button } from 'shared/ui/uikit/button';
import { FormikTextField } from 'shared/ui/uikit/formik-text-field';
import { ITablesFilters } from 'shared/api/tables';
import { createBooking, ICreateBooking } from 'shared/api/bookings';

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    marginTop: theme.spacing(4),
  },
}));

export interface IBookingTableDialog {
  open: boolean;
  onClose: () => void;
  tableId: string;
  filters: ITablesFilters;
}

export const BookingTableDialog = ({
  open,
  onClose,
  tableId,
  filters,
}: IBookingTableDialog) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const { enqueueSnackbar } = useSnackbar();

  const formikProps = {
    initialValues: {
      firstname: 'Рома',
      phone: '79297660409',
      email: 'web.rnskv@gmail.com',
      description: 'Проверка системы бронирования',
    },
    onSubmit: (values: any) => {
      setLoading(true);

      const data: ICreateBooking = {
        ...values,
        startDate: filters.startDate,
        endDate: filters.endDate,
        persons: filters.seats,
        table: tableId,
      };

      createBooking(data)
        .then((response) => {
          enqueueSnackbar(t('common:Booking successfully'), { variant: 'success' });
          onClose();
        })
        .catch((err) => {
          setError('error');
        })
        .finally(() => {
          setLoading(false);
        });
    },
  };
  return (
    <FullScreenDialog title="Booking table" open={open} onClose={onClose}>
      <Container maxWidth="sm" className={classes.root}>
        <Card>
          <CardActionArea>
            <CardMedia
              component="img"
              alt="Restourant preview"
              height="140"
              image="https://find-rest.ru/upload/iblock/f4b/f4bdd827520ba67d0bd71dfae82881eb.jpg"
              title="Kontakt bar"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                Форма бронирования
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                Бронируя столик вы подтверждаете бла бла бла, и после чего
                заполните форму и подтвердите бронь
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>

        <Box pt={2} pb={4}>
          <Formik {...formikProps}>
            <Form>
              <FormikTextField
                fullWidth
                name="firstname"
                label={t('common:Firstname')}
              />
              <FormikTextField
                placeholder="+79005003344"
                fullWidth
                type="phone"
                name="phone"
                label={t('common:Phone number')}
              />
              <FormikTextField
                placeholder="example@gmail.com"
                fullWidth
                type="email"
                name="email"
                label={t('common:Email')}
              />

              <FormikTextField
                multiline
                rows={4}
                fullWidth
                name="description"
                label={t('common:Description')}
              />

              <Box mt={2}>
                <Button type="submit" fullWidth size="large" loading={loading}>
                  Подтвердить бронирование
                </Button>
              </Box>
            </Form>
          </Formik>
        </Box>
      </Container>
    </FullScreenDialog>
  );
};
