import {
  FC, useEffect, useState, useCallback,
  useMemo,
} from 'react';
import {
  Box,
  Grid,
  TextField,
  Divider,
  MenuItem,
  InputAdornment,
} from '@material-ui/core';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

import { Clock, KeyboardDatePicker } from '@material-ui/pickers';
import {
  AccessTime, FilterList, People, Timer,
} from '@material-ui/icons';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import Search from '@material-ui/icons/Search';
import { ITablesFilters } from 'shared/api/tables';
import { TabsContainer } from 'shared/ui/uikit/tabs-container';
import { MobileFolder } from 'shared/ui/uikit/mobile-folder';
import { useMobile } from 'shared/hooks/useMobile';
import { Button } from 'shared/ui/uikit/button';
import { IPlace } from 'entities/place/libs/types';
import {
  generateTimesArrayFromMinutes,
  getCurrentDay,
  getTimesBetweenHours,
  getMinutesCountBetweenTimes,
  localizedMoment,
} from 'shared/helpers/time';
import { IBusiness } from 'entities/business/libs/types';
import { FetchableData } from 'shared/types/store';
import { useTableFiltes } from 'features/business/libs/hooks/useTablesFilters';

interface ITableFiltersPanel {
  handleChangeFilters: (filters: ITablesFilters) => void;
  filters: ITablesFilters;
  places: FetchableData<IPlace[]>,
  business: FetchableData<IBusiness>;
}

const useStyles = makeStyles((theme: Theme) => createStyles({
  button: {
    height: theme.spacing(7),
  },
}));

export const TableFiltersPanel: FC<ITableFiltersPanel> = ({
  handleChangeFilters,
  filters,
  places,
  business,
}) => {
  // Ммм, хуита все что ниже

  const {
    handlePlaceChange,
    handleDateChange,
    handleDurationChange,
    handleSeatsChange,
    handleTimeChange,
    selectedPlace,
    selectedDate,
    time,
    duration,
    hours,
    bookingHours,
    isValidFiltersForBooking,
    seatsItems,
  } = useTableFiltes({
    handleChangeFilters,
    places,
    business,
    filters,
  });
  const { t } = useTranslation();
  const { isMobile } = useMobile();
  const [openedMobileFiltes, setOpenedMobileFilters] = useState(false);

  const classes = useStyles();

  const tabsData = places.data.map(({ name }: IPlace) => ({ label: name }));

  const onOpenMobileFilters = useCallback(() => {
    setOpenedMobileFilters(true);
  }, []);

  const onCloseMobileFilters = useCallback(() => {
    setOpenedMobileFilters(false);
  }, []);

  return (
    <>
      <MobileFolder
        title={t('common:filters')}
        icon={<FilterList />}
        open={openedMobileFiltes}
        onClose={onCloseMobileFilters}
        onOpen={onOpenMobileFilters}
        showBadge={!isValidFiltersForBooking}
      >
        <Grid container justify="flex-start" spacing={2} alignItems="center">
          <Grid item xs={12} md={4} lg={5}>
            <KeyboardDatePicker
              fullWidth
              id="date-picker-dialog"
              label="Когда вас ждать?"
              format="DD/MM (dddd)"
              value={selectedDate}
              onChange={handleDateChange}
              error={!selectedDate}
              margin="none"
              disablePast
              required
              helperText=""
              KeyboardButtonProps={{
                'aria-label': 'change date',
              }}
            />
          </Grid>

          <Grid xs={12} item md={3} lg={2}>
            <TextField
              margin="none"
              fullWidth
              label="Время"
              select
              required
              value={time}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <AccessTime />
                  </InputAdornment>
                ),
              }}
              error={!time}
              onChange={handleTimeChange}
              InputLabelProps={{
                shrink: true,
              }}
            >
              {
                hours.map((dateTime, index) => (
                  <MenuItem key={dateTime} value={dateTime} disabled={index === hours.length - 1}>
                    { dateTime }
                  </MenuItem>
                ))
              }
            </TextField>
          </Grid>

          <Grid xs={12} item md={3} lg={2}>
            <TextField
              margin="none"
              fullWidth
              label="Длительность"
              value={duration}
              required
              onChange={handleDurationChange}
              select
              error={!duration}
              InputLabelProps={{
                shrink: true,
              }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Timer />
                  </InputAdornment>
                ),
              }}
            >
              {
                bookingHours.map((dateTime) => (
                  <MenuItem key={dateTime} value={dateTime}>
                    { dateTime }
                  </MenuItem>
                ))
              }
            </TextField>
          </Grid>

          <Grid xs={12} item md={3} lg={3}>
            <TextField
              margin="none"
              fullWidth
              label="Кол-во гостей"
              required
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <People />
                  </InputAdornment>
                ),
              }}
              InputLabelProps={{
                shrink: true,
              }}
              value={filters.seats}
              error={!filters.seats || filters.seats <= 0}
              onChange={
                handleSeatsChange
              }
              select
            >
              {
                seatsItems.map((value) => <MenuItem value={value}>{value}</MenuItem>)
              }
            </TextField>
          </Grid>

          { isMobile && (
          <Grid item xs={12} md={3} lg={12}>
            <Button
              startIcon={<Search />}
              fullWidth
              size="large"
              className={classes.button}
              onClick={onCloseMobileFilters}
            >
              { t('common:Show variants')}
            </Button>
          </Grid>
          ) }
        </Grid>
      </MobileFolder>
      <Box mt={isMobile ? 0 : 2}>
        <TabsContainer
          onChange={handlePlaceChange}
          value={selectedPlace}
          ariaLabel="Places"
          data={tabsData}
        />
        <Divider />
      </Box>
    </>
  );
};
