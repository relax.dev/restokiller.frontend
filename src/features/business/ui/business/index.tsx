import { Box, Button, Container } from '@material-ui/core';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo } from 'react';
import { useAppDispatch, useAppSelector } from 'shared/hooks/useRedux';
import { BusinessMediaHeader } from 'entities/business/ui/business-media-header';
import { TablesCardsContainer } from 'entities/table/ui/tables-cards-container';
import { ITablesFilters } from 'shared/api/tables';
import { useMobile } from 'shared/hooks/useMobile';
import { LoaderFullScreen } from 'shared/ui/uikit/loader-fullscreen';
import {
  businessFiltersSelector,
  businessTablesSelector,
  setBusinessFilters,
  fetchAllTables,
  businessPlacesSelector,
  businessBodySelector,
  getInitialBusinessData,
  resetBusiness,
} from '../../business.slice';
import { TableFiltersPanel } from '../tables-filters-panel';

export const BusinessFeature = () => {
  const dispatch = useAppDispatch();
  const router = useRouter();
  const businessFilters = useAppSelector(businessFiltersSelector);
  const businessTables = useAppSelector(businessTablesSelector);
  const businessPlaces = useAppSelector(businessPlacesSelector);
  const businessBody = useAppSelector(businessBodySelector);

  const { id } = router.query;

  const { isMobile } = useMobile();

  useEffect(() => {
    getInitialBusinessData(id as any, dispatch);
    return () => {
      dispatch(resetBusiness());
    };
  }, [id]);

  console.log(businessTables);

  useEffect(() => {
    if (!businessFilters?.places?.[0]) return;

    dispatch(fetchAllTables(businessFilters));
  }, [businessFilters]);

  const handleChangeFilters = useCallback(
    (filters: ITablesFilters) => {
      dispatch(setBusinessFilters({ ...filters }));
    },
    [],
  );

  if (!businessBody.loaded
      || !businessTables.dirty
      || !businessPlaces.dirty
  ) {
    return <LoaderFullScreen />;
  }

  return (
    <>
      <BusinessMediaHeader business={businessBody} />
      <Container maxWidth="md">
        <Box mt={isMobile ? 2 : 4} mb={4}>
          <TableFiltersPanel
            handleChangeFilters={handleChangeFilters}
            filters={businessFilters}
            places={businessPlaces}
            business={businessBody}
          />
        </Box>
        <TablesCardsContainer filters={businessFilters} tables={businessTables} />
      </Container>
    </>
  );
};
