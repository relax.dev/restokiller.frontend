import { IBusiness } from 'entities/business/libs/types';
import { IPlace } from 'entities/place/libs/types';
import moment from 'moment';
import { useEffect, useMemo, useState } from 'react';
import { ITablesFilters } from 'shared/api/tables';
import {
  extractHoursAndMinutes,
  generateTimesArrayFromMinutes, getCurrentDay, getMinutesCountBetweenTimes, getTimesBetweenHours,
} from 'shared/helpers/time';
import { FetchableData } from 'shared/types/store';

export const useTableFiltes = ({
  handleChangeFilters, places, business, filters,
}: {
  handleChangeFilters: any;
  places: FetchableData<IPlace[]>;
  business: FetchableData<IBusiness>;
  filters: ITablesFilters;
}) => {
  const [selectedPlace, setSelectedPlace] = useState<number>(0);

  const [selectedDate, setSelectedDate] = useState<moment.Moment | null>(
    moment(),
  );

  const [time, setTime] = useState<string>('00:00');

  const [duration, setDuration] = useState<string>('00:00');

  const handlePlaceChange = (event: any, value: number) => {
    setSelectedPlace(value);
  };

  const handleDateChange = (date: any) => {
    setSelectedDate(date);
  };

  const handleTimeChange = (e: any) => {
    setTime(e.target.value);
  };

  const handleDurationChange = (e: any) => {
    setDuration(e.target.value);
  };

  const handleSeatsChange = (e: any) => {
    handleChangeFilters({
      seats: e.target.value,
    });
  };

  const isValidFiltersForBooking = useMemo(
    () => selectedDate && time && duration && filters.seats,
    [selectedDate, time, duration, filters.seats],
  );

  const seatsItems = new Array(10).fill('').map((value, index) => `${index + 1}`);

  const now = moment();
  const currentDay = getCurrentDay();

  const hours = getTimesBetweenHours({
    startTime: business.data.workingHours[currentDay].start,
    endTime: business.data.workingHours[currentDay].end,
    interval: {
      value: 30,
      type: 'minutes',
    },
    includePast: false,
  });

  const bookingMinutes = getMinutesCountBetweenTimes(time, hours[hours.length - 1]);

  const bookingHours = generateTimesArrayFromMinutes(bookingMinutes);

  const startWorkTime = business.data.workingHours[currentDay].start;

  useEffect(() => {
    const place = places.data[selectedPlace];

    if (!place) return;

    handleChangeFilters({
      places: [place.id],
    });
  }, [selectedPlace, places.data]);

  useEffect(() => {
    setTime(hours[0]);
  }, []);

  useEffect(() => {
    const value = bookingHours.find((hour) => hour === duration);
    if (!value) {
      setDuration('00:30');
    }
  }, [time]);

  useEffect(() => {
    if (!selectedDate || !time || !duration) {
      handleChangeFilters({
        startDate: undefined,
        endDate: undefined,
      });

      return;
    }

    const [durationHours, durationMinutes] = extractHoursAndMinutes(duration);
    const [timeHours] = extractHoursAndMinutes(time);
    const [startTimeHours] = extractHoursAndMinutes(startWorkTime);

    const startDate = moment(`${selectedDate?.toDate().toDateString()} ${time}`);

    if (timeHours < startTimeHours) {
      startDate.add(1, 'day');
    }

    const endDate = startDate.clone();

    endDate.add(durationHours, 'hours');
    endDate.add(durationMinutes, 'minutes');

    handleChangeFilters({
      startDate: startDate.toDate().toString(),
      endDate: endDate.toDate().toString(),
    });
  }, [duration, time, selectedDate]);

  return {
    handlePlaceChange,
    handleDateChange,
    handleDurationChange,
    handleSeatsChange,
    handleTimeChange,
    selectedPlace,
    selectedDate,
    time,
    duration,
    hours,
    bookingHours,
    isValidFiltersForBooking,
    seatsItems,
  };
};
