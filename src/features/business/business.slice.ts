import {
  createAsyncThunk, createSlice, PayloadAction, unwrapResult,
} from '@reduxjs/toolkit';
import { StringSchema } from 'yup';
import { FetchableData } from 'shared/types/store';
import { AppDispatch, RootState } from 'shared/modules/store';
import { addFetchableDataCase } from 'shared/helpers/store';
import { getBusinessById } from 'shared/api/businesses';
import { getAllPlaces, IPlacesFilters } from 'shared/api/places';
import { getAllTables, ITablesFilters } from 'shared/api/tables';

import { IBusiness, IWorkingHours } from 'entities/business/libs/types';
import { ITable } from 'entities/table/libs/types';
import { IPlace } from 'entities/place/libs/types';
import { defaultBusinessData } from 'entities/business/libs/data';

interface IBusinessState {
  body: FetchableData<IBusiness>;
  places: FetchableData<IPlace[]>;
  tables: FetchableData<ITable[]>;
  filters: ITablesFilters;
}

const initialState: IBusinessState = {
  body: {
    data: defaultBusinessData,
    loaded: false,
    error: null,
  },
  places: {
    data: [],
    loaded: false,
    error: null,
  },
  tables: {
    data: [],
    loaded: false,
    error: null,
  },
  filters: {},
};

export const fetchAllTables = createAsyncThunk(
  'business/fetchAllTables',
  async (filters: ITablesFilters) => {
    const response = await getAllTables({ filters });
    return response.data;
  },
);

export const fetchAllPlaces = createAsyncThunk(
  'business/fetchAllPlaces',
  async (filters: IPlacesFilters) => {
    const response = await getAllPlaces({ filters });
    return response.data;
  },
);

export const fetchBusinessById = createAsyncThunk(
  'business/fetchBusinessById',
  async (id: string) => {
    const response = await getBusinessById(id);
    return response.data;
  },
);

export const getInitialBusinessData = (id: string, dispatch: AppDispatch) => {
  if (!id) return;
  dispatch(fetchBusinessById(id))
    .then(unwrapResult)
    .then(() => dispatch(fetchAllPlaces({ businesses: [id] }))
      .then(unwrapResult)
      .then((places: any) => {
        const placesIds = places.map((place: any) => place.id);

        dispatch(setBusinessFilters({ places: [placesIds[0]] }));
      }))
    .catch((error: any) => {
      console.error('error', error);
    });
};

export const businessSlice = createSlice({
  name: 'business',
  initialState,
  reducers: {
    setBusinessFilters: (state, action: PayloadAction<ITablesFilters>) => {
      state.filters = { ...state.filters, ...action.payload };
    },

    resetBusinessFilters: (state) => {
      state.filters = initialState.filters;
    },

    resetBusiness: (state) => {
      state.tables = initialState.tables;
      state.body = initialState.body;
      state.places = initialState.places;
      state.filters.places = [];
    },
  },
  extraReducers: (builder) => {
    addFetchableDataCase(builder, fetchBusinessById, 'body');
    addFetchableDataCase(builder, fetchAllTables, 'tables');
    addFetchableDataCase(builder, fetchAllPlaces, 'places');
  },
});

export const { setBusinessFilters, resetBusinessFilters, resetBusiness } = businessSlice.actions;

export const businessFiltersSelector = (state: RootState) => state.business.filters;
export const businessTablesSelector = (state: RootState) => state.business.tables;
export const businessPlacesSelector = (state: RootState) => state.business.places;
export const businessBodySelector = (state: RootState) => state.business.body;

export default businessSlice.reducer;
