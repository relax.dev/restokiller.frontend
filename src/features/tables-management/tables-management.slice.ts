import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { IPlace } from 'entities/place/libs/types';
import { ITable } from 'entities/table/libs/types';
import { getAllPlaces, IPlacesFilters } from 'shared/api/places';
import { getAllTables, ITablesFilters } from 'shared/api/tables';
import { addFetchableDataCase } from 'shared/helpers/store';
import { RootState } from 'shared/modules/store';
import { FetchableData } from 'shared/types/store';

export interface ITablesManagementState {
  places: FetchableData<IPlace[]>
  tables: FetchableData<ITable[]>
}

const initialState: ITablesManagementState = {
  tables: {
    data: [],
    loaded: false,
    error: null,
  },
  places: {
    data: [],
    loaded: false,
    error: null,
  },
};

export const fetchAllTables = createAsyncThunk(
  'tablesManagementSlice/fetchAllTables',
  async (filters: ITablesFilters) => {
    const response = await getAllTables({ filters });
    return response.data;
  },
);

export const fetchAllPlaces = createAsyncThunk(
  'tablesManagementSlice/fetchAllPlaces',
  async (filters: IPlacesFilters) => {
    const response = await getAllPlaces({ filters });
    return response.data;
  },
);

export const tablesManagementSlice = createSlice({
  name: 'tables-management',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    addFetchableDataCase(builder, fetchAllPlaces, 'places');
    addFetchableDataCase(builder, fetchAllTables, 'tables');
  },
});

export const tablesSelector = (state: RootState) => state.tablesManagement.tables;
export const placesSelector = (state: RootState) => state.tablesManagement.places;

export default tablesManagementSlice.reducer;
