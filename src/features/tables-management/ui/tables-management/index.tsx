import {
  Typography, Box, Grid, Tabs, Tab, Tooltip, IconButton,
} from '@material-ui/core';
import { AddCircleOutlined, PlusOneOutlined } from '@material-ui/icons';
import { useTranslation } from 'next-i18next';
import {
  FC, useMemo, useEffect, useCallback, useState,
} from 'react';

import { useAppDispatch, useAppSelector } from 'shared/hooks/useRedux';
import { TabPanel } from 'shared/ui/uikit/tab-panel';
import { TabsContainer } from 'shared/ui/uikit/tabs-container';
import { CardButton } from 'shared/ui/uikit/card-button';

import { TableCard } from 'entities/table/ui/table-card';
import { TableCreateDialog } from 'entities/table/ui/table-create-dialog';
import { PlaceCreateDialog } from 'entities/place/ui/place-create-dialog';
import { IPlace } from 'entities/place/libs/types';
import { ITable } from 'entities/table/libs/types';

import {
  fetchAllPlaces, fetchAllTables, placesSelector, tablesSelector,
} from '../../tables-management.slice';

export interface ITablesManagement {
  businessId: string;
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export const TablesManagement: FC<ITablesManagement> = ({ businessId }) => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const [value, setValue] = useState(0);

  const handleChange = (event: any, newValue: number) => {
    setValue(newValue);
  };

  const [newTableOpened, setNewTableOpened] = useState(false);
  const [newPlaceOpened, setNewPlaceOpened] = useState(false);

  const [seletedPlace, setSeletedPlace] = useState <IPlace | null>(null);

  const closeNewTable = useCallback(() => {
    setNewTableOpened(false);
    setSeletedPlace(null);
  }, []);

  const openNewTable = useCallback((place: IPlace) => () => {
    setNewTableOpened(true);
    setSeletedPlace(place);
  }, []);

  const closeNewPlace = useCallback(() => {
    setNewPlaceOpened(false);
  }, []);

  const openNewPlace = useCallback(() => {
    setNewPlaceOpened(true);
  }, []);

  const places = useAppSelector(placesSelector);
  const tables = useAppSelector(tablesSelector);

  const placesIds = useMemo(
    (): string[] => places.data.map(({ id }: IPlace) => id),
    [places.data],
  );

  const updateTables = useCallback(() => {
    dispatch(fetchAllTables({ places: placesIds }));
  }, [placesIds]);

  const updatePlaces = useCallback(() => {
    dispatch(fetchAllPlaces({ businesses: [businessId] }));
  }, [businessId]);

  useEffect(() => {
    updatePlaces();
  }, []);

  useEffect(() => {
    updateTables();
  }, [places.data]);

  const remappedTables: any = {};

  places.data.forEach((place: IPlace) => {
    remappedTables[place.id] = tables.data
      .filter(({ place: tablePlace }: ITable) => tablePlace.id === place.id);
  });

  const tabsData = places.data.map(({ name }: IPlace) => ({ label: name }));

  return (
    <>
      <Typography gutterBottom variant="h4">
        { t('common:Places') }
      </Typography>
      <Box display="flex" alignItems="center">
        <TabsContainer
          onChange={handleChange}
          data={tabsData}
          value={value}
          ariaLabel="Places selector"
          color="secondary"
        />

        <Box ml={1}>
          <Tooltip arrow title={t('common:Add place') || ''}>
            <div>
              <IconButton onClick={openNewPlace}>
                <AddCircleOutlined />
              </IconButton>
            </div>
          </Tooltip>
        </Box>
      </Box>

      {
        places.data.map((place: IPlace, index: number) => (
          <TabPanel key={`${place.id}`} value={index} index={value}>
            <Box mt={4}>
              <Box mt={2}>
                <Grid container spacing={4}>

                  <Grid item xs={12} md={6} lg={4}>
                    <CardButton disabled={!tables.loaded} onClick={openNewTable(place)} />
                  </Grid>
                  {
                    remappedTables[place.id].map((table: ITable) => (
                      <Grid key={table.id} item xs={12} md={6} lg={4}>
                        <TableCard isManagement data={table} button={{ label: t('common:edit'), callback: () => { alert('edit'); } }} />
                      </Grid>
                    ))
                  }
                </Grid>
              </Box>
            </Box>
          </TabPanel>
        ))
      }

      {
        seletedPlace && (
          <TableCreateDialog
            open={newTableOpened}
            onClose={closeNewTable}
            place={seletedPlace}
            onSuccess={updateTables}
          />
        )
      }

      <PlaceCreateDialog
        businessId={businessId}
        onClose={closeNewPlace}
        open={newPlaceOpened}
        onSuccess={updatePlaces}
      />
    </>
  );
};
