import { Typography, Divider } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    marginTop: theme.spacing(4),
  },
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    minHeight: 250,
    background: theme.palette.background.default,
  },
}));

export const Footer = () => {
  const classes = useStyles();

  return (
    <footer className={classes.root}>
      <Divider />

      <div className={classes.container}>
        <Typography variant="body2" color="textSecondary">
          FOOOTER COPYRIGHT AND BLA BLA FUN
        </Typography>
      </div>
    </footer>
  );
};
