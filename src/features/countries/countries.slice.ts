import {
  createAsyncThunk, createSlice, PayloadAction, unwrapResult,
} from '@reduxjs/toolkit';
import Cookies from 'js-cookie';
import { FetchableData } from 'shared/types/store';
import { RootState, store } from 'shared/modules/store';
import { addFetchableDataCase } from 'shared/helpers/store';
import { getCitiesByIso3, getNearestCities } from 'shared/api/countries';

interface IState {
  name: string,
  state_code: string,
}

interface ICountry {
  name: string;
  iso3: string;
}

interface ICity {
  state: IState;
  country: ICountry;
  name: string;
}

interface INearestCity {
  name: string,
  distantion: number;
}

interface ICountriesState {
  currentCountry: ICountry,
  currentCity: string | null,

  cities: FetchableData<ICity[]>
  nearestCities: FetchableData<INearestCity[]>
}

const initialState: ICountriesState = {
  currentCountry: {
    name: Cookies.get('currentCountryName') || 'Russia',
    iso3: Cookies.get('currentCountryISO3') || 'RUS',
  },
  currentCity: Cookies.get('currentCity') || null,
  cities: {
    loaded: false,
    error: null,
    data: [],
  },
  nearestCities: {
    loaded: false,
    error: null,
    data: [],
  },
};

export const fetchCitiesByIso3 = createAsyncThunk(
  'countries/fetchCitiesByIso3',
  async (iso3: string) => {
    const response = await getCitiesByIso3(iso3);
    return response.data;
  },
);

export const fetchNearestCities = createAsyncThunk(
  'countries/fetchNearestCities',
  async ({ latitude, longitude }: { latitude: string, longitude: string }) => {
    const response = await getNearestCities(latitude, longitude);
    return response.data;
  },
);

export const countriesSlice = createSlice({
  name: 'countries',
  initialState,
  reducers: {
    setCurrentCity: (state, action: PayloadAction<string>) => {
      state.currentCity = action.payload;
    },
  },
  extraReducers: (builder) => {
    addFetchableDataCase(builder, fetchCitiesByIso3, 'cities');
    addFetchableDataCase(builder, fetchNearestCities, 'nearestCities');
  },
});

export const { setCurrentCity } = countriesSlice.actions;

export const currentCitySelector = (state: RootState) => state.countries.currentCity;
export const currentCountrySelector = (state: RootState) => state.countries.currentCountry;

export const citiesSelector = (state: RootState) => state.countries.cities;
export const nearestCitiesSelector = (state: RootState) => state.countries.nearestCities;

export default countriesSlice.reducer;
