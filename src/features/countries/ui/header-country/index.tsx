import {
  useCallback, useEffect, useRef, useState,
} from 'react';

import {
  Backdrop,
  Box,
  Grid,
  LinearProgress,
  Menu,
  MenuItem,
  Typography,
} from '@material-ui/core';
import { FixedSizeList } from 'react-window';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import Cookies from 'js-cookie';
import { LocationOn } from '@material-ui/icons';
import { SearchField } from 'shared/ui/uikit/search-field';

import { usePosition } from 'shared/hooks/usePosition';
import { Button } from 'shared/ui/uikit/button';
import {
  fetchCitiesByIso3,
  citiesSelector,
  currentCitySelector,
  setCurrentCity,
  currentCountrySelector,
  fetchNearestCities,
  nearestCitiesSelector,
} from '../../countries.slice';

const useButtonStyles = makeStyles((theme: Theme) => createStyles({
  label: {
    textTransform: 'none',
  },
}));

const useStyles = makeStyles((theme: Theme) => createStyles({
  container: {
    minWidth: 350,
  },
  predicateContainer: {
    minWidth: 250,
    maxWidth: 300,
  },
}));

const renderRow = ({ t, filteredCities, onSelectCity }: any) => ({ index, style }: any) => {
  const city = filteredCities[index].name;
  return (
    <MenuItem key={index} style={style} onClick={onSelectCity(city)}>
      {t(`cities:${city}`)}
    </MenuItem>
  );
};

export const HeaderCountry = () => {
  const [predicatedCity, setPredicatedCity] = useState<string | null>(null);
  const [showPredicatedCity, setShowPredicatedCity] = useState<boolean>(false);
  const [showNearestList, setShowNearestList] = useState<boolean>(false);
  const [showFullList, setShowFullList] = useState<boolean>(false);
  const [showCitiesSearch, setShowCitiesSearch] = useState<boolean>(false);

  const cityButtonRef = useRef(null);

  const [filteredCities, setFilteredCities] = useState<any[]>([]);
  const [searchValue, setSearchValue] = useState<string>('');

  const buttonClasses = useButtonStyles();
  const classes = useStyles();

  const dispatch = useDispatch();
  const cities = useSelector(citiesSelector);
  const nearestCities = useSelector(nearestCitiesSelector);

  const currentCity = useSelector(currentCitySelector);
  const currentCountry = useSelector(currentCountrySelector);

  const {
    latitude,
    longitude,
    error: positionError,
  } = usePosition();

  const { t } = useTranslation();

  const handleShowPredicatedCity = useCallback(() => {
    setShowPredicatedCity(true);
  }, []);

  const handleClosePredicatedCity = useCallback(() => {
    setShowPredicatedCity(false);
  }, []);

  const approveCity = useCallback(() => {
    setShowCitiesSearch(false);
    setShowNearestList(false);
    setShowFullList(false);
  }, []);

  const onChangeSearch = useCallback((e) => {
    setSearchValue(e.target.value);
  }, []);

  const handleShowFullList = useCallback(() => {
    dispatch(fetchCitiesByIso3('RUS'));
    setShowNearestList(false);
    setShowFullList(true);
    setShowCitiesSearch(true);
  }, []);

  const handleHideCitiesSearch = useCallback(() => {
    setShowCitiesSearch(false);
    setShowNearestList(false);
    setShowFullList(false);
  }, []);

  const handleChangePredicatedCity = useCallback(
    (city: string) => (e: any) => {
      setPredicatedCity(city);
      handleHideCitiesSearch();
    },
    [handleHideCitiesSearch],
  );
  const handleApproveCity = useCallback(() => {
    if (!predicatedCity) return;

    handleClosePredicatedCity();
    dispatch(setCurrentCity(predicatedCity));
    Cookies.set('currentCity', predicatedCity);
  }, [dispatch, handleClosePredicatedCity, predicatedCity]);

  const handleShowNearestList = useCallback(() => {
    if (positionError) {
      handleShowFullList();
      return;
    }

    if (!nearestCities.loaded || nearestCities.error) {
      dispatch(fetchNearestCities({ latitude, longitude }));
    }

    setShowNearestList(true);
    setShowCitiesSearch(true);
  }, [
    positionError,
    nearestCities.loaded,
    nearestCities.error,
    handleShowFullList,
    latitude,
    longitude,
  ]);

  useEffect(() => {
    if (showFullList && !cities.loaded) return;
    if (showNearestList && !nearestCities.loaded) return;

    let values: any[] = [];

    if (showFullList) {
      values = cities.data;
    }

    if (showNearestList) {
      values = nearestCities.data;
    }

    setFilteredCities(
      values.filter(({ name }) => name.includes(searchValue)),
    );
  }, [
    searchValue,
    showNearestList,
    showFullList,
    nearestCities,
    cities,
  ]);

  useEffect(() => {
    if (currentCity) return;
    if (!latitude || !longitude) return;
    if (nearestCities.loaded && !nearestCities.error) return;
    if (nearestCities.error) return;

    dispatch(fetchNearestCities({ latitude, longitude }));
  }, [latitude, longitude, currentCity, nearestCities]);

  useEffect(() => {
    if (predicatedCity) return;

    if (currentCity) {
      setPredicatedCity(currentCity);
      return;
    }

    if (nearestCities.data[0]) {
      setPredicatedCity(nearestCities.data[0].name);
    }
  }, [currentCity, nearestCities.loaded, predicatedCity]);

  useEffect(() => {
    const cookieCity: string | undefined = Cookies.get('currentCity');

    if (cookieCity) {
      dispatch(setCurrentCity(cookieCity));
    }
  }, []);

  const isLoadingPredicate = !nearestCities.loaded
    && !currentCity
    && nearestCities.loaded
    && !positionError;

  return (
    <div ref={cityButtonRef}>
      <Backdrop open={showPredicatedCity} />
      <Box>
        <Button
          startIcon={<LocationOn />}
          classes={buttonClasses}
          variant="text"
          size="small"
          color="default"
          onClick={handleShowPredicatedCity}
          loading={isLoadingPredicate}
          disabled={!positionError && !nearestCities.loaded}
        >
          <Typography>{currentCity || t('common:City')}</Typography>
        </Button>
        <Menu
          id="nearest-city-menu"
          anchorEl={cityButtonRef.current}
          keepMounted
          open={showPredicatedCity}
          onClose={handleClosePredicatedCity}
          PaperProps={{ elevation: 3 }}
        >
          <div className={classes.predicateContainer}>
            {!positionError || predicatedCity ? (
              <Box p={2}>
                <Typography variant="h6">Это ваш город?</Typography>
                <Typography variant="h4" color="primary">
                  {predicatedCity}
                </Typography>

                {!showNearestList ? (
                  <Box mt={2}>
                    <Grid container spacing={2}>
                      <Grid item xs={6}>
                        <Button
                          onClick={handleApproveCity}
                          fullWidth
                          size="large"
                          color="primary"
                        >
                          Да
                        </Button>
                      </Grid>
                      <Grid item xs={6}>
                        <Button
                          fullWidth
                          size="large"
                          color="secondary"
                          onClick={handleShowNearestList}
                        >
                          Нет
                        </Button>
                      </Grid>
                    </Grid>
                  </Box>
                ) : null}
              </Box>
            ) : (
              <Box p={2}>
                <Typography variant="h6">Что то пошло не так :(</Typography>
                <Typography variant="body2" color="textSecondary">
                  Не удалось определить ваше местоположение
                </Typography>

                <Box mt={2}>
                  <Button
                    fullWidth
                    size="large"
                    color="primary"
                    onClick={handleShowNearestList}
                  >
                    Выбрать город из списка
                  </Button>
                </Box>
              </Box>
            )}
          </div>
        </Menu>
        <Menu
          id="city-menu"
          anchorEl={cityButtonRef.current}
          keepMounted
          open={showCitiesSearch}
          onClose={handleHideCitiesSearch}
          PaperProps={{ elevation: 3 }}
        >
          <div className={classes.container}>
            <Box p={2}>
              <Typography variant="h5">{t('common:Choose city')}</Typography>
              <Typography variant="body1" color="textSecondary">
                {currentCountry.name}
              </Typography>
              <Box mt={2}>
                <SearchField
                  onChange={onChangeSearch}
                  value={searchValue}
                  placeholder={t('common:City')}
                />
              </Box>
            </Box>
            {(showNearestList && nearestCities.loaded)
            || (showFullList && cities.loaded) ? (
              <>
                <FixedSizeList
                  height={300}
                  width={350}
                  itemSize={46}
                  itemCount={filteredCities.length}
                >
                  {renderRow({
                    t,
                    filteredCities,
                    onSelectCity: handleChangePredicatedCity,
                  })}
                </FixedSizeList>
              </>
              ) : (
                <LinearProgress />
              )}

            {showNearestList && (
              <Box m={2}>
                <Button onClick={handleShowFullList} fullWidth>
                  Моего города нет в списке
                </Button>
              </Box>
            )}
          </div>
        </Menu>
      </Box>
    </div>
  );
};
