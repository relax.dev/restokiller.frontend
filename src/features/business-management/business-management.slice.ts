import {
  createAsyncThunk, createSlice, PayloadAction, unwrapResult,
} from '@reduxjs/toolkit';
import { StringSchema } from 'yup';
import { FetchableData } from 'shared/types/store';
import { AppDispatch, RootState } from 'shared/modules/store';
import { addFetchableDataCase } from 'shared/helpers/store';
import { getBusinessForManagementById } from 'shared/api/businesses';
import { IBusiness } from 'entities/business/libs/types';
import { defaultBusinessData } from 'entities/business/libs/data';

interface IBusinessState {
  body: FetchableData<IBusiness>;
}

const initialState: IBusinessState = {
  body: {
    data: defaultBusinessData,
    loaded: false,
    error: null,
  },
};

export const fetchBusinessForManagementById = createAsyncThunk(
  'business-managemen/fetchBusinessById',
  async (id: string) => {
    if (!id) return;

    const response = await getBusinessForManagementById(id);
    return response.data;
  },
);

export const businessManagementSlice = createSlice({
  name: 'business-management',
  initialState,
  reducers: {
    resetBusinessForManagement: (state) => {
      state.body = initialState.body;
    },
  },
  extraReducers: (builder) => {
    addFetchableDataCase(builder, fetchBusinessForManagementById, 'body');
  },
});

export const { resetBusinessForManagement } = businessManagementSlice.actions;

export const businessManagementBodySelector = (state: RootState) => state.businessManagement.body;

export default businessManagementSlice.reducer;
