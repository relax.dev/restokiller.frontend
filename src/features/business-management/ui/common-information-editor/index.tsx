import { useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useAppDispatch, useAppSelector } from 'shared/hooks/useRedux';
import { ErrorContainer } from 'shared/ui/uikit/error-container';
import { EditableText } from 'shared/ui/uikit/editable-text';
import { updateBusinessById } from 'shared/api/businesses';
import { useDispatch } from 'react-redux';
import { useNGLoader } from 'shared/hooks/useNGLoader';
import { useSnackbar } from 'notistack';
import { LoaderFullScreen } from 'shared/ui/uikit/loader-fullscreen';
import { businessManagementBodySelector, fetchBusinessForManagementById } from '../../business-management.slice';

export const CommonInformationEditor = () => {
  const { t } = useTranslation();
  const business = useAppSelector(businessManagementBodySelector);
  const dispatch = useAppDispatch();
  const [loading, setLoading] = useState<boolean>(false);

  const { enqueueSnackbar } = useSnackbar();

  const onSaveProperty = useCallback((name: string, value: string, closeHandler: () => void) => {
    setLoading(true);
    updateBusinessById(business.data.id, { [name]: value }).then(async () => dispatch(
      fetchBusinessForManagementById(business.data.id),
    )).then(() => {
      closeHandler();
      enqueueSnackbar(t('common:Saved'), { variant: 'success' });
    }).catch(() => {})
      .finally(() => {
        setLoading(false);
      });
  }, [t, business.data.id]);

  useNGLoader(!business.loaded);

  if (business.error) {
    return <ErrorContainer />;
  }

  if (!business.dirty) {
    return <LoaderFullScreen />;
  }

  return (
    <div>
      <EditableText
        label={t('common:Title')}
        name="name"
        loading={loading}
        value={business.data.name}
        onSave={onSaveProperty}
      />
      <EditableText
        label={t('common:Description')}
        name="description"
        value={business.data.description}
        loading={loading}
        onSave={onSaveProperty}
        EditInputProps={{
          multiline: true,
          rows: 6,
        }}
      />
    </div>
  );
};
