import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Form, Formik } from 'formik';
import { Grid, Box, Divider } from '@material-ui/core';
import cloneDeep from 'lodash/cloneDeep';
import { useAppSelector } from 'shared/hooks/useRedux';
import { ErrorContainer } from 'shared/ui/uikit/error-container';
import { PartnerRegistrationTimeForm } from 'shared/ui/forms/partner/registration/time';
import { Button } from 'shared/ui/uikit/button';
import { useDeepMemo } from 'shared/hooks/useDeepMemo';
import { updateBusinessById } from 'shared/api/businesses';
import { useNGLoader } from 'shared/hooks/useNGLoader';
import { useSnackbar } from 'notistack';
import { LoaderFullScreen } from 'shared/ui/uikit/loader-fullscreen';
import { businessManagementBodySelector, fetchBusinessForManagementById } from '../../business-management.slice';

export const WorkingTimeEditor = () => {
  const { t } = useTranslation();
  const business = useAppSelector(businessManagementBodySelector);
  const { enqueueSnackbar } = useSnackbar();
  const [loading, setLoading] = useState<boolean>(false);
  const formikProps = useDeepMemo(
    () => ({
      initialValues: {
        workingHours: cloneDeep(business.data.workingHours),
      },
      //   validationSchema: getStepValidation(activeStep)({ t }),
      onSubmit: (values: any) => {
        setLoading(true);
        updateBusinessById(business.data.id, { workingHours: values.workingHours }).then(() => {
          enqueueSnackbar(
            t('common:Saved'),
            {
              variant: 'success',
            },
          );
        })
          .catch(() => {}).finally(() => {
            setLoading(false);
          });
      },
    }),
    [t, business.data.id],
  );

  useNGLoader(!business.loaded);

  if (business.error) {
    return <ErrorContainer />;
  }

  if (!business.dirty) {
    return <LoaderFullScreen />;
  }

  return (
    <div>
      <Formik {...formikProps}>
        <Form>
          <PartnerRegistrationTimeForm />
          <Box my={2}>
            <Divider />
          </Box>
          <Grid container spacing={2} justify="flex-end">
            <Grid item xs={12} md={6} lg={4}>
              <Button fullWidth type="submit" loading={loading}>
                { t('common:Save')}
              </Button>
            </Grid>
          </Grid>
        </Form>
      </Formik>
    </div>
  );
};
