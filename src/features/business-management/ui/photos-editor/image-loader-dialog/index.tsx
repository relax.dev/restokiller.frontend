import {
  Dialog, DialogTitle, DialogContent,
} from '@material-ui/core';
import React, {
  FC, forwardRef, useMemo,
} from 'react';
import Slide from '@material-ui/core/Slide';
import { TransitionProps } from '@material-ui/core/transitions';
import { Formik } from 'formik';
import { ImageLoaderForm } from './image-loader-form';

const Transition = forwardRef((
  props: TransitionProps,
  ref: React.Ref<unknown>,
) => <Slide direction="up" ref={ref} {...props} />);

export interface IImageLoaderDialog {
  open: boolean;
  onClose: (e: any) => void;
  onSubmit: (values: any) => void;
  title: string;
  buttonText: string;
  loading: boolean;
  error: string | null;
}

export const ImageLoaderDialog: FC<IImageLoaderDialog> = ({
  open, onClose, onSubmit, title, buttonText, loading, error,
}) => {
  const formikProps = useMemo(
    () => ({
      initialValues: {
        photo: [],
      },
      onSubmit: (values: any) => {
        onSubmit(values);
      },
    }),
    [onClose],
  );

  return (
    <Dialog id="add-photo" TransitionComponent={Transition} open={open} onClose={onClose} maxWidth="sm" fullWidth>
      <DialogTitle>
        { title }
      </DialogTitle>

      <DialogContent>
        <Formik {...formikProps}>
          <ImageLoaderForm error={error} loading={loading} buttonText={buttonText} />
        </Formik>
      </DialogContent>
    </Dialog>
  );
};
