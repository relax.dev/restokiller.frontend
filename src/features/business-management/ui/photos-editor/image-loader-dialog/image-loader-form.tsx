import { Form } from 'formik';
import { FC } from 'react';
import { Box } from '@material-ui/core';
import { Button } from 'shared/ui/uikit/button';
import { FormikDropzoneArea } from 'shared/ui/uikit/formik-dropzone-area';

export interface IImageLoaderForm {
  error: null | string;
  loading: boolean;
  buttonText: string;
}

export const ImageLoaderForm: FC<IImageLoaderForm> = ({ error, loading, buttonText }) => (
  <Form>
    <FormikDropzoneArea id="photo" name="photo" filesLimit={1} />

    <Box my={2}>
      <Button type="submit" fullWidth>
        { buttonText }
      </Button>
    </Box>
  </Form>
);
