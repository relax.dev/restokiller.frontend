import { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  CardMedia, Divider, Grid, Box, Typography, Card, CardActionArea, CircularProgress,
} from '@material-ui/core';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';

import { Delete, Loop } from '@material-ui/icons';
import { useAppDispatch, useAppSelector } from 'shared/hooks/useRedux';
import { ErrorContainer } from 'shared/ui/uikit/error-container';
import { CardButton } from 'shared/ui/uikit/card-button';
import { addBusinessPhoto, deleteBusinessPhoto, updateBusinessHeaderPhoto } from 'shared/api/businesses';
import { useNGLoader } from 'shared/hooks/useNGLoader';
import { useSnackbar } from 'notistack';
import { LoaderFullScreen } from 'shared/ui/uikit/loader-fullscreen';
import { ImageLoaderDialog } from './image-loader-dialog';
import { businessManagementBodySelector, fetchBusinessForManagementById } from '../../business-management.slice';

const useStyles = makeStyles((theme: Theme) => createStyles({
  backdrop: {
    position: 'absolute',
    background: fade('#000', 0.8),
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    opacity: 0,
    transition: '.3s',
    willChange: 'opacity',
    flexDirection: 'column',
  },
  card: {
    position: 'relative',
    '&:hover': {
      '& $backdrop': {
        opacity: 1,
      },
    },
  },
}));

export const PhotosEditor = () => {
  const { t } = useTranslation();
  const business = useAppSelector(businessManagementBodySelector);
  const classes = useStyles();
  const dispatch = useAppDispatch();
  const [openedPhotosLoader, setOpenedPhotosLoader] = useState(false);
  const [openedHeaderPhotoLoader, setOpenedHeaderPhotoLoader] = useState(false);

  const { enqueueSnackbar } = useSnackbar();

  const handleOpenPhotosLoader = useCallback(() => {
    setOpenedPhotosLoader(true);
  }, []);

  const handleClosePhotosLoader = useCallback(() => {
    setOpenedPhotosLoader(false);
  }, []);

  const handleOpenHeaderPhotoLoader = useCallback(() => {
    setOpenedHeaderPhotoLoader(true);
  }, []);

  const handleCloseHeaderPhotoLoader = useCallback(() => {
    setOpenedHeaderPhotoLoader(false);
  }, []);

  const handleRefreshData = useCallback(() => {
    dispatch(fetchBusinessForManagementById(business.data?.id));
  }, [business.data?.id]);

  const handleAddPhoto = useCallback(({ photo }) => {
    addBusinessPhoto({ id: business.data.id, photo: photo[0] }).then(() => {
      handleRefreshData();
      handleClosePhotosLoader();
      enqueueSnackbar(t('common:Success'), { variant: 'success' });
    });
  }, [business.data?.id]);

  const handleChangeHeaderPhoto = useCallback(({ photo }) => {
    updateBusinessHeaderPhoto({ id: business.data.id, photo: photo[0] }).then(() => {
      handleRefreshData();
      handleCloseHeaderPhotoLoader();
      enqueueSnackbar(t('common:Success'), { variant: 'success' });
    });
  }, [business.data?.id]);

  const handleDeletePhoto = useCallback(({ url }) => () => {
    deleteBusinessPhoto({
      id: business.data?.id,
      url,
    }).then(() => {
      handleRefreshData();
    });
  }, [business.data?.id]);

  useNGLoader(!business.loaded);

  if (business.error) {
    return <ErrorContainer />;
  }

  if (!business.dirty) {
    return <LoaderFullScreen />;
  }

  return (
    <div>
      <Typography gutterBottom variant="h5">
        { t('common:Header image')}
      </Typography>
      <Card className={classes.card}>
        <CardActionArea disabled={!business.loaded} onClick={handleOpenHeaderPhotoLoader}>
          <CardMedia
            component="img"
            image={business.data.headerPhoto || 'http://sun9-61.userapi.com/c850628/v850628167/b47a5/WIR4TiBoK4g.jpg'}
            height={250}
            width="100%"
          />
          <div className={classes.backdrop}>
            <Loop color="inherit" style={{ fontSize: 120 }} />
            <Typography>
              { t('common:Update photo')}
            </Typography>
          </div>
        </CardActionArea>
      </Card>
      <Box my={2}>
        <Divider />
      </Box>

      <Typography gutterBottom variant="h5">
        { t('common:Preview images')}
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6} lg={4}>
          <CardButton disabled={!business.loaded} onClick={handleOpenPhotosLoader} />
        </Grid>
        {
          business.data.photos?.map((photo) => (
            <Grid item xs={12} md={6} lg={4}>
              <Card className={classes.card}>
                <CardActionArea onClick={handleDeletePhoto({ url: photo })}>
                  <CardMedia
                    component="img"
                    image={photo}
                    height={250}
                    width="100%"
                  />
                  <div className={classes.backdrop}>
                    <Delete color="error" style={{ fontSize: 120 }} />
                  </div>
                </CardActionArea>
              </Card>
            </Grid>
          ))
        }
      </Grid>
      <ImageLoaderDialog
        open={openedPhotosLoader}
        onClose={handleClosePhotosLoader}
        onSubmit={handleAddPhoto}
        title={t('common:Add new photo')}
        buttonText={t('common:Save')}
        loading={!business.loaded}
        error={null}
      />
      <ImageLoaderDialog
        open={openedHeaderPhotoLoader}
        onClose={handleCloseHeaderPhotoLoader}
        onSubmit={handleChangeHeaderPhoto}
        title={t('common:Update header photo')}
        buttonText={t('common:Update')}
        loading={!business.loaded}
        error={null}
      />
    </div>
  );
};
