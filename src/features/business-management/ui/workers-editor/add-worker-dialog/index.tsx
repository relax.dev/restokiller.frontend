import {
  FC, forwardRef,
  useState,
} from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  Slide,
  DialogActions,
  MenuItem,
  Grid,
} from '@material-ui/core';
import { Edit } from '@material-ui/icons';
import { useTranslation } from 'react-i18next';
import { TransitionProps } from '@material-ui/core/transitions';
import { Button } from 'shared/ui/uikit/button';
import { FormikTextField } from 'shared/ui/uikit/formik-text-field';
import { Form, Formik } from 'formik';
import { BusinessRole } from 'entities/user/libs/types';
import { addBusinessWorker } from 'shared/api/businesses';
import { useSnackbar } from 'notistack';
import { useDispatch } from 'react-redux';
import { fetchBusinessForManagementById } from 'features/business-management/business-management.slice';
import { useAppDispatch } from 'shared/hooks/useRedux';

export interface IAddWorkerDialog {
  open: boolean;
  onClose: () => void;
  businessId: string;
}

const Transition = forwardRef((
  props: TransitionProps,
  ref: React.Ref<unknown>,
) => <Slide direction="up" ref={ref} {...props} />);

export const AddWorkerDialog: FC<IAddWorkerDialog> = ({
  open, onClose, businessId,
}) => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState<boolean>(false);

  const { enqueueSnackbar } = useSnackbar();
  const dispatch = useAppDispatch();

  const handleAddUser = (values: { role: BusinessRole, email: string }) => {
    setLoading(true);

    addBusinessWorker(businessId, values).then(async () => {
      enqueueSnackbar(t('common:Worker added'), { variant: 'success' });
      await dispatch(fetchBusinessForManagementById(businessId));
      onClose();
    }).catch((err) => {
      enqueueSnackbar(t(`common:${err.response?.data.message}`), { variant: 'error' });
    }).finally(() => {
      setLoading(false);
    });
  };

  const handleInviteUser = () => {

  };

  return (
    <Dialog
      id="add-new-worker"
      TransitionComponent={Transition}
      open={open}
      onClose={onClose}
      maxWidth="sm"
      fullWidth
    >
      <Formik onSubmit={handleAddUser} initialValues={{ email: '123@mail.ru', role: BusinessRole.ADMINISTRATOR }}>
        <Form>
          <DialogTitle>
            { t('common:Add new worker')}
          </DialogTitle>

          <DialogContent>

            <Grid container spacing={2}>
              <Grid item xs={12} md={6} lg={8}>
                <FormikTextField
                  type="email"
                  name="email"
                  placeholder="hello@rest.ru"
                  label="Email"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <FormikTextField
                  name="role"
                  label="Role"
                  select
                  fullWidth
                >
                  <MenuItem value={BusinessRole.ADMINISTRATOR}>
                    { t('role:ADMINISTRATOR')}
                  </MenuItem>
                  <MenuItem value={BusinessRole.MANAGER}>
                    { t('role:MANAGER')}
                  </MenuItem>
                  <MenuItem value={BusinessRole.HOSTES}>
                    { t('role:HOSTES')}
                  </MenuItem>
                </FormikTextField>
              </Grid>
            </Grid>

          </DialogContent>

          <DialogActions>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Button type="submit" fullWidth disabled>
                  { t('common:Send invite') }
                </Button>
              </Grid>
              <Grid item xs={6}>
                <Button type="submit" fullWidth loading={loading}>
                  { t('common:Add') }
                </Button>
              </Grid>
            </Grid>
          </DialogActions>
        </Form>
      </Formik>
    </Dialog>
  );
};
