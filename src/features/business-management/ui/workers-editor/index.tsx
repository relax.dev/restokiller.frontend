import {
  Typography, Grid,
} from '@material-ui/core';
import { useTranslation } from 'next-i18next';
import { BusinessRole, IUser, IWorker } from 'entities/user/libs/types';
import { useAppDispatch, useAppSelector } from 'shared/hooks/useRedux';
import { businessManagementBodySelector, fetchBusinessForManagementById } from 'features/business-management/business-management.slice';
import { FullScreenDialog } from 'shared/ui/uikit/dialog-fullscreen';
import { FC, useState, useCallback } from 'react';
import { CardButton } from 'shared/ui/uikit/card-button';
import { removeBusinessWorker } from 'shared/api/businesses';
import { useDispatch } from 'react-redux';
import { DialogConfirmation } from 'shared/ui/uikit/dialog-confirmation';
import { useSnackbar } from 'notistack';
import { WorkerCard } from './worker-card';
import { AddWorkerDialog } from './add-worker-dialog';

export const WorkersEditor = () => {
  const { t } = useTranslation();
  const business = useAppSelector(businessManagementBodySelector);

  const [openedDialog, setOpenedDialog] = useState(false);
  const [openedRemoveDialog, setOpenedRemoveDialog] = useState(false);

  const [removingUserId, setRemovingUserId] = useState<string | null>(null);
  const [isRemoving, setIsRemoving] = useState(false);

  const dispatch = useAppDispatch();

  const { enqueueSnackbar } = useSnackbar();

  const handleOpenAddUserDialog = useCallback(() => {
    setOpenedDialog(true);
  }, []);

  const handleCloseAddUserDialog = useCallback(() => {
    setOpenedDialog(false);
  }, []);

  const handleOpenRemoveUserDialog = (userId: string) => () => {
    setOpenedRemoveDialog(true);
    setRemovingUserId(userId);
  };

  const handleCloseRemoveUserDialog = () => {
    setOpenedRemoveDialog(false);
  };

  const handleRemoveUser = useCallback(() => {
    if (!removingUserId) return;

    setIsRemoving(true);

    removeBusinessWorker(business.data.id, removingUserId).then(async () => {
      enqueueSnackbar(t('common:Worker removed'), { variant: 'success' });
      await dispatch(fetchBusinessForManagementById(business.data.id));

      handleCloseRemoveUserDialog();
      setRemovingUserId(null);
    }).catch((err) => {
      enqueueSnackbar(t(`common:${err.response.data.message}`), { variant: 'error' });
    }).finally(() => {
      setIsRemoving(false);
    });
  }, [business.data.id, removingUserId]);

  return (
    <>
      <Typography gutterBottom variant="h4">
        { t('common:Workers') }
      </Typography>

      <Grid container spacing={4}>
        <Grid item xs={12} md={6} lg={3}>
          <WorkerCard
            worker={{ id: business.data.owner, role: BusinessRole.OWNER }}
          />
        </Grid>
        {
          business.data.users.map((user: IWorker) => (
            <Grid item key={user.id} xs={12} md={6} lg={3}>
              <WorkerCard worker={user} onRemoveUser={handleOpenRemoveUserDialog(user.id)} />
            </Grid>
          ))

        }
        <Grid item xs={12} md={6} lg={3}>
          <CardButton onClick={handleOpenAddUserDialog} />
        </Grid>
      </Grid>

      <DialogConfirmation
        variant="delete"
        onClose={handleCloseRemoveUserDialog}
        onConfirm={handleRemoveUser}
        open={openedRemoveDialog}
        loading={isRemoving}
      />

      <AddWorkerDialog
        open={openedDialog}
        onClose={handleCloseAddUserDialog}
        businessId={business.data.id}
      />
    </>
  );
};
