import { Box } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import { IUser, IWorker } from 'entities/user/libs/types';
import { UserCard } from 'entities/user/ui/user-card';
import {
  useCallback, useState, useEffect, FC,
} from 'react';
import { removeBusinessWorker } from 'shared/api/businesses';

import { getUserById } from 'shared/api/user';
import { DialogConfirmation } from 'shared/ui/uikit/dialog-confirmation';

export interface IWorkerCard {
  worker: IWorker;
  onRemoveUser?: () => void;
}

export const WorkerCard: FC<IWorkerCard> = ({ worker, onRemoveUser }) => {
  const [user, setUser] = useState<IUser | null>(null);
  const [openedDialog, setOpenedDialog] = useState(false);

  useEffect(() => {
    getUserById(worker.id).then((response) => {
      setUser(response.data);
    });
  }, [worker.id]);

  if (!user) {
    return (
      <Box width="100%" height="250px">
        <Skeleton variant="rect" width="100%" height="100%" />
      </Box>
    );
  }

  return (
    <>
      <UserCard user={user} role={worker.role} onDelete={onRemoveUser} />
    </>
  );
};
