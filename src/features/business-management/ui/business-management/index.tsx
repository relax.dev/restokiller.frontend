import {
  useEffect,
  FC,
  useCallback,
} from 'react';
import {
  Container, Typography, Box, Divider,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { useRouter } from 'next/router';
import {
  Edit, EventSeat, ImageOutlined, People, QueryBuilder,
} from '@material-ui/icons';
import { useAppDispatch, useAppSelector } from 'shared/hooks/useRedux';
import { TabsContainer } from 'shared/ui/uikit/tabs-container';
import { LoaderFullScreen } from 'shared/ui/uikit/loader-fullscreen';
import { businessManagementBodySelector, fetchBusinessForManagementById, resetBusinessForManagement } from '../../business-management.slice';
import { TablesManagement } from '../../../tables-management/ui/tables-management';
import { CommonInformationEditor } from '../common-information-editor';
import { WorkingTimeEditor } from '../working-time-editor';
import { PhotosEditor } from '../photos-editor';
import { WorkersEditor } from '../workers-editor';

export interface IBusinessManagement {
  id: string;
}

export const BusinessManagement: FC<IBusinessManagement> = ({ id }) => {
  const dispatch = useAppDispatch();
  const { t } = useTranslation();

  const business = useAppSelector(businessManagementBodySelector);
  const router = useRouter();

  const { section } = router.query as { section: string };

  useEffect(() => {
    dispatch(fetchBusinessForManagementById(id));

    return () => {
      dispatch(resetBusinessForManagement());
    };
  }, []);

  const tabs = [
    {
      label: t('common:Common'),
      value: 'common',
      icon: <Edit />,
      Component: <CommonInformationEditor />,
    },
    {
      label: t('common:Photos'),
      value: 'photos',
      icon: <ImageOutlined />,
      Component: <PhotosEditor />,
    },
    {
      label: t('common:Working hours'),
      value: 'hours',
      icon: <QueryBuilder />,
      Component: <WorkingTimeEditor />,
    },
    {
      label: t('common:Workers'),
      value: 'workers',
      icon: <People />,
      Component: <WorkersEditor />,
    },
    {
      label: t('common:Tables'),
      value: 'tables',
      icon: <EventSeat />,
      Component: <TablesManagement businessId={id} />,
    },
  ];

  const currentTab = tabs.findIndex((tab) => tab.value === section);

  const handleChangeTab = useCallback((event, value: number) => {
    router.push({
      query: {
        section: tabs[value].value,
      },
    });
  }, []);

  if (!business.dirty) {
    return <LoaderFullScreen />;
  }

  return (
    <Container maxWidth="lg">
      <Typography gutterBottom variant="h3">
        { t('common:Business settings') }
      </Typography>
      <TabsContainer ariaLabel="business profile tabs" data={tabs} value={currentTab} onChange={handleChangeTab} />
      <Divider />
      <Box my={4}>
        {
          tabs[currentTab]?.Component
        }
      </Box>

      {/* { JSON.stringify(business.data) } */}
    </Container>
  );
};
