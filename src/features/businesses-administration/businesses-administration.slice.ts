import { createAsyncThunk, createSlice, unwrapResult } from '@reduxjs/toolkit';
import { AppDispatch, RootState } from 'shared/modules/store';
import { addFetchableDataCase } from 'shared/helpers/store';
import { getAllBusinesses, IBusinessesFilter } from 'shared/api/businesses';
import { IBusiness } from 'entities/business/libs/types';
import { FetchableData } from 'shared/types/store';

export interface IBusinessesAdministration {
  businessesList: FetchableData<IBusiness[]>
}

const initialState: IBusinessesAdministration = {
  businessesList: {
    data: [],
    loaded: false,
    error: null,
  },
};

export const fetchAllBusinesses = createAsyncThunk(
  'businessesAdministration/fetchAllBusinesses',
  async (filters: IBusinessesFilter, { rejectWithValue }) => {
    try {
      const response = await getAllBusinesses({ filters });
      return response.data;
    } catch (err) {
      return rejectWithValue('Не удалось получить список заведений');
    }
  },
);

export function getBusinesses(dispatch: AppDispatch): void {
  dispatch(fetchAllBusinesses({}))
    .then(unwrapResult)
    .catch((error: any) => {
      console.error('error', error);
    });
}

export const businessesAdministrationSlice = createSlice({
  name: 'businessesAdministration',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    addFetchableDataCase(builder, fetchAllBusinesses, 'businessesList');
  },
});

export const businessesListSelector = (state: RootState): FetchableData<IBusiness[]> => (
  state.businessesAdministration.businessesList
);

export default businessesAdministrationSlice.reducer;
