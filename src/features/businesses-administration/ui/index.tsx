import React, { ReactElement, useCallback } from 'react';
import { Container, Grid, Box } from '@material-ui/core';
import { useAppDispatch, useAppSelector } from 'shared/hooks/useRedux';
import {
  businessesListSelector,
  getBusinesses,
} from '../businesses-administration.slice';
import { BusinessCard } from './components/BusinessCard';

export function BusinessesAdministration(): ReactElement {
  const dispatch = useAppDispatch();
  const businesses = useAppSelector(businessesListSelector);
  const businessesList = businesses.data;

  const updatePage = useCallback(() => getBusinesses(dispatch), [dispatch]);

  React.useEffect(() => {
    updatePage();
  }, []);

  return (
    <Container maxWidth="md">
      <Box mt={4}>
        <Grid
          container
          spacing={2}
        >
          {businessesList.map((business) => (
            <Grid item xs={12} sm={6} md={6} lg={6} key={business.id}>
              <BusinessCard business={business} updatePage={updatePage} />
            </Grid>
          ))}
        </Grid>
      </Box>

    </Container>
  );
}
