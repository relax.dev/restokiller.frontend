import React, { ReactElement } from 'react';
import { CardActions, IconButton } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Check from '@material-ui/icons/Check';
import Clear from '@material-ui/icons/Clear';
import clsx from 'clsx';
import { SetStateType } from 'shared/types/common';
import { confirmVerification, deleteBusinessById } from 'shared/api/businesses';
import { useStyles } from './styles';

export interface IControlButtons {
  isTailVisible: boolean;
  setIsTailVisible: SetStateType<boolean>;
  businessId: string;
  isVerified: boolean;
  updatePage: () => void;
}

export function ControlButtons({
  isTailVisible,
  setIsTailVisible,
  businessId,
  isVerified,
  updatePage,
}: IControlButtons): ReactElement {
  const classes = useStyles();

  return (
    <CardActions disableSpacing>
      <IconButton
        className={clsx(classes.confirmButton)}
        onClick={() => {
          confirmVerification({ id: businessId })
            .then((res) => console.log('res', res))
            .catch((err) => console.error('err', err))
            .finally(updatePage);
        }}
        disabled={isVerified}
        color="primary"
        aria-label="confirm"
      >
        <Check />
      </IconButton>

      <IconButton
        onClick={() => {
          deleteBusinessById(businessId)
            .then((res) => console.log('res', res))
            .catch((err) => console.error('err', err))
            .finally(updatePage);
        }}
        color="secondary"
        aria-label="cancel"
      >
        <Clear />
      </IconButton>

      <IconButton
        className={clsx(classes.expand, {
          [classes.expandOpen]: isTailVisible,
        })}
        onClick={() => setIsTailVisible((prev: boolean): boolean => !prev)}
        aria-expanded={isTailVisible}
        aria-label="show more"
      >
        <ExpandMoreIcon />
      </IconButton>
    </CardActions>
  );
}
