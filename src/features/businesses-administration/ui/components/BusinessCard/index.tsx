import React, { ReactElement } from 'react';
import {
  Card, CardContent, Collapse, Box, Grid, IconButton, CardMedia,
} from '@material-ui/core';
import Room from '@material-ui/icons/Room';
import { useTranslation } from 'react-i18next';
import { IBusiness } from 'entities/business/libs/types';
import { TextInfo } from '../TextInfo';
import { ControlButtons } from '../ControlButtons';
import { useStyles } from '../../styles';

export interface IBusinessCard {
  business: IBusiness;
  updatePage: () => void;
}

export function BusinessCard({ business, updatePage }: IBusinessCard): ReactElement {
  const [isTailVisible, setIsTailVisible] = React.useState<boolean>(false);
  const classes = useStyles();
  const { t } = useTranslation();

  const Place = (
    <>
      {t('common:Location:')}
      <IconButton classes={{ root: classes.placeIcon }} color="primary">
        <Room />
      </IconButton>
    </>
  );

  return (
    <Card classes={{ root: classes.root }}>
      <CardMedia
        component="img"
        image={business.photos[0]}
        height={200}
      />
      <Box className={classes.contentContainer} p={2} pb={0}>
        <CardContent>
          <TextInfo title={t('common:Identificator:')}>{business.id}</TextInfo>
          <TextInfo title={t('common:Name:')}>{business.name}</TextInfo>
          <TextInfo title={t('common:Description:')}>{business.description}</TextInfo>

          <Collapse in={isTailVisible}>
            <TextInfo title={t('common:Addres:')}>{business.address}</TextInfo>
            <TextInfo title={t('common:City:')}>{business.city}</TextInfo>
            <TextInfo title={Place}>
              {`${t('common:Latitude:')} ${business.location.lat}; ${t('common:Longitude:')} ${business.location.lng}`}
            </TextInfo>
            <TextInfo title={t('common:Type:')}>{business.type}</TextInfo>
          </Collapse>
        </CardContent>

        <ControlButtons
          isTailVisible={isTailVisible}
          setIsTailVisible={setIsTailVisible}
          businessId={business.id}
          isVerified={business.isVerified}
          updatePage={updatePage}
        />
      </Box>
    </Card>
  );
}
