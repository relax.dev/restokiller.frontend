import React, { ReactChild, ReactElement } from 'react';
import { Typography } from '@material-ui/core';

export interface ITextInfo {
  children: ReactChild;
  title: ReactElement | string;
}

export function TextInfo({ children, title }: ITextInfo): ReactElement {
  return (
    <>
      <Typography variant="caption">{title}</Typography>
      <Typography gutterBottom>{children || 'нет'}</Typography>
    </>
  );
}
