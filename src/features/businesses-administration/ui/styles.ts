import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  root: {
    minHeight: 300,
  },

  placeIcon: {
    marginLeft: '.5rem',
    padding: 0,
  },

  contentContainer: {
    display: 'flex',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
});
