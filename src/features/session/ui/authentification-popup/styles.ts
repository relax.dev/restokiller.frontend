import {
  createStyles, makeStyles, Theme,
} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => createStyles({
  '@keyframes enter': {
    from: { transform: 'rotateX(90deg) translateY(-50%) scale(0.5)' },
    to: { transform: 'rotateX(0deg) translateY(0) scale(1)' },
  },
  container: {
    animationName: '$enter',
    animationDuration: '.4s',
    animationTimingFunction: 'ease',
    animationIterationCount: 1,
    transformStyle: 'preserve-3d',
  },
  wrapper: {
    perspective: '500px',
  },
  backdrop: {
    zIndex: 1,
    overflow: 'hidden',
  },
}));
