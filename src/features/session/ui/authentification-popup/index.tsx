import {
  Backdrop, Container, Paper, Box, Typography, ClickAwayListener,
} from '@material-ui/core';
import { useLoginStrategy } from 'features/session/libs/hooks/useLoginStrategy';
import { Formik } from 'formik';
import { memo, FC } from 'react';

import { useTranslation } from 'react-i18next';
import { useSession } from 'shared/hooks/useSession';
import { LoginForm } from 'shared/ui/forms/login';
import { useStyles } from './styles';

export const AuthentificationPopup = (() => {
  const { t } = useTranslation();
  const { error, loading, formikProps } = useLoginStrategy({
    onSuccessLogin: () => {},
  });
  const classes = useStyles();

  const { openedLoginPopup, handleCloseLoginPopup } = useSession();

  return (
    <Backdrop
      open={openedLoginPopup}
      className={classes.backdrop}
      transitionDuration={400}
      unmountOnExit
    >
      <Container maxWidth="sm" className={classes.wrapper}>
        <ClickAwayListener
          mouseEvent="onMouseDown"
          touchEvent="onTouchStart"
          onClickAway={handleCloseLoginPopup}
        >
          <Paper className={classes.container}>
            <Box p={4}>
              <Typography gutterBottom component="h1" variant="h4">
                Login for continue
              </Typography>

              <Formik {...formikProps}>
                <LoginForm error={error} loading={loading} />
              </Formik>
            </Box>
          </Paper>
        </ClickAwayListener>
      </Container>
    </Backdrop>
  );
});
