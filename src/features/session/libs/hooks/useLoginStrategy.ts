import { useTranslation } from 'next-i18next';
import { useState } from 'react';
import { useSession } from 'shared/hooks/useSession';
import { loginByEmailAndPassword } from 'shared/api/user';
import { validationSchema } from 'shared/ui/forms/login/validation.schema';

export interface IUseLoginStrategy {
  onSuccessLogin: () => void;
}

export const useLoginStrategy = ({ onSuccessLogin }: IUseLoginStrategy) => {
  const { login, loginCallback } = useSession();
  const { t } = useTranslation();

  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const formikProps = {
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: validationSchema({ t }),
    onSubmit: (values: any) => {
      setError(null);
      setLoading(true);
      loginByEmailAndPassword({
        email: values.email,
        password: values.password,
      })
        .then((response: { data: { token: string } }) => {
          login(response.data.token);

          onSuccessLogin();
          if (loginCallback) loginCallback();
        })
        .catch((e) => {
          setError(e.message);
        })
        .finally(() => {
          setLoading(false);
        });
    },
  };

  return {
    formikProps,
    loading,
    error,
  };
};
