import {
  createAsyncThunk, createSlice, PayloadAction, unwrapResult,
} from '@reduxjs/toolkit';
import { StringSchema } from 'yup';
import { FetchableData } from 'shared/types/store';
import { AppDispatch, RootState } from 'shared/modules/store';
import { addFetchableDataCase } from 'shared/helpers/store';
import { getBusinessForManagementById } from 'shared/api/businesses';
import { IBusiness } from 'entities/business/libs/types';
import { defaultBusinessData } from 'entities/business/libs/data';
import { getAllBookingsForGuestBook } from 'shared/api/bookings';
import { IBusinessBooking } from 'entities/bookings/libs/types';

interface IBusinessBookingsState {
  list: FetchableData<IBusinessBooking[]>;
}

const initialState: IBusinessBookingsState = {
  list: {
    data: [],
    loaded: false,
    error: null,
  },
};

export const fetchAllBookingsForGuestBook = createAsyncThunk(
  'business-bookings/fetchAllBookingsForGuestBook',
  async (id: string) => {
    if (!id) return;

    const response = await getAllBookingsForGuestBook(id);
    return response.data;
  },
);

export const businessBookingsSlice = createSlice({
  name: 'business-bookings',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    addFetchableDataCase(builder, fetchAllBookingsForGuestBook, 'list');
  },
});

export const businessBookingsSelector = (state: RootState) => state.businessBookings.list;

export default businessBookingsSlice.reducer;
