import { IBusinessBooking } from 'entities/bookings/libs/types';
import { FC, useEffect, useCallback } from 'react';
import { BookingCardBusiness } from 'entities/bookings/ui/booking-card-business';

import { useAppDispatch, useAppSelector } from 'shared/hooks/useRedux';
import { businessBookingsSelector, fetchAllBookingsForGuestBook } from 'features/business-bookings/business-bookings.slice';
import { Grid, Box, Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { BookingStatusEnum, updateBookingStatus } from 'shared/api/bookings';

export interface IBusinessBookings {
  businessId: string;
}

export const BusinessBookings: FC<IBusinessBookings> = ({ businessId }) => {
  const dispatch = useAppDispatch();
  const bookings = useAppSelector(businessBookingsSelector);
  const { t } = useTranslation();

  useEffect(() => {
    updateBookings();
  }, []);

  const updateBookings = useCallback(() => {
    dispatch(fetchAllBookingsForGuestBook(businessId));
  }, [businessId]);

  const cancelBooking = useCallback((id: string) => {
    updateBookingStatus(id, BookingStatusEnum.CANCELED);
  }, []);

  const approveBooking = useCallback((id: string) => {
    updateBookingStatus(id, BookingStatusEnum.APPROVED);
  }, []);

  const doneBooking = useCallback((id: string) => {
    updateBookingStatus(id, BookingStatusEnum.DONE);
  }, []);

  return (
    <Box mx={4}>
      <Typography gutterBottom variant="h3">
        { t('common:Bookings')}
        {' '}
        <button type="button" onClick={updateBookings}>Update</button>
      </Typography>
      <Grid container spacing={2}>
        {
          bookings.data.map((booking) => (
            <Grid item xs={12} md={6} lg={4} key={booking.id}>
              <BookingCardBusiness
                booking={booking}
                onCancelBooking={cancelBooking}
                onApproveBooking={approveBooking}
                onDoneBooking={doneBooking}
              />
            </Grid>
          ))
        }
      </Grid>
    </Box>
  );
};
