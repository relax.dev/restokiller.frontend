import {
  ActionReducerMapBuilder, AsyncThunk, createAsyncThunk, createSlice, PayloadAction,
} from '@reduxjs/toolkit';
import { boolean } from 'yup/lib/locale';
import { FetchableData } from 'shared/types/store';
import { RootState } from 'shared/modules/store';
import { addFetchableDataCase } from 'shared/helpers/store';
import {
  getAllBusinesses, getBusinessFeatures, getBusinessKitchen, getBusinessPurposes, IBusinessesFilter,
} from 'shared/api/businesses';
import {
  IBusiness, IFeature, IKitchen, IPurpose,
} from 'entities/business/libs/types';

interface IBusinessesState {
  list: FetchableData<IBusiness[]>
  purposes: FetchableData<IPurpose[]>
  features: FetchableData<IFeature[]>
  kitchen: FetchableData<IKitchen[]>

  filters: IBusinessFilters,
  sorting: {
    field: string,
  },
  showMap: boolean;
}

export interface IBusinessFilters {
  purposes?: string[],
  features?: string[],
  kitchen?: string[],
  averagePrice?: number[],
  nearby?: boolean,
  name?: string,
  cities?: string[],
}

const initialState: IBusinessesState = {
  list: {
    data: [],
    loaded: false,
    error: null,
  },
  purposes: {
    data: [],
    loaded: false,
    error: null,
  },
  features: {
    data: [],
    loaded: false,
    error: null,
  },
  kitchen: {
    data: [],
    loaded: false,
    error: null,
  },
  filters: {
    purposes: [],
    features: [],
    kitchen: [],
    averagePrice: [0, 0],
    nearby: false,
    name: '',
    cities: [],
  },
  sorting: {
    field: 'popularity',
  },

  showMap: false,
};

export const fetchAllBusinesses = createAsyncThunk(
  'businesses/fetchAllBusinesses',
  async (filters: IBusinessesFilter, { rejectWithValue }) => {
    try {
      const response = await getAllBusinesses({ filters });
      return response.data;
    } catch (err) {
      return rejectWithValue('Не удалось получить список заведений');
    }
  },
);

export const fetchBusinessPurposes = createAsyncThunk(
  'businesses/fetchBusinessesPurposes',
  async () => {
    const response = await getBusinessPurposes();
    return response.data;
  },
);

export const fetchBusinessFeatures = createAsyncThunk(
  'businesses/fetchBusinessFeatures',
  async () => {
    const response = await getBusinessFeatures();
    return response.data;
  },
);

export const fetchBusinessKitchen = createAsyncThunk(
  'businesses/fetchBusinessKitchen',
  async () => {
    const response = await getBusinessKitchen();
    return response.data;
  },
);

export const businessesSlice = createSlice({
  name: 'businesses',
  initialState,
  reducers: {
    setBusinessesFilters: (state, action: PayloadAction<IBusinessFilters>) => {
      state.filters = { ...state.filters, ...action.payload };
    },

    resetBusinessesFilters: (state) => {
      state.filters = {};
    },
  },
  extraReducers: (builder) => {
    addFetchableDataCase(builder, fetchAllBusinesses, 'list');
    addFetchableDataCase(builder, fetchBusinessFeatures, 'features');
    addFetchableDataCase(builder, fetchBusinessKitchen, 'kitchen');
    addFetchableDataCase(builder, fetchBusinessPurposes, 'purposes');
  },
});

export const { setBusinessesFilters, resetBusinessesFilters } = businessesSlice.actions;

export const businessesSelector = (state: RootState) => state.businesses.list;
export const purposesSelector = (state: RootState) => state.businesses.purposes;
export const featuresSelector = (state: RootState) => state.businesses.features;
export const kitchenSelector = (state: RootState) => state.businesses.kitchen;
export const businessesFiltersSelector = (state: RootState) => state.businesses.filters;

export default businessesSlice.reducer;
