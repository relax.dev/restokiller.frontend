import {
  FormControlLabel,
  Grid,
  InputAdornment,
  MenuItem,
  Switch,
  TextField,
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Search, Sort } from '@material-ui/icons';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import {
  businessesFiltersSelector,
  businessesSelector,
  setBusinessesFilters,
} from '../../businesses.slice';

const useStyles = makeStyles((theme: Theme) => createStyles({}));

export interface IBusinessSearchPanel {
  showMapCard: boolean;
  toggleMapCard: () => void;
}

export const BusinessesSearchPanel: FC<IBusinessSearchPanel> = ({ showMapCard, toggleMapCard }) => {
  const dispatch = useDispatch();
  const filters = useSelector(businessesFiltersSelector);
  const businesses = useSelector(businessesSelector);

  const { t } = useTranslation();

  // eslint-disable-next-line max-len
  const handleChangeTextField = (fieldName: string) => (event: React.ChangeEvent<{ value: unknown }>) => {
    dispatch(
      setBusinessesFilters({
        [fieldName]: event.target.value,
      }),
    );
  };

  return (
    <div>
      <Grid container spacing={2} alignItems="center">
        <Grid item xs={12} md={12} lg={6}>
          <TextField
            margin="none"
            fullWidth
            disabled={!!businesses.error}
            value={filters.name}
            onChange={handleChangeTextField('name')}
            placeholder="Найти по имени"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Search />
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item xs={7} md={8} lg={3}>
          <TextField
            margin="none"
            disabled={!!businesses.error}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Sort />
                </InputAdornment>
              ),
            }}
            fullWidth
            label="Сортировать по:"
            select
            value="pop1"
          >
            <MenuItem value="pop">Популярности</MenuItem>
            <MenuItem value="pop1">Рейтингу</MenuItem>
            <MenuItem value="pop2">Расстоянию</MenuItem>
          </TextField>
        </Grid>
        <Grid item xs={5} md={4} lg={3}>
          <FormControlLabel
            control={(
              <Switch
                checked={showMapCard}
                disabled={!!businesses.error}
                onChange={toggleMapCard}
                name="checkedB"
                color="primary"
              />
            )}
            label="Показать на карте"
            labelPlacement="start"
          />
        </Grid>
      </Grid>
    </div>
  );
};
