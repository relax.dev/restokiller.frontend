import {
  Box,
  Chip,
  FormControlLabel,
  Grid,
  MenuItem,
  Paper,
  Slider,
  Switch,
  TextField,
  Typography,
  InputAdornment,
} from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { IFeature, IKitchen, IPurpose } from 'entities/business/libs/types';
import { EmojiObjectsRounded, MyLocation, RestaurantMenu } from '@material-ui/icons';
import {
  purposesSelector,
  kitchenSelector,
  featuresSelector,
  businessesFiltersSelector,
  setBusinessesFilters,
} from '../../businesses.slice';

const useStyles = makeStyles(() => createStyles({
  root: {},
}));

const renderValue = (selected: unknown) => (
  <Grid container spacing={1}>
    {(selected as string[]).map((value) => (
      <Grid item>
        <Chip
          variant="outlined"
          color="primary"
          size="medium"
          key={value}
          label={value}
        />
      </Grid>
    ))}
  </Grid>
);

export const BusinessesFiltersPanel = () => {
  const classes = useStyles();

  const dispatch = useDispatch();
  const purposes = useSelector(purposesSelector);
  const features = useSelector(featuresSelector);
  const kitchen = useSelector(kitchenSelector);
  const filters = useSelector(businessesFiltersSelector);

  const { t } = useTranslation();

  const pricesMarks = [
    {
      value: 500,
      label: '500',
    },
    {
      value: 24000,
      label: '24k',
    },
  ];

  // eslint-disable-next-line max-len
  const handleChangeSelect = (fieldName: string) => (event: React.ChangeEvent<{ value: unknown }>) => {
    dispatch(
      setBusinessesFilters({
        [fieldName]: event.target.value,
      }),
    );
  };

  // eslint-disable-next-line max-len
  const handleChangeSwitch = (fieldName: string) => (event: React.ChangeEvent<{ value: unknown }>, checked: boolean) => {
    dispatch(
      setBusinessesFilters({
        [fieldName]: checked,
      }),
    );
  };

  return (
    <div className={classes.root}>
      <Typography gutterBottom variant="h4">{ t('common:Filters')}</Typography>
      <Paper elevation={3}>
        <Box p={2}>
          <Box>
            <Grid container>
              <Grid item xs={12}>
                <TextField
                  label="Цель посещения"
                  select
                  fullWidth
                  onChange={handleChangeSelect('purposes')}
                  SelectProps={{
                    multiple: true,
                    renderValue,
                  }}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <MyLocation />
                      </InputAdornment>
                    ),
                  }}
                  disabled={!purposes.data.length}
                  value={filters.purposes}
                >
                  {purposes.data.map((purpose: IPurpose) => (
                    <MenuItem key={purpose.name} value={purpose.name}>
                      {t(`purposes:${purpose.name}`)}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  label="Особенности"
                  select
                  fullWidth
                  onChange={handleChangeSelect('features')}
                  SelectProps={{
                    multiple: true,
                    renderValue,
                  }}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <EmojiObjectsRounded />
                      </InputAdornment>
                    ),
                  }}
                  disabled={!features.data.length}
                  value={filters.features}
                >
                  {features.data.map((feature: IFeature) => (
                    <MenuItem key={feature.name} value={feature.name}>
                      {t(`features:${feature.name}`)}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  label="Кухня"
                  select
                  fullWidth
                  onChange={handleChangeSelect('kitchen')}
                  SelectProps={{
                    multiple: true,
                    renderValue,
                  }}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <RestaurantMenu />
                      </InputAdornment>
                    ),
                  }}
                  disabled={!kitchen.data.length}
                  value={filters.kitchen}
                >
                  {kitchen.data.map((kitchenItem: IKitchen) => (
                    <MenuItem key={kitchenItem.name} value={kitchenItem.name}>
                      {t(`kitchen:${kitchenItem.name}`)}
                    </MenuItem>
                  ))}
                </TextField>
                {' '}
              </Grid>

              <Grid item xs={12}>
                <Box pt={2}>
                  <Typography id="range-slider" gutterBottom>
                    Средний чек
                  </Typography>
                  <Box ml={1} mr={1}>
                    <Slider
                      disabled
                      value={[1200, 5220]}
                      min={500}
                      max={24000}
                      onChange={() => {}}
                      valueLabelDisplay="auto"
                      aria-labelledby="range-slider"
                      getAriaValueText={() => '$20.'}
                      valueLabelFormat={() => '$20.'}
                      marks={pricesMarks}
                    />
                  </Box>
                </Box>
              </Grid>

              <Grid item xs={12}>
                <FormControlLabel
                  control={(
                    <Switch
                      checked={filters.nearby}
                      onChange={handleChangeSwitch('nearby')}
                      name="checkedB"
                      color="primary"
                    />
                  )}
                  label="Рядом со мной"
                />
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Paper>
    </div>
  );
};
