import {
  Button,
  CircularProgress,
  Container,
  Paper,
  Typography,
} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'next-i18next';

import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import { useEffect, useState, useCallback } from 'react';
import { Alert } from '@material-ui/lab';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { AuthLayout } from 'shared/ui/layouts/auth-layout';
import { verify } from 'shared/api/user';
import { useSession } from 'shared/hooks/useSession';
import { LoaderFullScreen } from 'shared/ui/uikit/loader-fullscreen';

const useStyles = makeStyles((theme) => ({
  paper: {
    width: '100%',
    boxSizing: 'border-box',
    padding: theme.spacing(2),
    marginTop: theme.spacing(2),
    textAlign: 'center',
  },
  icon: {
    width: '60px',
    height: '60px',
  },
  enterText: {
    margin: theme.spacing(3, 0),
  },
}));

export default function Login(props: any) {
  const classes = useStyles();
  const { t } = useTranslation();
  const router = useRouter();
  const { token } = router.query;

  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<any | null>(null);

  const { login } = useSession();

  const handleHome = useCallback(() => {
    router.push('/').catch(() => {
      console.error('route');
    });
  }, [router]);

  useEffect(() => {
    if (!token) return;

    setLoading(true);

    let timeout: any = null;

    verify(token as string)
      .then((response) => {
        login(response.data.token);

        timeout = setTimeout(handleHome, 3000);
      })
      .catch((err) => {
        setError(err);
      })
      .finally(() => {
        setLoading(false);
      });

    return () => clearTimeout(timeout);
  }, [token]);

  if (router.isFallback) {
    return <LoaderFullScreen />;
  }

  const getPageTitle = () => {
    if (loading) {
      return t('common:Waiting');
    }

    if (error) {
      return t('common:Error');
    }

    return t('common:Welcome Roman!');
  };

  return (
    <AuthLayout title={getPageTitle()}>
      <Paper className={classes.paper}>
        {!loading && !error && <CheckCircleIcon className={classes.icon} />}

        {!error ? (
          <Typography component="h2" variant="h5" align="center">
            {loading ? 'Проверка токена...' : 'Ваш профиль подтвержден!'}
          </Typography>
        ) : (
          <Alert severity="error">{error.response.data.message}</Alert>
        )}

        {!loading && !error && (
          <>
            <Typography align="center" className={classes.enterText}>
              Автоматический вход через 3 секунды...
            </Typography>
            <Button onClick={handleHome}>Войти сейчас</Button>
          </>
        )}
      </Paper>
    </AuthLayout>
  );
}

export function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}

export const getStaticProps = async ({ locale }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});
