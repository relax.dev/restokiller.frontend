import { ReactElement } from 'react';
import { MainLayout } from 'shared/ui/layouts/main-layout';
import { BusinessesAdministration } from 'features/businesses-administration/ui';

export default function BusinessesAdministrationPage(): ReactElement {
  return <MainLayout><BusinessesAdministration /></MainLayout>;
}
