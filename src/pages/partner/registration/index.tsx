import {
  Container,
  Typography,
  Box,
  Grid,
  Paper,
  Stepper,
  Step,
  StepLabel,
  StepContent,
} from '@material-ui/core';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { Formik } from 'formik';

import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import {
  useCallback, useEffect, useState, useMemo,
} from 'react';
import { useTranslation } from 'next-i18next';
import { PartnerLayout } from 'shared/ui/layouts/partner-layout';
import { experementalUpload } from 'shared/api/businesses';

import {
  fetchBusinessFeatures,
  fetchBusinessKitchen,
  fetchBusinessPurposes,
} from 'features/businesses/businesses.slice';
import { fetchCitiesByIso3 } from 'features/countries/countries.slice';

import { useAppDispatch } from 'shared/hooks/useRedux';

import { PartnerRegistrationCommonForm } from 'shared/ui/forms/partner/registration/common';

import { PartnerRegistrationTimeForm } from 'shared/ui/forms/partner/registration/time';
import { PartnerRegistrationAdditionalForm } from 'shared/ui/forms/partner/registration/addtitional';
import { PartnerRegistrationSubbmitingForm } from 'shared/ui/forms/partner/registration/submiting';
import { PartnerRegistrationMainForm } from 'shared/ui/forms/partner/registration/main';

import { validationSchema as partnerCommonFormValidationSchema } from 'shared/ui/forms/partner/registration/common/validation.schema';
import { validationSchema as partnerTimeFormValidationSchema } from 'shared/ui/forms/partner/registration/time/validation.schema';
import { validationSchema as partnerAdditionalFormValidationSchema } from 'shared/ui/forms/partner/registration/addtitional/validation.schema';
import { validationSchema as partnerSubbmitingFormValidationSchema } from 'shared/ui/forms/partner/registration/submiting/validation.schema';

const useStyles = makeStyles((theme: Theme) => createStyles({
  container: {
    [theme.breakpoints.down('md')]: {
      paddingLeft: 0,
      paddingRight: 0,
    },
  },
}));

function getStepContent(stepIndex: number) {
  switch (stepIndex) {
    case 0:
      return <PartnerRegistrationCommonForm />;
    case 1:
      return <PartnerRegistrationTimeForm />;
    case 2:
      return <PartnerRegistrationAdditionalForm />;
    case 3:
      return <PartnerRegistrationSubbmitingForm />;
    default:
      return 'Unknown stepIndex';
  }
}

function getStepValidation(stepIndex: number) {
  switch (stepIndex) {
    case 0:
      return partnerCommonFormValidationSchema;
    case 1:
      return partnerTimeFormValidationSchema;
    case 2:
      return partnerAdditionalFormValidationSchema;
    case 3:
      return partnerSubbmitingFormValidationSchema;
    default:
      return () => 'Unknown stepIndex';
  }
}

export default function PartnerRegistration() {
  const [activeStep, setActiveStep] = useState(0);
  const dispatch = useAppDispatch();
  const classes = useStyles();
  const { t } = useTranslation();

  const formikProps = useMemo(
    () => ({
      initialValues: {
        workingHours: {
          monday: {
            active: true,
            start: '12:00',
            end: '00:00',
          },
          tuesday: {
            active: true,
            start: '12:00',
            end: '13:00',
          },
          wednesday: {
            active: true,
            start: '12:00',
            end: '13:00',
          },
          thursday: {
            active: true,
            start: '12:00',
            end: '13:00',
          },
          friday: {
            active: true,
            start: '12:00',
            end: '13:00',
          },
          saturday: {
            active: false,
            start: '12:00',
            end: '13:00',
          },
          sunday: {
            active: false,
            start: '12:00',
            end: '13:00',
          },
        },
        photos: [],
        name: '',
        description: '',
        type: '',
        purposes: [],
        features: [],
        kitchen: [],
        city: '',
        address: '',
        location: null,
        averageCheck: 0,
      },
      validationSchema: getStepValidation(activeStep)({ t }),
      onSubmit: (values: any) => {
        experementalUpload(values).catch((err) => {
          console.error('error', err);
        });
      },
    }),
    [activeStep, t],
  );

  const handleNext = useCallback((e: any) => {
    e.preventDefault();
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  }, []);

  const handleBack = useCallback((e: any) => {
    e.preventDefault();
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  }, []);

  const handleReset = useCallback(() => {
    setActiveStep(0);
  }, []);

  const steps = [
    {
      label: 'Основное',
      description: 'Заполните основную информацию о вашем заведении',
    },
    {
      label: 'Время работы',
      description: 'Укажите время работы вашего заведения',
    },
    {
      label: 'Дополнительно',
      description: 'Заполните дополнительную информацию о вашем заведении',
    },
    {
      label: 'Подтверждение',
      description: 'Подтвердите регистрацию заведения',
    },
  ];

  useEffect(() => {
    dispatch(fetchBusinessPurposes());
    dispatch(fetchBusinessFeatures());
    dispatch(fetchBusinessKitchen());
    dispatch(fetchCitiesByIso3('RUS'));
  }, []);

  return (
    <PartnerLayout>
      <Container maxWidth="md" className={classes.container}>
        <Box p={2}>
          <Typography gutterBottom variant="h4">
            Регистрация заведения
          </Typography>
        </Box>

        <Grid container>
          <Grid item xs={12} md={4}>
            <Stepper orientation="vertical" activeStep={activeStep}>
              {steps.map((step, index) => (
                <Step key={step.label}>
                  <StepLabel>{step.label}</StepLabel>
                  <StepContent>
                    <Typography>
                      Шаг
                      {index + 1}
                      .
                    </Typography>
                    <Typography variant="body2" color="textSecondary">
                      {step.description}
                    </Typography>
                  </StepContent>
                </Step>
              ))}
            </Stepper>
          </Grid>
          <Grid item xs={12} md={8}>
            <Paper elevation={4}>
              <Box p={4}>
                <Formik {...formikProps}>
                  <PartnerRegistrationMainForm
                    activeStep={activeStep}
                    handleBack={handleBack}
                    handleNext={handleNext}
                    content={getStepContent(activeStep)}
                    label={steps[activeStep].label}
                  />
                </Formik>
              </Box>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </PartnerLayout>
  );
}

export const getStaticProps = async ({ locale, ...ctx }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});
