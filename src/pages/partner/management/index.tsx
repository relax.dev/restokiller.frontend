import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { PartnerLayout } from 'shared/ui/layouts/partner-layout';

export default function PartnerRegistration() {
  return <PartnerLayout>123123123123123</PartnerLayout>;
}

export const getStaticProps = async ({ locale, ...ctx }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});
