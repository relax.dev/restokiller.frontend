import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { BusinessesManagement } from 'features/businesses-management/ui/businesses-management';
import { PartnerLayout } from 'shared/ui/layouts/partner-layout';

export default function PartnerManagementBusinesses() {
  return (
    <PartnerLayout>
      <BusinessesManagement />
    </PartnerLayout>
  );
}

export const getStaticProps = async ({ locale, ...ctx }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});
