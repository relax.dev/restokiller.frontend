import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { PartnerLayout } from 'shared/ui/layouts/partner-layout';
import { BusinessManagement } from 'features/business-management/ui/business-management';

export default function PartnerManagementBusiness() {
  const router = useRouter();
  const { id } = router.query as { id: string };

  return (
    <PartnerLayout>
      <BusinessManagement id={id} />
    </PartnerLayout>
  );
}

export const getStaticProps = async ({ locale, ...ctx }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});

export function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}
