import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { BusinessBookings } from 'features/business-bookings/ui/business-bookings';
import { PartnerLayout } from 'shared/ui/layouts/partner-layout';
import { store } from 'shared/modules/store';
import { fetchAllBookingsForGuestBook } from 'features/business-bookings/business-bookings.slice';
import { useEffect } from 'react';

export default function BusinessBookingsPage() {
  return (
    <PartnerLayout>
      <BusinessBookings businessId="60c8acce9d313fc5fb6fa80e" />
    </PartnerLayout>
  );
}

export const getStaticPaths = () => ({
  paths: [],
  fallback: true,
});

export const getStaticProps = async ({ locale, id }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});
