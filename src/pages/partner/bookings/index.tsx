import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { PartnerLayout } from 'shared/ui/layouts/partner-layout';

export default function PartnerBookings() {
  return <PartnerLayout>Bookings</PartnerLayout>;
}

export const getStaticProps = async ({ locale, ...ctx }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});
