import { Box, Container, Typography } from '@material-ui/core';
import { IFeature, IKitchen, IPurpose } from 'entities/business/libs/types';
import { BusinessesGrid } from 'entities/business/ui/businesses-grid';
import { fetchAllBusinesses, businessesSelector } from 'features/businesses/businesses.slice';

import { currentCitySelector } from 'features/countries/countries.slice';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { useAppDispatch, useAppSelector } from 'shared/hooks/useRedux';
import { MainLayout } from 'shared/ui/layouts/main-layout';

export default function Collection() {
  const { t } = useTranslation();

  const businesses = useAppSelector(businessesSelector);
  const dispatch = useAppDispatch();

  const currentCity = useSelector(currentCitySelector);

  useEffect(() => {
    dispatch(fetchAllBusinesses({
      cities: ['Penza'],
      purposes: ['Business lunch'],
    }));
  }, []);

  return (
    <MainLayout>
      <Container maxWidth="lg">
        <Box my={4}>
          <Typography variant="h3" component="h1">
            { t('common:Collection')}
          </Typography>
          <Typography variant="body1">
            { t('common:Some description about this collection, for every collection this text will unique')}
          </Typography>
        </Box>
        <BusinessesGrid businesses={businesses} />
      </Container>
    </MainLayout>
  );
}

export const getStaticPaths = () => ({
  paths: [],
  fallback: true,
});

export const getStaticProps = async ({ locale }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});
