import {
  Box,
  Button,
  Container, Divider, Grid, GridSize, Typography,
} from '@material-ui/core';
import { Search } from '@material-ui/icons';
import {
  ICategory, IFeature, IKitchen, IPurpose,
} from 'entities/business/libs/types';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import { useTranslation } from 'react-i18next';
import {
  getBusinessCategories, getBusinessFeatures, getBusinessKitchen, getBusinessPurposes,
} from 'shared/api/businesses';
import { gridGenerator } from 'shared/helpers/grid-generator';

import { MainLayout } from 'shared/ui/layouts/main-layout';
import { MediaHeader } from 'shared/ui/uikit/media-header';
import { IPreviewCard, PreviewCard } from 'shared/ui/uikit/preview-card';

export interface PreviewParams {
  title: string;
  description: string;
  image: string;
}

export interface ICollectionsPage {
  categories: ICategory[];
}

export default function Collections({ categories }: ICollectionsPage) {
  const { t } = useTranslation();

  return (
    <MainLayout>
      <MediaHeader image="http://zexler.ru/sites/default/files/image/blog-komandy/12421/531719.jpg" height={450}>
        <Box textAlign="center">
          <Box width="100%">
            <Typography variant="h2" component="h1">
              { t('common:Resto booking')}
            </Typography>
          </Box>

          <Box mt={4}>
            <Button
              startIcon={<Search style={{ fontSize: '32px' }} />}
              style={{
                borderRadius: '32px',
                fontSize: '16px',
              }}
            >
              {t('common:Find and book now')}
            </Button>
          </Box>
        </Box>

      </MediaHeader>
      <Container maxWidth="lg">
        <Box mt={4}>
          {
            categories.map((category) => (
              <Box mb={2} key={category.name}>
                <Typography gutterBottom variant="h3" component="h2">
                  {t(`common:${category.name}`)}
                </Typography>
                <Box my={4}>
                  <Grid container spacing={4}>
                    { gridGenerator(category.collections, 'title', ({ key, ...data }: IPreviewCard, size: GridSize) => (
                      <Grid item xs={12} md={6} lg={size} key={key}>
                        <PreviewCard
                          {...data}
                        />
                      </Grid>
                    )) }
                  </Grid>
                </Box>
              </Box>
            ))
          }
        </Box>
      </Container>
    </MainLayout>
  );
}

export const getStaticProps = async ({ locale }: any) => {
  let categories: ICategory[] = [];
  let error: string | null = null;

  try {
    categories = (await getBusinessCategories()).data;
  } catch (e) {
    console.log(e);
    error = e.message;
  }

  console.log('categories', categories);

  return {
    props: {
      ...(await serverSideTranslations(locale, ['common'])),
      categories,
    },
  };
};
