import { ProfileSettingsEditor } from 'features/profile-settings/ui/profile-settings-editor';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { MainLayout } from 'shared/ui/layouts/main-layout';

export default function ProfileSettings() {
  return (
    <MainLayout>
      <ProfileSettingsEditor />
    </MainLayout>
  );
}

export const getStaticProps = async ({ locale }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});
