import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import { Formik } from 'formik';
import { AuthLayout } from 'shared/ui/layouts/auth-layout';
import { LoginForm } from 'shared/ui/forms/login';
import { useLoginStrategy } from 'features/session/libs/hooks/useLoginStrategy';

export default function Login() {
  const { t } = useTranslation();
  const router = useRouter();

  const { error, loading, formikProps } = useLoginStrategy({
    onSuccessLogin: () => router.push('/'),
  });

  return (
    <AuthLayout title={t('common:Login')}>
      <Formik {...formikProps}>
        <LoginForm error={error} loading={loading} />
      </Formik>
    </AuthLayout>
  );
}

export const getStaticProps = async ({ locale }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});
