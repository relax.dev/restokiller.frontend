import {
  Box, Container, Grid, Card, Dialog,
} from '@material-ui/core';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { BusinessesFiltersPanel } from 'features/businesses/ui/businesses-filters-panel';
import { BusinessesGrid } from 'entities/business/ui/businesses-grid';
import { BusinessesSearchPanel } from 'features/businesses/ui/businesses-search-panel';

import { MainLayout } from 'shared/ui/layouts/main-layout';

import {
  businessesFiltersSelector,
  businessesSelector,
  fetchAllBusinesses,
  fetchBusinessFeatures,
  fetchBusinessKitchen,
  fetchBusinessPurposes,
  setBusinessesFilters,
} from 'features/businesses/businesses.slice';

import { currentCitySelector } from 'features/countries/countries.slice';
import { GoogleMap } from 'shared/ui/uikit/google-map';
import { useMobile } from 'shared/hooks/useMobile';
import { BusinessesCollectionsButton } from 'entities/business/ui/businesses-collections-button';
import { BusinessesMapButton } from 'entities/business/ui/businesses-map-button';

export default function Home() {
  const businesses = useSelector(businessesSelector);
  const dispatch = useDispatch();

  const currentCity = useSelector(currentCitySelector);

  const filters = useSelector(businessesFiltersSelector);

  useEffect(() => {
    if (!currentCity) return;

    dispatch(
      setBusinessesFilters({
        cities: [currentCity],
      }),
    );
  }, [currentCity]);

  useEffect(() => {
    dispatch(fetchAllBusinesses(filters));
  }, [filters]);

  useEffect(() => {
    dispatch(fetchBusinessPurposes());
    dispatch(fetchBusinessFeatures());
    dispatch(fetchBusinessKitchen());
  }, []);

  console.log(businesses.data);

  const { isMobile } = useMobile();

  const [showMapCard, setShowMapCard] = useState(false);
  const [showMapPopup, setShowMapPopup] = useState(false);

  const handleShowMapPopup = () => {
    setShowMapPopup(true);
  };

  const handleCloseMapPopup = () => {
    setShowMapPopup(false);
  };

  const handleToggleMapCard = () => {
    setShowMapCard(!showMapCard);
  };

  return (
    <MainLayout>
      <Container maxWidth="xl">
        <Box mt={5} mx={isMobile ? 0 : 6}>
          <Grid container spacing={4}>
            <Grid item xs={12} md={4} lg={3} xl={3}>
              <Box>
                <BusinessesCollectionsButton />
              </Box>

              <Box my={4}>
                <BusinessesMapButton onClick={handleShowMapPopup} />
              </Box>

              <Box position="sticky" top={90}>
                <BusinessesFiltersPanel />
              </Box>

            </Grid>
            <Grid item xs={12} md={8} lg={9} xl={9}>
              <BusinessesSearchPanel
                showMapCard={showMapCard}
                toggleMapCard={handleToggleMapCard}
              />
              <Dialog open={showMapPopup} onClose={handleCloseMapPopup} fullWidth maxWidth="xl">
                <Box height="100vh">
                  <GoogleMap
                    markers={businesses.data.map((item) => ({
                      position: item.location,
                      title: item.name,
                    }))}
                  />
                </Box>
              </Dialog>
              { showMapCard && (
              <Box mt={2}>
                <Card elevation={0} style={{ height: 500 }}>
                  <GoogleMap
                    markers={businesses.data.map((item) => ({
                      position: item.location,
                      title: item.name,
                    }))}
                  />
                </Card>
              </Box>
              ) }
              <Box mt={4}>
                <BusinessesGrid businesses={businesses} />
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </MainLayout>
  );
}

export const getStaticProps = async ({ locale }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});
