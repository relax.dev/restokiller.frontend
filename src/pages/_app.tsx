import { useEffect } from 'react';
import type { AppContext, AppProps } from 'next/app';

import { ThemeProvider } from '@material-ui/core/styles';

import { CssBaseline } from '@material-ui/core';
import { appWithTranslation } from 'next-i18next';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { Provider as StoreProvider } from 'react-redux';

import Router from 'next/router';
import NProgress from 'nprogress'; // nprogress module
import Head from 'next/head';
import MomentUtils from '@date-io/moment';

import App from 'next/app';
import { NotificationProvider } from 'shared/providers/notification-provider';
import { store, useStore } from 'shared/modules/store';
import { AuthProvider } from 'shared/providers/auth-provider';
import { ProfileProvider } from 'shared/providers/profile-provider';
import { dark } from 'shared/themes/dark';
import SwiperCore, {
  Autoplay,
  Pagination,
} from 'swiper/core';
import nextI18NextConfig from '../../next-i18next.config.js';
import 'swiper/swiper.min.css';
import 'swiper/components/pagination/pagination.min.css';
import './_app.css';
import 'nprogress/nprogress.css'; // styles of nprogress

SwiperCore.use([Pagination, Autoplay]);
Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

Router.events.on('hashChangeStart', () => NProgress.start());
Router.events.on('hashChangeComplete', () => NProgress.done());

function MyApp({ Component, pageProps }: AppProps) {
  useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement?.removeChild(jssStyles);
    }
  }, []);

  const reduxStore = useStore(pageProps.initialReduxState);

  return (
    <>
      <Head>
        <title>Relax</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
        <script
          type="text/javascript"
          src={process.env.NEXT_STATIC_GOOGLE_MAPS_API_URL}
        />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
          rel="stylesheet"
        />
      </Head>
      <NotificationProvider>
        <StoreProvider store={reduxStore}>
          <ThemeProvider theme={dark}>
            <MuiPickersUtilsProvider utils={MomentUtils} locale="ru">
              <AuthProvider>
                <ProfileProvider>
                  <>
                    <CssBaseline />
                    <Component {...pageProps} />
                  </>
                </ProfileProvider>
              </AuthProvider>
            </MuiPickersUtilsProvider>
          </ThemeProvider>
        </StoreProvider>
      </NotificationProvider>
    </>
  );
}

MyApp.getInitialProps = async (appContext: AppContext) => {
  const appProps = await App.getInitialProps(appContext);

  const { dispatch } = store;
  // there u can dispatch some actions.

  appProps.pageProps = {
    ...appProps.pageProps,
    initialReduxState: store.getState(),
  };

  return appProps;
};
export default appWithTranslation(MyApp, nextI18NextConfig);
