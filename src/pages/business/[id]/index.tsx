import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { MainLayout } from 'shared/ui/layouts/main-layout';
import { BusinessFeature } from 'features/business/ui/business';
import { store } from 'shared/modules/store';
import { resetBusiness } from 'features/business/business.slice';

export default function BusinessPage() {
  return (
    <MainLayout>
      <BusinessFeature />
    </MainLayout>
  );
}

export const getStaticPaths = () => ({
  paths: [],
  fallback: true,
});

export const getStaticProps = async ({ locale, id }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});
