import {
  Stepper,
  Step,
  StepLabel,
  Paper,
  Card,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'next-i18next';

import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { Formik } from 'formik';
import { useState } from 'react';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { AuthLayout } from 'shared/ui/layouts/auth-layout';

import { validationSchema } from 'shared/ui/forms/registration/validation.schema';
import { registration } from 'shared/api/user';
import { RegistrationForm } from 'shared/ui/forms/registration';

const useStyles = makeStyles((theme) => ({
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
    width: '50px',
    height: '50px',
  },
  paper: {
    padding: theme.spacing(2),
    width: '100%',
    boxSizing: 'border-box',
    textAlign: 'center',
    marginTop: theme.spacing(1),
  },
  icon: {
    width: '30px',
    height: '30px',
  },
  stepper: {
    width: '100%',
    padding: 0,
    backgroundColor: 'transparent',
    margin: theme.spacing(3, 0, 1),
  },
  description: {
    fontSize: '14px',
    margin: theme.spacing(1),
  },
}));

export default function Registration(props: any) {
  const classes = useStyles();
  const { t } = useTranslation();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);

  const [activeStep, setActiveStep] = useState(0);

  const steps = ['Создание профиля', 'Подтверждение'];

  function getStepContent(step: number) {
    switch (step) {
      case 0:
        return <RegistrationForm loading={loading} error={error} />;
      case 1:
        return (
          <Paper className={classes.paper}>
            <CheckCircleIcon className={classes.icon} />

            <Typography component="h1" variant="h6">
              Успешная регистрация
            </Typography>
            <Typography component="p" className={classes.description}>
              Ссылка для подтверждения профиля отправлена на вашу почту. Для
              активации профиля, пожалуйста, перейдите по ней.
            </Typography>
          </Paper>
        );
      default:
        return <div>???</div>;
    }
  }

  const formikProps = {
    initialValues: {
      email: '',
      password: '',
      firstName: '',
      lastName: '',
      phone: '',
    },
    validationSchema: validationSchema({ t }),
    onSubmit: (values: any) => {
      setError(null);
      setLoading(true);
      registration(values)
        .then((data) => {
          setActiveStep(1);
        })
        .catch((e) => {
          setError(e.message);
        })
        .finally(() => {
          setLoading(false);
        });
    },
  };

  return (
    <AuthLayout title={t('common:Sign Up')}>
      <Stepper
        activeStep={activeStep}
        className={classes.stepper}
        alternativeLabel
      >
        {steps.map((label, index) => (
          <Step key={label} completed={activeStep > index}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>

      <Formik {...formikProps}>{getStepContent(activeStep)}</Formik>
    </AuthLayout>
  );
}

export const getStaticProps = async ({ locale, ...ctx }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});
