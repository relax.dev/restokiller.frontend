import { useMemo } from 'react';
import { configureStore } from '@reduxjs/toolkit';

import { AnyObjectSchema } from 'yup';
import business from 'features/business/business.slice';
import countries from 'features/countries/countries.slice';
import businesses from 'features/businesses/businesses.slice';
import businessesAdministration from 'features/businesses-administration/businesses-administration.slice';
import tablesManagement from 'features/tables-management/tables-management.slice';
import businessesManagement from 'features/businesses-management/businesses-management.slice';
import businessManagement from 'features/business-management/business-management.slice';
import businessBookings from 'features/business-bookings/business-bookings.slice';
import { useDeepMemo } from '../hooks/useDeepMemo';

export const createStore = (preloadedState: any) => configureStore({
  reducer: {
    business,
    countries,
    businesses,
    businessesAdministration,
    tablesManagement,
    businessesManagement,
    businessManagement,
    businessBookings,
  },
  preloadedState,
});

let existedStore: ReturnType<typeof createStore>;

const initializeStore = (preloadedState: any) => {
  let store = existedStore || createStore(preloadedState);

  if (preloadedState && existedStore) {
    store = createStore({ ...existedStore.getState(), ...preloadedState });
    existedStore = undefined as any;
  }

  if (typeof window === 'undefined') return store;
  if (!existedStore) existedStore = store;

  return store;
};

export function useStore(initialState: AnyObjectSchema) {
  const instance = useDeepMemo(
    () => initializeStore(initialState),
    [initialState],
  );
  return instance;
}

export const store = initializeStore({});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
