import { createMuiTheme } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';

export const dark = createMuiTheme({
  overrides: {
    MuiCssBaseline: {
      '@global': {
        html: {
          WebkitFontSmoothing: 'auto',
        },
        '#nprogress .bar': {
          zIndex: 99999,
        },
      },
    },
    MuiBackdrop: {
      root: {
        backdropFilter: 'blur(4px)',
      },
    },
    MuiStepper: {
      root: {
        background: 'transparent',
      },
    },
    MuiTooltip: {
      tooltip: {
        fontSize: '14px',
      },
    },
    MuiPaper: {
      rounded: {
        borderRadius: '8px',
      },
    },
    MuiListItem: {
      root: {
        '&$selected': {
          backgroundColor: 'rgba(0, 171, 85, 0.25)',
          borderRight: '3px solid rgb(0, 171, 85)',
        },
      },
    },
    MuiButton: {
      root: {
        minHeight: '48px',
        borderRadius: '8px',
      },
    },
  },
  palette: {
    type: 'dark',
    primary: {
      main: 'rgb(0, 171, 85)',
    },
    secondary: {
      main: 'rgb(255, 72, 66)',
    },
    background: {
      default: 'rgb(22, 28, 36)',
      paper: 'rgb(33, 43, 54)',
    },
  },
  typography: {
    fontFamily: 'Public Sans, sans-serif',
  },
  props: {
    MuiTextField: {
      variant: 'outlined',
      margin: 'normal',
    },
    MuiButton: {
      variant: 'contained',
      color: 'primary',
      style: {
        textTransform: 'none',
      },
    },
    MuiAppBar: {
      style: {
        // backdropFilter: 'blur(6px)',
        backgroundColor: 'rgba(22, 28, 36, 0.9)',
      },
    },
  },

});
