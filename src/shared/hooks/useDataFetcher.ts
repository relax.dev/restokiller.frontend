import { useCallback, useEffect, useState } from 'react';

export interface IUseDataFetcher<T> {
  api: () => Promise<any>,
  defaultData: T,
}

export interface IUseDataFetcherResult<T> {
  data: T;
  loaded: boolean;
  dirty: boolean;
  error: string | null;
  refresh: () => void;
  reset: () => void;
}

export const useDataFetcher = <T>(
  { api, defaultData }: IUseDataFetcher<T>,
): IUseDataFetcherResult<T> => {
  const [data, setData] = useState<T>(defaultData);
  const [loaded, setLoaded] = useState(false);
  const [dirty, setDirty] = useState(false);
  const [error, setError] = useState(null);

  const reset = useCallback(() => {
    setLoaded(false);
    setDirty(false);
    setError(null);
    setData(defaultData);
  }, []);

  const refresh = useCallback(() => {
    setLoaded(false);

    api()
      .then((response) => {
        setData(response.data);
        setDirty(true);
      })
      .catch((err) => {
        setError(err.message);
      })
      .finally(() => {
        setLoaded(true);
      });
  }, [api]);

  useEffect(() => {
    refresh();
  }, []);

  return {
    data,
    loaded,
    dirty,
    error,
    refresh,
    reset,
  };
};
