import { useMediaQuery, useTheme } from '@material-ui/core';

export const useMobile = () => {
  const theme = useTheme();
  const isMobile: boolean = useMediaQuery(theme.breakpoints.down('md'));

  return {
    isMobile,
  };
};
