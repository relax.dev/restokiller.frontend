import { DependencyList, useMemo } from 'react';
import { useDeepCompareMemoize } from '../helpers/objects';

export function useDeepMemo<T>(callback: () => T, dependencies: DependencyList | undefined): T {
  return useMemo(
    callback,
    dependencies?.map(useDeepCompareMemoize),
  );
}
