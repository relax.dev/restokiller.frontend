import { useContext } from 'react';
import { IProfileContext, ProfileContext } from '../providers/profile-provider';

export const useProfile = () => {
  const profile: IProfileContext = useContext(ProfileContext);

  return {
    profile,
  };
};
