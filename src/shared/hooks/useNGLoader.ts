import { useEffect } from 'react';
import NProgress from 'nprogress';

export const useNGLoader = (loading: boolean) => {
  useEffect(() => {
    if (loading) {
      NProgress.start();
    } else {
      NProgress.done();
    }
  }, [loading]);
};
