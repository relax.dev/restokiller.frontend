import { useContext } from 'react';
import { AuthContext, IAuthContext } from '../providers/auth-provider';

interface IUseSessionResult extends IAuthContext {
  isAuth: boolean;
}

export const useSession = (): IUseSessionResult => {
  const auth = useContext(AuthContext);

  return {
    ...auth,
    isAuth: auth.token !== null,
  };
};
