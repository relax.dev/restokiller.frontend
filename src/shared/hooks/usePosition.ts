import { useEffect, useState } from 'react';

interface IPosition {
  longitude: string,
  latitude: string;
}

export const usePosition = () => {
  const [position, setPosition] = useState<IPosition>({
    longitude: '',
    latitude: '',
  });

  const [error, setError] = useState<string | null>(null);

  const onChange = ({ coords }: any) => {
    setPosition({
      latitude: coords.latitude,
      longitude: coords.longitude,
    });
  };
  const onError = (err: any) => {
    setError(err.message);
  };

  useEffect(() => {
    const geo = navigator.geolocation;
    if (!geo) {
      setError('Geolocation is not supported');
      return;
    }
    const watcher = geo.watchPosition(onChange, onError);
    return () => geo.clearWatch(watcher);
  }, []);

  return { ...position, error };
};
