export const addFetchableDataCase = (builder: any, fetch: any, field: string) => {
  builder.addCase(fetch.pending, (state: any, action: any) => {
    state[field].loaded = false;
  });
  builder.addCase(fetch.rejected, (state: any, action: any) => {
    state[field].loaded = true;
    state[field].error = action.payload;
    state[field].data = [];
  });
  builder.addCase(fetch.fulfilled, (state: any, action: any) => {
    state[field].data = action.payload || [];
    state[field].loaded = true;
    state[field].dirty = true;
  });
};
