import moment from 'moment';

import { IDay } from 'entities/business/libs/types';

moment.locale('ru');

export const getCurrentDay = (): IDay => (moment().locale('en').format('dddd').toLowerCase() as IDay);
export const formatToHours = (date: string): string => moment(date).format('HH:mm');
export const formatTime = (time: number): string => (time < 10 ? `0${time}` : `${time}`);
export const extractHoursAndMinutes = (time: string) => {
  const [timeHours, timeMinutes] = time.split(':').map((_time) => Number(_time));
  return [timeHours, timeMinutes];
};

export const getTimesBetweenDates = (
  startDate: moment.Moment,
  endDate: moment.Moment,
  interval: { value: number; type: string },
  includePast: boolean,
) => {
  const now = startDate.clone();
  const dates = [];

  while (now.isSameOrBefore(endDate)) {
    dates.push(now.clone());
    now.add(interval.value as any, interval.type as any);
  }

  const result = !includePast ? dates.filter((date) => moment().diff(date) < 0) : dates;

  return result.map((date) => date.format('HH:mm'));
};

export interface IGetTimesBetweenHours {
  startTime: string;
  endTime: string;
  interval: {
    value: number;
    type: string;
  }
  includePast: boolean;
}

export const getTimesBetweenHours = ({
  startTime,
  endTime,
  interval,
  includePast,
}: IGetTimesBetweenHours) => {
  const now = moment(new Date());

  const startDate = now.clone();
  const endDate = now.clone();

  const [startHours, startMinutes] = extractHoursAndMinutes(startTime);
  const [endHours, endMinutes] = extractHoursAndMinutes(endTime);

  startDate.set('hours', startHours);
  startDate.set('minutes', startMinutes);

  endDate.set('hours', endHours);
  endDate.set('minutes', endMinutes);

  if (endHours < startHours) {
    endDate.add(1, 'day');
  }

  return getTimesBetweenDates(startDate, endDate, interval, includePast);
};

export const getMinutesCountBetweenTimes = (startTime = '00:00', endTime = '23:59') => {
  const [startHours, startMinutes] = extractHoursAndMinutes(startTime);
  const [endHours, endMinutes] = extractHoursAndMinutes(endTime);

  const now = moment(new Date());

  const startDate = now.clone();
  const endDate = now.clone();

  startDate.set('hours', startHours);
  startDate.set('minutes', startMinutes);

  endDate.set('hours', endHours);
  endDate.set('minutes', endMinutes);

  if (startHours > endHours) {
    endDate.add(1, 'day');
  }

  return endDate.diff(startDate) / 60 / 1000;
};

export const generateTimesArrayFromMinutes = (minutes: number, interval = 30) => {
  let usedMinutes = interval;
  const result = [];

  while (usedMinutes <= minutes) {
    const originHours = Math.floor(usedMinutes / 60);
    const originMinutes = usedMinutes % 60;

    result.push(`${formatTime(originHours)}:${formatTime(originMinutes)}`);
    usedMinutes += 30;
  }

  return result;
};

export const localizedMoment = moment;
