import { chunk, shuffle } from 'lodash';

export const gridGenerator = (data: any[], keyField: string, renderer: any) => {
  const preparedArray = chunk(data, 3);

  const renderers = [];

  for (const row of preparedArray) {
    const sizes = shuffle([3, 4, 5]);

    for (const item of row) {
      const size = sizes.shift();

      renderers.push(renderer({ ...item, key: item[keyField] }, size));
    }
  }

  return renderers;
};
