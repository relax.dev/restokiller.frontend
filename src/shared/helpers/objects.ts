import { useRef } from 'react';
import isEqual from 'lodash/isEqual';

export function deepCompareEquals(a: object, b: object) {
  return isEqual(a, b);
}

export function useDeepCompareMemoize(value: object) {
  const ref = useRef({});

  if (!deepCompareEquals(value, ref.current)) {
    ref.current = value;
  }

  return ref.current;
}
