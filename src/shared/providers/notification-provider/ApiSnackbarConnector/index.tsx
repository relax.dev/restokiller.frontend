import { useSnackbar } from 'notistack';
import { useEffect } from 'react';
import { api } from '../../../api';

export function ApiSnackbarConnector(): null {
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    api.interceptors.response.use((response) => response, (e) => Promise.reject(e));
  }, [enqueueSnackbar, api]);

  return null;
}
