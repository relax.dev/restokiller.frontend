import React, { ReactElement } from 'react';
import { SnackbarProvider } from 'notistack';
import Slide from '@material-ui/core/Slide';

interface INotification {
  children: ReactElement;
}

export function NotificationProvider({ children }: INotification): ReactElement {
  return (
    <SnackbarProvider
      maxSnack={3}
      autoHideDuration={3000}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      TransitionComponent={Slide as any}
      preventDuplicate
    >
      {children}
    </SnackbarProvider>
  );
}
