import {
  createContext, useEffect, useState, useCallback,
} from 'react';
import { IUser } from 'entities/user/libs/types';
import { IUseDataFetcherResult, useDataFetcher } from 'shared/hooks/useDataFetcher';
import { useSession } from '../../hooks/useSession';
import { getCurrentUser } from '../../api/user';

export type IProfileContext = IUseDataFetcherResult<IUser | null>;

export const ProfileContext = createContext<IProfileContext>({
  data: null,
  loaded: false,
  dirty: false,
  error: null,
  refresh: () => {},
  reset: () => {},
});

export const ProfileProvider = ({ children }: any) => {
  const { isAuth } = useSession();

  const fetcher = useCallback(() => {
    if (isAuth) {
      return getCurrentUser();
    }
    return Promise.reject(new Error('not auth'));
  }, [isAuth]);

  const {
    data, loaded, dirty, error, refresh, reset,
  } = useDataFetcher<IUser | null>({
    api: fetcher,
    defaultData: null,
  });

  const context: IProfileContext = {
    data,
    refresh,
    loaded,
    dirty,
    error,
    reset,
  };

  useEffect(() => {
    if (isAuth) {
      refresh();
    } else {
      reset();
    }
  }, [isAuth]);

  return (
    <ProfileContext.Provider value={context}>
      {children}
    </ProfileContext.Provider>
  );
};
