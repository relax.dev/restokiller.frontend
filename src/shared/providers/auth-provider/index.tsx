import { useRouter } from 'next/router';
import { useCallback, useState, createContext } from 'react';

import { useCookie } from '../../hooks/useCookie';

const cookieName = process?.env?.NEXT_STATIC_COOKIE_TOKEN_NAME || 'token';

export interface IHandleOpenLoginPopup {
  onSuccessLogin: () => void
}
export interface IAuthContext {
  token: string | null;
  handleOpenLoginPopup: (params: IHandleOpenLoginPopup) => void;
  handleCloseLoginPopup: () => void;
  loginCallback: (() => void) | null;
  login: (accessToken: string) => void;
  logout: () => void;
  openedLoginPopup: boolean;
}

export const AuthContext = createContext<IAuthContext>({
  token: null,
  handleOpenLoginPopup: () => {},
  handleCloseLoginPopup: () => {},
  loginCallback: null,
  openedLoginPopup: false,
  login: (accessToken) => {},
  logout: () => {},
});

export const AuthProvider = ({ children }: any) => {
  const [token, setToken, deleteToken] = useCookie(cookieName);
  const [openedLoginPopup, setOpenedLoginPopup] = useState<boolean>(false);
  const [loginCallback, setLoginCallback] = useState<(() => void) | null>(null);

  const handleOpenLoginPopup = useCallback(({ onSuccessLogin }: { onSuccessLogin: () => void }) => {
    setOpenedLoginPopup(true);
    setLoginCallback(() => onSuccessLogin);
  }, []);

  const handleCloseLoginPopup = useCallback(() => {
    setOpenedLoginPopup(false);
    setLoginCallback(null);
  }, []);

  const login = useCallback((accessToken: string) => {
    setToken(accessToken);
  }, []);

  const logout = useCallback(() => {
    deleteToken();
  }, []);

  const value = {
    token,
    loginCallback,
    handleOpenLoginPopup,
    handleCloseLoginPopup,
    openedLoginPopup,
    login,
    logout,
  };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};
