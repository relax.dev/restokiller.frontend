import { BusinessRole } from 'entities/user/libs/types';
import { api } from './index';

export interface IBusinessesFilter {
  cities?: string[];
  features?: string[];
  kitchen?: string[];
  purposes?: string[];
}
interface IGetAllBusinesses {
  filters?: IBusinessesFilter;
}

interface IGetBusinessWorkload {
  id: string,
  filters?: {
    startDate: Date;
    endDate: Date;
  }
}

interface IAddBusinessWorker {
  email: string;
  role: BusinessRole;
}

export const getBusinessById = async (id: string) => api.get(`businesses/${id}`);
export const getBusinessForManagementById = async (id: string) => api.get(`businesses/${id}/management`);

export const getAllBusinesses = async (body: IGetAllBusinesses) => api.get('businesses', {
  params: body,
});

export const getBusinessFeatures = async () => api.get('businesses/features');

export const getBusinessKitchen = async () => api.get('businesses/kitchen');

export const getBusinessPurposes = async () => api.get('businesses/purposes');

export const getBusinessCategories = async () => api.get('businesses/categories');

export const experementalUpload = async (body: any) => api.post('businesses', body, {
  headers: { 'Content-Type': 'multipart/form-data' },
});

export const deleteBusinessById = async (id: string) => api.delete(`businesses/${id}`);

export const confirmVerification = async (body: { id: string }) => api.post('businesses/confirm-verification', body);

export const getWorkload = async ({ id, filters }: IGetBusinessWorkload) => api.get(`businesses/workload/${id}`, {
  params: {
    filters,
  },
});

export const getOwnerBusinesses = async () => api.get('businesses/owner');

export interface IRemoveBusinessPhoto {
  id: string;
  url: string;
}

export interface IAddBusinessPhoto {
  id: string;
  photo: File;
}

export const deleteBusinessPhoto = async ({ id, url }: IRemoveBusinessPhoto) => api.delete(`businesses/${id}/photos`, {
  params: {
    url,
  },
});

export const addBusinessPhoto = async ({ id, photo }: IAddBusinessPhoto) => api.post(`businesses/${id}/photos`, {
  photo,
}, {
  headers: { 'Content-Type': 'multipart/form-data' },
});

export const updateBusinessHeaderPhoto = async ({ id, photo }: IAddBusinessPhoto) => api.put(`businesses/${id}/photos/header`, {
  photo,
}, {
  headers: { 'Content-Type': 'multipart/form-data' },
});
// @todo Сделать интерфейс создания бизнеса.
export const updateBusinessById = async (id: string, data: any) => api.put(`businesses/${id}`, data);

export const addBusinessWorker = async (id: string, data: IAddBusinessWorker) => api.post(`businesses/${id}/users`, data);
export const removeBusinessWorker = async (id: string, userId: string) => api.delete(`businesses/${id}/users/${userId}`);
