import { api } from './index';

export interface IPlacesFilters {
  businesses?: string[];
}
export interface IGetAllPlaces {
  filters?: IPlacesFilters
}

export interface ICreatePlace {
  name: string;
  business: string;
}

export const getAllPlaces = async (body: IGetAllPlaces) => api.get('places', { params: body });

export const createPlace = async (body: ICreatePlace) => api.post('places', body);
