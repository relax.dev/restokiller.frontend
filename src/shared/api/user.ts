import { IUser } from 'entities/user/libs/types';
import { api } from './index';

export interface Credentials {
  email: string,
  password: string
}

export const loginByEmailAndPassword = async (credentials: Credentials) => api.post('auth/login', credentials);

export const getAllUsers = async () => api.get('users');

interface UserRegistrationDto {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  phone: string;
}

export const registration = async (data: UserRegistrationDto) => api.post('users', data);

export const verify = async (token: string) => api.post(`auth/verify/${token}`);

export const getCurrentUser = async () => api.get('users/profile');
export const updateCurrentUser = async (data: Partial<IUser>) => api.put('users/profile', data);

export const getUserById = async (id: string) => api.get(`users/${id}`);
