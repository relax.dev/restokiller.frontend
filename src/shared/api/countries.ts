import { api } from './index';

export const getCitiesByIso3 = async (iso3: string) => api.get(`countries/cities/${iso3}`);

export const getNearestCities = async (latitude: string, longitude: string) => api.get(`countries/cities/nearest?longitude=${longitude}&latitude=${latitude}`);
