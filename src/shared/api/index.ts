import axios from 'axios';
import Cookies from 'js-cookie';
import getConfig from 'next/config';

const { publicRuntimeConfig } = getConfig();

const toFormData = function (model: any, form?: any, namespace?: any) {
  const formData = form || new FormData();

  if (model instanceof File || typeof model === 'string') {
    formData.append(namespace, model);
    return formData;
  }

  for (const propertyName in model) {
    if (!model.hasOwnProperty(propertyName) || !model[propertyName]) continue;
    const formKey = namespace ? `${namespace}[${propertyName}]` : propertyName;
    if (model[propertyName] instanceof Date) {
      formData.append(formKey, model[propertyName].toISOString());
    } else if (Array.isArray(model[propertyName])) {
      model[propertyName].forEach((element: any, index: any) => {
        const tempFormKey = `${formKey}[]`;
        toFormData(element, formData, tempFormKey);
      });
    } else if (typeof model[propertyName] === 'object' && !(model[propertyName] instanceof File)) toFormData(model[propertyName], formData, formKey);
    else formData.append(formKey, model[propertyName]);
  }
  return formData;
};

export const apiV1 = axios.create({
  baseURL: `${publicRuntimeConfig.API_URL}`,
  headers: {
    'Content-Type': 'application/json',
  },
  transformRequest: [function (data, headers) {
    if (headers['Content-Type'] && headers['Content-Type'].startsWith('multipart/form-data')) {
      return toFormData(data);
    }

    try {
      return JSON.stringify(data);
    } catch (e) {
      return data;
    }
  }],

});

export const api = apiV1;

api.interceptors.request.use((config) => {
  const token = Cookies.get(process?.env?.NEXT_STATIC_COOKIE_TOKEN_NAME || 'token');
  // eslint-disable-next-line no-param-reassign
  config.headers.Authorization = token ? `Bearer ${token}` : '';

  return config;
});
