import { IBooking } from 'entities/bookings/libs/types';
import { api } from './index';

export interface ICreateBooking {
  firstname: string;
  persons: number;
  phone: string;
  email: string;
  startDate: Date;
  endDate: Date;
  description: string;
  table: string;
}

export enum BookingStatusEnum {
  PENDING = 'PENDING', // после бронирования
  APPROVED = 'APPROVED', // после подтверждения, что человек занял столик
  DONE = 'DONE', // после того как пользователь ушел,
  OVERDUE = 'OVERDUE', // просрочено
  CANCELED = 'CANCELED', // закрыто
}

// export const createBookingAsGuest = async (body: ICreateBooking) =>
// api.post('bookings/guest', body);

export const createBooking = async (body: ICreateBooking) => api.post('bookings', body);

export const getAllBookingsForGuestBook = async (businessId: string) => api.get(`bookings/guest-book/${businessId}`);

export const updateBookingStatus = async (id: string, status: BookingStatusEnum) => api.put(`bookings/${id}/status`, {
  status,
});

export const getAllBookingsForCurrentUser = async () => api.get('bookings/user');
