import { api } from './index';

export interface ITablesFilters {
  places?: string[];
  seats?: number;
  startDate?: string;
  endDate?: string;
}

interface IGetAllTables {
  filters?: ITablesFilters
}

interface ICreateTable {
  seats: number;
  photos: File[];
  number: number;
  place: string;
}

export const getAllTables = async (body: IGetAllTables) => api.get('tables', { params: body });

export const createTable = async (body: ICreateTable) => api.post('tables', body, {
  headers: { 'Content-Type': 'multipart/form-data' },
});
