import {
  Typography,
  Avatar,
  Container,
} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
    width: '50px',
    height: '50px',
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
}));

export const AuthLayout = ({ title, children }: any) => {
  const classes = useStyles();
  const settings = {
    logo: {
      url: 'https://sun1-15.userapi.com/s/v1/ig2/MfFqFgtKEMp4lDNFr6LPznEfKC4-evn7xAYUqXvhgoCypF4QFX9FWYMPDtzGX2Ppp2p5fedKAo9cBI--NGCsSpuy.jpg?size=200x0&quality=96&crop=69,64,378,378&ava=1',
    },
  };

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar} src={settings.logo.url} />
        <Typography component="h1" variant="h5">
          {title}
        </Typography>

        {children}
      </div>
    </Container>
  );
};
