import React from 'react';
import { Footer } from 'features/footer/ui/footer';
import { Header } from 'features/header/ui/header';
import { AuthentificationPopup } from 'features/session/ui/authentification-popup';

export const MainLayout = ({ children, SubHeader }: any) => (
  <div>
    <Header />
    <div>{children}</div>
    <Footer />
    <AuthentificationPopup />
  </div>
);
