import React from 'react';
import { SidebarDrawer } from 'features/header/ui/sidebar-drawer';

export const PartnerLayout = ({ children }: any) => (
  <div>
    <SidebarDrawer>{children}</SidebarDrawer>
  </div>
);
