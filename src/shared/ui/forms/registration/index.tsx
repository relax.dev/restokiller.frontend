import { Grid, Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'next-i18next';

import { Form } from 'formik';
import { Alert } from '@material-ui/lab';
import { Button } from '../../uikit/button';
import { Link } from '../../uikit/link';
import { FormikTextField } from '../../uikit/formik-text-field';

const useStyles = makeStyles((theme) => ({
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
}));

interface IRegistrationForm {
  error: string | null;
  loading: boolean;
}

export const RegistrationForm = ({ error, loading }: IRegistrationForm) => {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <Form className={classes.form}>
      <FormikTextField
        placeholder="example@gmail.com"
        fullWidth
        name="email"
        label={t('common:Email')}
      />

      <FormikTextField
        placeholder="******"
        fullWidth
        type="password"
        name="password"
        label={t('common:Password')}
      />

      <FormikTextField
        placeholder="Ivan"
        fullWidth
        name="firstName"
        label={t('common:Firstname')}
      />

      <FormikTextField
        placeholder="Vankin"
        fullWidth
        name="lastName"
        label={t('common:Lastname')}
      />

      <FormikTextField
        placeholder="+79005003344"
        fullWidth
        type="phone"
        name="phone"
        label={t('common:Phone number')}
      />

      <Button
        className={classes.submit}
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
        loading={loading}
      >
        {t('common:Sign Up')}
      </Button>

      {error && (
        <Box mb={1}>
          <Alert severity="error">{error}</Alert>
        </Box>
      )}

      <Grid container>
        <Grid item>
          <Link href="/login">{t('common:Already have account? Log In')}</Link>
        </Grid>
      </Grid>
    </Form>
  );
};
