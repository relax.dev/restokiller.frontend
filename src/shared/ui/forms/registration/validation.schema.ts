import * as yup from 'yup';

const phoneRegExp = /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/;

export const validationSchema = ({ t }: any) => yup.object({
  email: yup
    .string()
    .email(t('common:Enter a valid email'))
    .required(t('common:Email is required')),
  password: yup
    .string()
    .min(8, t('common:Password should be of minimum 8 characters length'))
    .required(t('common:Password is required')),
  firstName: yup
    .string()
    .required(t('common:Firstname is required')),
  lastName: yup
    .string()
    .required(t('common:Lastname is required')),
  phone: yup
    .string()
    .required(t('common:Phone number is required'))
    .matches(phoneRegExp, t('common:Phone number is not valid')),
});
