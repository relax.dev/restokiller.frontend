import {
  Grid, FormControlLabel, Checkbox, Box,
} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'next-i18next';

import { Form, useFormikContext } from 'formik';
import { Alert } from '@material-ui/lab';
import { ArrowForward, NavigateNext } from '@material-ui/icons';
import { Link } from '../../uikit/link';
import { FormikTextField } from '../../uikit/formik-text-field';
import { Button } from '../../uikit/button';

const useStyles = makeStyles((theme) => ({
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
}));

interface ILoginForm {
  error: string | null;
  loading: boolean;
}

export const LoginForm = ({ error, loading }: ILoginForm) => {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <Form className={classes.form}>
      <FormikTextField
        placeholder="+79005003344"
        fullWidth
        type="email"
        name="email"
        label={t('common:Email')}
      />

      <FormikTextField
        placeholder="******"
        fullWidth
        type="password"
        name="password"
        label={t('common:Password')}
      />

      <FormControlLabel
        control={<Checkbox value="remember" color="primary" />}
        label={t('common:Remember me')}
      />

      <Button
        className={classes.submit}
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
        loading={loading}
        startIcon={<ArrowForward />}
      >
        {t('common:Sign In')}
      </Button>

      { error && (
      <Box mb={1}>
        <Alert severity="error">{error}</Alert>
      </Box>
      ) }

      <Grid container>
        <Grid item xs>
          <Link href="/login" variant="body2">
            {t('common:Forgot password?')}
          </Link>
        </Grid>
        <Grid item>
          <Link href="/registration">
            {t("common:Don't have an account? Sign Up")}
          </Link>
        </Grid>
      </Grid>
    </Form>
  );
};
