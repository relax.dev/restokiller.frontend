import * as yup from 'yup';

export const validationSchema = ({ t }: any) => yup.object({
  email: yup
    .string()
    .email(t('common:Enter a valid email'))
    .required(t('common:Email is required')),
  password: yup
    .string()
    .min(8, t('common:Password should be of minimum 8 characters length'))
    .required(t('common:Password is required')),
});
