import { Typography, Box, Grid } from '@material-ui/core';
import { Form, useFormikContext } from 'formik';
import { useTranslation } from 'next-i18next';
import { FC, MouseEventHandler, useEffect } from 'react';
import { Button } from '../../../../uikit/button';

interface IPartnerRegistrationForm {
  content: React.ReactNode;
  label: string;
  handleBack: MouseEventHandler<HTMLButtonElement>;
  handleNext: MouseEventHandler<HTMLButtonElement>;
  activeStep: number;
}

export const PartnerRegistrationMainForm: FC<IPartnerRegistrationForm> = ({
  content,
  label,
  handleBack,
  handleNext,
  activeStep,
}) => {
  const {
    isValid, dirty, errors, touched, setErrors,
  } = useFormikContext();
  const { t } = useTranslation();

  useEffect(() => {
    setErrors({});
  }, [activeStep]);

  const isLastStep = activeStep === 3;

  return (
    <Form>
      <Typography gutterBottom variant="h4">
        {label}
      </Typography>
      {content}
      <Box mt={2}>
        <Grid container spacing={2}>
          <Grid item xs={4}>
            <Button
              fullWidth
              variant="outlined"
              color="inherit"
              onClick={handleBack}
            >
              {activeStep === 0 ? t('common:cancel') : t('common:back')}
            </Button>
          </Grid>
          <Grid item xs={8}>
            <Button
              disabled={!isValid || !dirty}
              fullWidth
              onClick={isLastStep ? undefined : handleNext}
              type={isLastStep ? 'submit' : 'button'}
            >
              {isLastStep ? t('common:Send to review') : t('common:Next step')}
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Form>
  );
};
