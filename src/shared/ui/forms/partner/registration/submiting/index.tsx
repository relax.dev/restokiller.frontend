import { Box, Typography } from '@material-ui/core';

export const PartnerRegistrationSubbmitingForm = () => (
  <Box pt={2} pb={2}>
    <Typography gutterBottom variant="body2" component="p">
      Вы успешно заполнили форму регистрации заведения. Отправьте её на
      проверку, это занимает 1-2 дня рабочего времени. После подтверждения, вы
      получите уведомление на свою электронную почту, а так же увидите это
      заведение в списке активированных в личном кабинете администратора
    </Typography>
  </Box>
);
