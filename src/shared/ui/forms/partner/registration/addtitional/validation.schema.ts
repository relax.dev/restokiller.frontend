import * as yup from 'yup';

export const validationSchema = ({ t }: any) => yup.object({
  averageCheck: yup.number().min(1, t('common:Average check must be greater than 0')).required(t('common:Average check is required')),
  photos: yup.array().min(1, t('common:Please load 1-3 photos')),
});
