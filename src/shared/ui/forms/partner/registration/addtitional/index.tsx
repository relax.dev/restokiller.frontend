import { Grid, MenuItem } from '@material-ui/core';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

import { useTranslation } from 'react-i18next';
import { IFeature, IKitchen, IPurpose } from 'entities/business/libs/types';
import { FormikTextField } from 'shared/ui/uikit/formik-text-field';
import { FormikDropzoneArea } from 'shared/ui/uikit/formik-dropzone-area';
import { useAppSelector } from 'shared/hooks/useRedux';

import {
  featuresSelector,
  kitchenSelector,
  purposesSelector,
} from 'features/businesses/businesses.slice';

export const PartnerRegistrationAdditionalForm = () => {
  const purposes = useAppSelector(purposesSelector);
  const features = useAppSelector(featuresSelector);
  const kitchen = useAppSelector(kitchenSelector);

  const { t } = useTranslation();

  return (
    <Grid container spacing={2}>
      <Grid item xs={6}>
        <FormikTextField
          name="features"
          fullWidth
          label="Особенности"
          select
          SelectProps={{
            multiple: true,
          }}
        >
          {features.data.map((feature: IFeature) => (
            <MenuItem key={feature.name} value={feature.name}>
              {t(`features:${feature.name}`)}
            </MenuItem>
          ))}
        </FormikTextField>
      </Grid>
      <Grid item xs={6}>
        <FormikTextField
          name="purposes"
          fullWidth
          label="Возможности"
          select
          SelectProps={{
            multiple: true,
          }}
        >
          {purposes.data.map((purpose: IPurpose) => (
            <MenuItem key={purpose.name} value={purpose.name}>
              {t(`purposes:${purpose.name}`)}
            </MenuItem>
          ))}
        </FormikTextField>
      </Grid>
      <Grid item xs={6}>
        <FormikTextField
          name="kitchen"
          fullWidth
          label="Варианты кухни"
          select
          SelectProps={{
            multiple: true,
          }}
        >
          {kitchen.data.map((kitchenItem: IKitchen) => (
            <MenuItem key={kitchenItem.name} value={kitchenItem.name}>
              {t(`kitchen:${kitchenItem.name}`)}
            </MenuItem>
          ))}
        </FormikTextField>
      </Grid>
      <Grid item xs={6}>
        <FormikTextField
          type="number"
          name="averageCheck"
          fullWidth
          label="Средний чек"
        />
      </Grid>
      <Grid item xs={12}>
        <FormikDropzoneArea
          acceptedFiles={['image/jpeg', 'image/png', 'image/bmp']}
          maxFileSize={5000000}
          dropzoneText="Загрузие фотографии вашего заведения"
          name="photos"
        />
      </Grid>
    </Grid>
  );
};
