import { Grid } from '@material-ui/core';
import { useFormikContext } from 'formik';

import { useTranslation } from 'react-i18next';
import { FormikTextField } from 'shared/ui/uikit/formik-text-field';
import { useAppSelector } from 'shared/hooks/useRedux';
import { FormikAddressPicker } from 'shared/ui//uikit/formik-address-picker';
import { FormikCityPicker } from 'shared/ui/uikit/formik-city-picker';
import { citiesSelector } from 'features/countries/countries.slice';

export const PartnerRegistrationCommonForm = () => {
  const cities = useAppSelector(citiesSelector);
  const { t } = useTranslation();

  const { values }: any = useFormikContext();

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} md={8}>
        <FormikTextField name="name" fullWidth label="Название" />
      </Grid>
      <Grid item xs={12} md={4}>
        <FormikTextField name="type" fullWidth label="Тип бизнеса" select />
      </Grid>
      <Grid item xs={12} md={12}>
        <FormikTextField
          name="description"
          fullWidth
          label="Краткое описание"
          multiline
          rows={3}
        />
      </Grid>
      <Grid item xs={12} md={4}>
        <FormikCityPicker name="city" fullWidth cities={cities} />
      </Grid>
      <Grid item xs={12} md={8}>
        <FormikAddressPicker
          name="address"
          locationName="location"
          disabled={!values?.city}
          city={t(`cities:${values.city}`)}
        />
      </Grid>
    </Grid>
  );
};
