import * as yup from 'yup';

export const validationSchema = ({ t }: any) => yup.object({
  name: yup
    .string()
    .min(3, t('common:Name min length is 3 symbols'))
    .required(t('common:Name is required')),
  type: yup
    .string(),
  // .required(t("common:Type is required")),
  description: yup
    .string()
    .required(t('common:Description is required')),
  address: yup
    .string()
    .required(t('common:Address is required')),
  location: yup.object().required(t('common:Please select location from list')).nullable(),
});
