import * as yup from 'yup';

export const validationSchema = ({ t }: any) => yup.object({
  workingHours: yup.object().required(),
});
