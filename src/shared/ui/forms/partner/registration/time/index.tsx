import {
  Divider, Box, Grid, useMediaQuery,
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import { useFormikContext } from 'formik';

import { useTranslation } from 'react-i18next';
import { get } from 'lodash';
import { FC } from 'react';
import { FormikTextField } from '../../../../uikit/formik-text-field';
import { FormikSwitch } from '../../../../uikit/formik-switch';

export const PartnerRegistrationTimeForm = () => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

  const { t } = useTranslation();
  const { values } = useFormikContext();

  const week = [
    {
      name: 'monday',
    },
    {
      name: 'tuesday',
    },
    {
      name: 'wednesday',
    },
    {
      name: 'thursday',
    },
    {
      name: 'friday',
    },
    {
      name: 'saturday',
    },
    {
      name: 'sunday',
    },
  ];

  return (
    <>
      {week.map((day) => (
        <>
          <Grid container spacing={2} alignItems="center">
            <Grid item xs={12} md={6}>
              <FormikSwitch
                name={`workingHours.${day.name}.active`}
                controlProps={{
                  labelPlacement: 'end',
                  label: t(`week:${day.name}`),
                }}
              />
            </Grid>
            <Grid item xs={6} md={3}>
              <FormikTextField
                name={`workingHours.${day.name}.start`}
                size="small"
                type="time"
                fullWidth
                margin={isMobile ? 'none' : 'normal'}
                disabled={!get(values, `workingHours.${day.name}.active`)}
              />
            </Grid>
            <Grid item xs={6} md={3}>
              <FormikTextField
                name={`workingHours.${day.name}.end`}
                size="small"
                type="time"
                fullWidth
                margin={isMobile ? 'none' : 'normal'}
                disabled={!get(values, `workingHours.${day.name}.active`)}
              />
            </Grid>
          </Grid>
          {isMobile && (
          <Box pb={2} pt={2}>
            <Divider />
          </Box>
          )}
        </>
      ))}
    </>
  );
};
