import * as yup from 'yup';

export const validationSchema = ({ t }: any) => yup.object({
  name: yup.string().min(1).max(30).required(),
});
