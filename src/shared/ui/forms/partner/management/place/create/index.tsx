import { Grid, Box, MenuItem } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'next-i18next';

import { Form } from 'formik';
import { Alert } from '@material-ui/lab';
import { FC } from 'react';
import { Button } from '../../../../../uikit/button';
import { Link } from '../../../../../uikit/link';
import { FormikTextField } from '../../../../../uikit/formik-text-field';
import { FormikDropzoneArea } from '../../../../../uikit/formik-dropzone-area';

const useStyles = makeStyles((theme) => ({
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
}));

interface IPlaceCreateForm {
  error: string | null;
  loading: boolean;
}

export const PlaceCreateForm: FC<IPlaceCreateForm> = ({ error, loading }) => {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <Form className={classes.form}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FormikTextField
            fullWidth
            name="name"
            margin="none"
            label={t('common:Place name')}
          />
        </Grid>
      </Grid>

      <Button
        className={classes.submit}
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
        loading={loading}
      >
        {t('common:Create place')}
      </Button>

      {error && (
        <Box mb={1}>
          <Alert severity="error">{error}</Alert>
        </Box>
      )}
    </Form>
  );
};
