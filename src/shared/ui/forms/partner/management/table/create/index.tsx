import { Grid, Box, MenuItem } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'next-i18next';

import { Form } from 'formik';
import { Alert } from '@material-ui/lab';
import { Link } from '../../../../../uikit/link';
import { FormikTextField } from '../../../../../uikit/formik-text-field';
import { FormikDropzoneArea } from '../../../../../uikit/formik-dropzone-area';
import { Button } from '../../../../../uikit/button';

const useStyles = makeStyles((theme) => ({
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
}));

interface ITableCreateForm {
  error: string | null;
  loading: boolean;
}

export const TableCreateForm = ({ error, loading }: ITableCreateForm) => {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <Form className={classes.form}>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <FormikTextField
            placeholder="4"
            fullWidth
            name="number"
            type="number"
            margin="none"
            label={t('common:Number')}
            InputProps={{
              startAdornment: <Box mr={1}>№</Box>,
            }}
          />
        </Grid>

        <Grid item xs={6}>
          <FormikTextField
            fullWidth
            name="seats"
            type="number"
            margin="none"
            label={t('common:Seats count')}
          />
        </Grid>

        <Grid item xs={12}>
          <FormikDropzoneArea
            name="photos"
            label={t('common:Table photos')}
          />
        </Grid>

      </Grid>

      <Button
        className={classes.submit}
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
        loading={loading}
      >
        {t('common:Create table')}
      </Button>

      {error && (
        <Box mb={1}>
          <Alert severity="error">{error}</Alert>
        </Box>
      )}
    </Form>
  );
};
