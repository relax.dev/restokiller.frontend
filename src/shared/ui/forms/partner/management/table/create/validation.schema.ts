import * as yup from 'yup';

export const validationSchema = ({ t }: any) => yup.object({
  seats: yup.number().min(1).max(20).required(),
  photos: yup.array().min(1).max(3).required(),
  number: yup.number().required(),
  place: yup.string().required(),
});
