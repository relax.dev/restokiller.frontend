import { Typography, Box } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { SentimentDissatisfied } from '@material-ui/icons';

const useStyles = makeStyles((theme: Theme) => createStyles({}));

export const ErrorContainer = () => (
  <Box textAlign="center" mt={4}>

    <SentimentDissatisfied style={{ width: 100, height: 100 }} />
    <Typography gutterBottom variant="h2">Ooooops....</Typography>
    <Typography gutterBottom variant="h5">Something went wrong</Typography>
    <Typography gutterBottom variant="body2">
      We are already working on solving the problem
    </Typography>
  </Box>
);
