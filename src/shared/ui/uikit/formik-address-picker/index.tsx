import { useFormikContext } from 'formik';
import { get } from 'lodash';
import { useCallback } from 'react';
import { AddressPicker } from '../address-picker';

export const FormikAddressPicker = ({
  id,
  name,
  locationName,
  onBlur,
  ...props
}: any) => {
  const {
    values, touched, errors, setFieldValue, setFieldTouched,
  } = useFormikContext<any>();

  const value = get(values, name);
  const touch = get(touched, name);
  const error = get(errors, name);

  const locationError = get(errors, locationName);

  const handleChange = useCallback(
    (city) => {
      setFieldValue(name, city);
    },
    [name, setFieldValue],
  );

  const handleChangeLocation = useCallback(
    (city) => {
      setFieldValue(locationName, city);
    },
    [locationName, setFieldValue],
  );

  const handleBlurField = useCallback(
    (e) => {
      if (onBlur) onBlur();
      setFieldTouched(name, true);
      setFieldTouched(locationName, true);
    },
    [locationName, name, onBlur, setFieldTouched],
  );

  return (
    <>
      <AddressPicker
        {...props}
        value={value}
        onChange={handleChange}
        onChangeLocation={handleChangeLocation}
        onBlur={handleBlurField}
        error={touch && Boolean(error)}
        locationError={touch && Boolean(locationError)}
        locationHelperText={touch && locationError}
        helperText={touch && error}
      />
    </>
  );
};
