import { CircularProgress } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => createStyles({
  container: {
    position: 'fixed',
    bottom: theme.spacing(4),
    left: 'calc((100vw - 40px) / 2 )',
    zIndex: 999,
  },
}));

export const LoaderBottom = () => {
  const classes = useStyles();

  return (
    <CircularProgress className={classes.container} />
  );
};
