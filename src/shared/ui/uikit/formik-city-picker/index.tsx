import { useFormikContext } from 'formik';
import { get } from 'lodash';
import { useCallback } from 'react';
import { CityPicker } from '../city-picker';

export const FormikCityPicker = ({ id, name, ...props }: any) => {
  const {
    values, touched, errors, setFieldValue,
  } = useFormikContext<any>();

  const handleChange = useCallback(
    (city) => {
      setFieldValue(name, city);
    },
    [name, setFieldValue],
  );

  return (
    <CityPicker value={get(values, name)} onChange={handleChange} {...props} />
  );
};
