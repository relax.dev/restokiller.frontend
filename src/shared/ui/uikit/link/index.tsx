import NextLink from 'next/link';
import MUILink from '@material-ui/core/Link';

export const Link = ({ children, ...props }: any) => (
  <NextLink {...props} passHref>
    <MUILink>{children}</MUILink>
  </NextLink>
);
