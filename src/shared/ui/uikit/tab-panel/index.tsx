import { Box } from '@material-ui/core';
import { FC } from 'react';

export interface ITabPanel {
  index: number;
  value: number;
  children: React.ReactNode,
}

export const TabPanel: FC<ITabPanel> = ({
  value, index, children, ...props
}) => (
  <div
    role="tabpanel"
    hidden={value !== index}
    id={`simple-tabpanel-${index}`}
    aria-labelledby={`simple-tab-${index}`}
    {...props}
  >
    {value === index && (
      <Box>
        {children}
      </Box>
    )}
  </div>
);
