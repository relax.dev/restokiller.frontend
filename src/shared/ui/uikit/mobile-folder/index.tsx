import { Box, Fab, Badge } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  useCallback, useState, FC, Children,
} from 'react';

import { useTranslation } from 'react-i18next';
import { useMobile } from '../../../hooks/useMobile';
import { FullScreenDialog } from '../dialog-fullscreen';

const useStyles = makeStyles((theme: Theme) => createStyles({
  button: {
    position: 'fixed',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
    zIndex: 10,
  },
}));

export interface IMobileFolder {
  children: React.ReactElement;
  title: string;
  icon: React.ReactElement;
  open?: boolean;
  onClose?: () => void;
  onOpen?: () => void;
  showBadge: boolean,
}

export const MobileFolder: FC<IMobileFolder> = ({
  children, open, onClose, onOpen, title, icon, showBadge,
}) => {
  const { t } = useTranslation();
  const { isMobile } = useMobile();
  const classes = useStyles();
  const [localOpen, setLocalOpen] = useState(false);

  const onOpenLocal = useCallback(() => {
    setLocalOpen(true);
  }, []);

  const onCloseLocal = useCallback(() => {
    setLocalOpen(false);
  }, []);

  if (!isMobile) {
    return children;
  }

  console.log(showBadge);

  return (
    <>
      <Badge
        color="secondary"
        badgeContent={showBadge ? 1 : 0}
        variant="dot"
        className={classes.button}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        <Fab color="primary" onClick={onOpen || onOpenLocal}>
          { icon }
        </Fab>
      </Badge>
      <FullScreenDialog open={open || localOpen} onClose={onClose || onCloseLocal} title={title}>
        <Box my="auto" mx={2}>
          { children }
        </Box>
      </FullScreenDialog>
    </>
  );
};
