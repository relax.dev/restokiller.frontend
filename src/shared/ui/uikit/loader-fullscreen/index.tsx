import { createStyles, makeStyles } from '@material-ui/core/styles';
import LoaderSvg from './loader.svg';

const useStyles = makeStyles(() => createStyles({
  container: {
    width: '100%',
    height: '100%',
    minHeight: '600px',
    maxHeight: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

export const LoaderFullScreen = () => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <LoaderSvg style={{ width: '120px', height: '120px' }} />
    </div>
  );
};
