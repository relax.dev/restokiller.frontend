import { FC, memo } from 'react';

import {
  Card, CardContent, CardActionArea,
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { AddCircleOutline, Height } from '@material-ui/icons';

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    height: '100%',
    minHeight: '250px',
    width: '100%',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    position: 'relative',
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionsArea: {
    height: '100%',
    position: 'relative',
  },
}));

export interface ITableCreateButton {
  onClick: (e: any) => void;
  disabled?: boolean;
}

export const CardButton: FC<ITableCreateButton> = memo(({ disabled = false, onClick }) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea disabled={disabled} onClick={onClick} className={classes.actionsArea}>
        <CardContent className={classes.content}>
          <AddCircleOutline style={{ fontSize: 120 }} />
        </CardContent>
      </CardActionArea>
    </Card>
  );
});
