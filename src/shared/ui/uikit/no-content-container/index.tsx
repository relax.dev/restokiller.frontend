import { Typography, Box } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { SentimentDissatisfied } from '@material-ui/icons';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme: Theme) => createStyles({}));

export const NoContentContainer = () => {
  const { t } = useTranslation();
  return (

    <Box textAlign="center" mt={4}>

      <SentimentDissatisfied style={{ width: 100, height: 100 }} />
      <Typography gutterBottom variant="h4">
        { t('common:We didn\'t find anything :(') }
      </Typography>
    </Box>
  );
};
