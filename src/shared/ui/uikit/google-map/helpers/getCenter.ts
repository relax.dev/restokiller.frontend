import { ILocation } from 'entities/business/libs/types';
import { IMarker } from '../index';

export function getCenter(markers: IMarker[]): ILocation | null {
  if (markers.length === 0) {
    return null;
  }

  const coordinatesSum: ILocation = markers.reduce(
    (acc: ILocation, marker: IMarker) => ({
      lng: acc.lng + marker.position.lng,
      lat: acc.lat + marker.position.lat,
    }),
    { lng: 0, lat: 0 },
  );

  return {
    lng: coordinatesSum.lng / markers.length,
    lat: coordinatesSum.lat / markers.length,
  };
}
