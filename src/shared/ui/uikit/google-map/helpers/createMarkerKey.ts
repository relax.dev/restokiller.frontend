import { ILocation } from 'entities/business/libs/types';

export function createMarkerKey(position: ILocation, title: string | undefined): string {
  return `${position.lat}${position.lng}${title}`;
}
