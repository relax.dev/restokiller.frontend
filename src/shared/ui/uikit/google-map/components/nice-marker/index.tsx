import React, { ReactElement, useRef } from 'react';
import { Marker } from '@react-google-maps/api';
import { IMarker } from '../..';

export interface INiceMarker extends IMarker {
  clusterer: any;
  onClick: (marker: any, e: any) => void;
}

export function NiceMarker({
  position,
  icon,
  title,
  clusterer,
  onClick,
}: INiceMarker): ReactElement {
  const marker = useRef();

  return (
    <Marker
      onLoad={(el) => {
        marker.current = el;
      }}
      onClick={(e) => onClick(marker.current, e)}
      icon={icon}
      title={title}
      position={position}
      clusterer={clusterer}
    />
  );
}
