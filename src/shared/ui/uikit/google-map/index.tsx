import React, {
  ReactElement, useMemo, useState, useCallback,
} from 'react';
import { GoogleMap as GMap, InfoWindow, MarkerClusterer } from '@react-google-maps/api';
import { ILocation } from 'entities/business/libs/types';
import { getCenter } from './helpers/getCenter';
import { createMarkerKey } from './helpers/createMarkerKey';
import { styles } from './themes/dark';
import { INiceMarker, NiceMarker } from './components/nice-marker';

const containerStyle = {
  width: '100%',
  height: '100%',
};

export interface IMarker {
  position: ILocation;
  icon?: string;
  title?: string;
}
export interface IGoogleMap {
  center?: ILocation;
  markers: IMarker[];
}

export const GoogleMap = ({ center, markers }: IGoogleMap): ReactElement | null => {
  const [currentMarker, setCurrentMarker] = useState<INiceMarker | null>(null);

  const memoisedCenter = useMemo(() => center || getCenter(markers), [markers]);
  const onMapClick = useCallback((e) => {
    if (e.placeId) {
      setCurrentMarker(null);
    }
  }, []);

  return memoisedCenter ? (
    <GMap
      mapContainerStyle={containerStyle}
      center={memoisedCenter}
      zoom={14}
      onClick={onMapClick}
      tilt={45}
      options={{
        styles,
        disableDefaultUI: true,
        zoomControl: true,
        fullscreenControl: true,
        gestureHandling: 'greedy',
      }}
    >
      <MarkerClusterer>
        {(clusterer) => markers.map(({ position, title, icon }) => (
          <NiceMarker
            onClick={(marker) => {
              setCurrentMarker(null);
              setCurrentMarker(marker);
            }}
            icon={icon}
            position={position}
            title={title}
            clusterer={clusterer}
            key={createMarkerKey(position, title || 'center')}
          />
        ))}
      </MarkerClusterer>

      {currentMarker && (
        <InfoWindow onCloseClick={() => setCurrentMarker(null)} anchor={currentMarker}>
          <div style={{ color: 'black' }}>{currentMarker.title}</div>
        </InfoWindow>
      )}
    </GMap>
  ) : null;
};

export default React.memo(GoogleMap);
