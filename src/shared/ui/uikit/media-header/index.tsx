import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { ReactNode, FC } from 'react';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    position: 'relative',
    overflow: 'hidden',
    boxShadow: 'inset 0 -4px 15px 0px #0000003b',
  },
  background: {
    backgroundSize: 'cover',
    filter: 'blur(6px) brightness(0.5)',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
  content: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

export interface IMediaHeader {
  children: ReactNode;
  image: string;
  height: number;
}

export const MediaHeader: FC<IMediaHeader> = ({ children, image, height }) => {
  const { t } = useTranslation();
  const classes = useStyles();

  return (
    <div className={classes.root} style={{ height: `${height}px` }}>
      <div className={classes.background} style={{ backgroundImage: `url(${image})` }} />
      <div className={classes.content}>
        { children }
      </div>
    </div>
  );
};
