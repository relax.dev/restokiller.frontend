import React, { useCallback, FC } from 'react';
import { Typography, Box } from '@material-ui/core';

import { useFormikContext } from 'formik';
import { get } from 'lodash';
import { DropzoneArea, DropzoneAreaProps } from 'material-ui-dropzone';

export interface IFormikDropzoneArea extends DropzoneAreaProps {
  name: string;
  id?: string;
  label?: string;
}

export const FormikDropzoneArea: FC<IFormikDropzoneArea> = ({ id, name, ...props }: any) => {
  const {
    values, errors, setFieldValue,
  } = useFormikContext<any>();

  const value = get(values, name);
  const error = get(errors, name);

  const handleChange = useCallback(
    (files: any[]) => {
      setFieldValue(name, files);
    },
    [name, setFieldValue],
  );

  return (
    <>
      <DropzoneArea
        inputProps={{ id: id || name, name }}
        initialFiles={value}
        onChange={handleChange}
        {...props}
      />
      {error && (
        <Box mt={1}>
          <Typography variant="body2" color="textSecondary">
            {error}
          </Typography>
        </Box>
      )}
    </>
  );
};
