import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Box,
  Menu,
  MenuItem,
  Typography,
  LinearProgress,
  Dialog,
} from '@material-ui/core';
import { useTranslation } from 'next-i18next';
import {
  LocationCity,
} from '@material-ui/icons';
import {
  useCallback, useEffect, useRef, useState,
} from 'react';
import { FixedSizeList } from 'react-window';
import { SearchField } from '../search-field';
import { Button } from '../button';

const useStyles = makeStyles((theme: Theme) => createStyles({
  button: { height: 56, margin: theme.spacing(2, 0, 1, 0) },
}));

export const renderRow = ({ t, filteredCities, onSelectCity }: any) => ({ index, style }: any) => {
  const city = filteredCities[index].name;
  return (
    <MenuItem key={index} style={style} onClick={onSelectCity(city)}>
      <Typography variant="body2">
        <Box display="flex" alignItems="center">
          <Box mr={1}>
            <LocationCity />
          </Box>
          <Box>{t(`cities:${city}`)}</Box>
        </Box>
      </Typography>
    </MenuItem>
  );
};

export const CityPicker = ({
  value,
  onChange,
  cities,
}: any) => {
  const { t } = useTranslation();
  const classes = useStyles();

  const [showCitiesSearch, setShowCitiesSearch] = useState<boolean>(false);
  const [searchValue, setSearchValue] = useState<string>('');
  const [filteredCities, setFilteredCities] = useState<any[]>([]);

  const cityButtonRef = useRef(null);

  const openCitiesSearch = useCallback(() => {
    setShowCitiesSearch(true);
  }, []);

  const closeCitiesSearch = useCallback(() => {
    setShowCitiesSearch(false);
  }, []);

  const onChangeSearch = useCallback((e) => {
    setSearchValue(e.target.value);
  }, []);

  const onChangeCity = useCallback(
    (city) => () => {
      if (onChange) onChange(city);
      closeCitiesSearch();
    },
    [closeCitiesSearch, onChange],
  );

  useEffect(() => {
    if (!cities.loaded) return;

    setFilteredCities(
      cities.data.filter(({ name: searhName }: any) => searhName.toLowerCase().match(`${searchValue.toLowerCase()}`)),
    );
  }, [cities.loaded, searchValue]);

  return (
    <div>
      <Box>
        <Button
          className={classes.button}
          color="default"
          variant="outlined"
          fullWidth
          startIcon={<LocationCity />}
          ref={cityButtonRef}
          onClick={openCitiesSearch}
        >
          {value || t('common:Select city')}
        </Button>

        <Dialog open={showCitiesSearch} onClose={closeCitiesSearch}>
          <div>
            <Box p={2}>
              <Typography variant="h5">{t('common:Choose city')}</Typography>
              <Box mt={2}>
                <SearchField
                  onChange={onChangeSearch}
                  value={searchValue}
                  placeholder={t('common:City')}
                />
              </Box>
              {cities.loaded ? (
                <>
                  <FixedSizeList
                    height={300}
                    width={350}
                    itemSize={46}
                    itemCount={filteredCities.length}
                  >
                    {renderRow({
                      t,
                      filteredCities,
                      onSelectCity: onChangeCity,
                    })}
                  </FixedSizeList>
                </>
              ) : (
                <LinearProgress />
              )}
            </Box>
          </div>
        </Dialog>
      </Box>
    </div>
  );
};
