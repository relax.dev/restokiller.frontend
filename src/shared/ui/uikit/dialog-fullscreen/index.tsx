import {
  Dialog,
  IconButton,
  Fade,
  Toolbar,
  AppBar,
  Typography,
  Container,
  DialogProps,
} from '@material-ui/core';
import { TransitionProps } from '@material-ui/core/transitions';
import { Close } from '@material-ui/icons';
import { forwardRef, useCallback, useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';

const useStyles = makeStyles((theme: Theme) => createStyles({
  paper: {
    backgroundColor: fade(theme.palette.background.default, 0.8),
    backdropFilter: 'blur(8px)',
    width: '100%',
  },
}));

const Transition = forwardRef((
  // eslint-disable-next-line react/require-default-props
  props: TransitionProps & { children?: React.ReactElement },
  ref: React.Ref<unknown>,
) => <Fade ref={ref} {...props} />);

export interface ITriggerComponentProps {
  onClick: () => void;
}

export interface IFullScreenDialog extends DialogProps {
  title: string;
}

export const FullScreenDialog: React.FC<IFullScreenDialog> = ({
  title,
  children,
  open,
  onClose,
}) => {
  const classes = useStyles();
  return (
    <>
      <Dialog
        fullScreen
        open={open}
        onClose={onClose}
        TransitionComponent={Transition}
        classes={{ paper: classes.paper }}
        keepMounted
      >
        <AppBar position="sticky">
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={(e: any) => onClose && onClose(e, 'escapeKeyDown')}
              aria-label="close"
            >
              <Close />
            </IconButton>
            <Typography variant="h6">{ title }</Typography>
          </Toolbar>
        </AppBar>
        {children}
      </Dialog>
    </>
  );
};
