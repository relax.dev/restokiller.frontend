import { FC, ReactElement } from 'react';
import { Tabs, Tab } from '@material-ui/core';

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export interface ITab {
  label: string;
  icon?: ReactElement
}

export interface ITabsContainer {
  value: number;
  onChange: (event: any, value: number) => void;
  data: ITab[];
  ariaLabel: string;
  color?: 'primary' | 'secondary' | undefined;
  fullWidth?: boolean;
}

export const TabsContainer: FC<ITabsContainer> = ({
  value, onChange, data, ariaLabel, color = 'primary', fullWidth = false,
}) => (
  <Tabs
    indicatorColor={color}
    variant={fullWidth ? 'fullWidth' : 'scrollable'}
    value={value}
    selectionFollowsFocus
    onChange={onChange}
    aria-label={ariaLabel}
  >
    {
        data.map(
          (item: ITab, index: number) => (
            <Tab
              key={item.label}
              icon={item.icon}
              label={item.label}
              fullWidth={fullWidth}
              {...a11yProps(index)}
            />
          ),
        )
      }
  </Tabs>
);
