import {
  Dialog, DialogContent, DialogTitle, DialogContentText, DialogActions,
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { useTranslation } from 'next-i18next';
import { FC } from 'react';
import { Button } from '../button';

const useStyles = makeStyles((theme: Theme) => createStyles({}));

export interface IDialogConfirmation {
  variant: 'delete' | 'confirm' | 'warning',
  open: boolean;
  onClose: () => void;
  onConfirm: () => void;
  loading: boolean;
}

export const DialogConfirmation: FC<IDialogConfirmation> = ({
  variant, open, onClose, onConfirm, loading,
}) => {
  const { t } = useTranslation();

  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{t('common:Attention')}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          { t('common:Are you sure you want to delete the user?')}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="default">
          { t('common:Cancel')}
        </Button>
        <Button onClick={onConfirm} color="secondary" autoFocus loading={loading}>
          { t('common:Remove')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};
