import { ButtonBase, Card, Typography } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Search, Visibility } from '@material-ui/icons';
import Link from 'next/link';
import { FC } from 'react';
import { IBusinessesFilter } from 'shared/api/businesses';
import { string } from 'yup';

const useStyles = makeStyles((theme: Theme) => createStyles({
  card: {
    position: 'relative',
    width: '100%',
    minWidth: 200,
    height: 250,
    transition: '.3s',
    willChange: 'transform',
    '&:hover': {
      transform: 'scale(1.05)',
      zIndex: 1,
    },
  },
  content: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
    boxSizing: 'border-box',
    padding: theme.spacing(2),
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundSize: 'cover',
    filter: 'brightness(0.7)',
    boxShadow: 'inset 0px -80px 40px 6px #0000009e',
  },
  preview: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: '#00000078',
    opacity: 0,
    transition: '.3s opacity',
    willChange: 'opacity',
    cursor: 'pointer',
    '&:hover': {
      opacity: 1,
    },
  },
}));

export interface IPreviewCard {
  name: string;
  description: string;
  image: string;
  filters: IBusinessesFilter,
  key?: string;
}

export const PreviewCard: FC<IPreviewCard> = ({
  name, description, image, filters,
}) => {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <div className={classes.background} style={{ backgroundImage: `url(${image})` }} />
      <div className={classes.content}>
        <Typography variant="h4" component="h3">
          { name }
        </Typography>
        <Typography variant="body1" component="h4">
          { description }
        </Typography>
      </div>
      <Link href={`/collections/${name}`}>
        <ButtonBase className={classes.preview}>
          <Search style={{ fontSize: 80 }} />
        </ButtonBase>
      </Link>
    </Card>
  );
};
