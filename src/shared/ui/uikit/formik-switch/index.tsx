import { FormControlLabel, Switch } from '@material-ui/core';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

export const FormikSwitch = ({
  controlProps,
  id,
  name,
  onBlur,
  ...props
}: any) => {
  const {
    values, handleChange,
  } = useFormikContext<any>();

  return (
    <FormControlLabel
      control={(
        <Switch
          id={id || name}
          checked={get(values, name)}
          onChange={handleChange}
          color="primary"
          {...props}
        />
      )}
      {...controlProps}
    />
  );
};
