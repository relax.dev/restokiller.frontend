import { TextField } from '@material-ui/core';
import { useFormikContext } from 'formik';
import { get } from 'lodash';
import { useCallback } from 'react';

export const FormikTextField = ({
  name,
  id,
  children,
  SelectProps,
  select,
  onBlur,
  ...props
}: any) => {
  const {
    values,
    handleChange,
    touched,
    setFieldValue,
    errors,
    setFieldTouched,
  } = useFormikContext<any>();

  const value = get(values, name);
  const touch = get(touched, name);
  const error = get(errors, name);

  const handleChangeField = useCallback(
    (e) => {
      if (select) {
        setFieldValue(name, e.target.value);
      } else {
        handleChange(e);
      }
    },
    [handleChange, name, select],
  );

  const handleBlurField = useCallback(
    (e) => {
      if (onBlur) onBlur(e);
      setFieldTouched(name, true);
    },
    [name, onBlur, setFieldTouched],
  );

  return (
    <TextField
      {...props}
      SelectProps={SelectProps}
      select={select}
      id={id || name}
      value={value}
      onChange={handleChangeField}
      onBlur={handleBlurField}
      error={touch && Boolean(error)}
      helperText={touch && error}
    >
      {children}
    </TextField>
  );
};
