import {
  Button as MUIButton,
  ButtonProps,
  CircularProgress,
} from '@material-ui/core';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => createStyles({
  buttonProgress: {
    color: theme.palette.getContrastText(theme.palette.primary.main),
  },
}));

interface IButton extends ButtonProps {
  loading?: boolean;
  disabled?: boolean;
  children: React.ReactNode;
}

export const Button: React.FC<IButton> = ({
  children,
  disabled,
  loading,
  ...props
}) => {
  const classes = useStyles();

  return (
    <MUIButton {...props} disabled={loading || disabled}>
      {loading ? (
        <CircularProgress size={24} className={classes.buttonProgress} />
      ) : (
        children
      )}
    </MUIButton>
  );
};
