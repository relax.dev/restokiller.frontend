import { Box, Chip } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { AccessTime } from '@material-ui/icons';
import moment from 'moment';
import { useMemo } from 'react';
import ScrollContainer from 'react-indiana-drag-scroll';

const useStyles = makeStyles((theme: Theme) => createStyles({
  container: { display: 'flex', overflow: 'auto', padding: theme.spacing(2, 0) },
}));

const getDaysBetweenDates = (
  startDate: moment.Moment,
  endDate: moment.Moment,
  interval: { value: number; type: string },
) => {
  const now = startDate.clone();
  const dates = [];

  while (now.isSameOrBefore(endDate)) {
    dates.push(now.clone());
    now.add(interval.value as any, interval.type as any);
  }

  return dates;
};

export const TimePickerCards = () => {
  const classes = useStyles();

  const now = moment(new Date());

  const times = useMemo(() => {
    const startDate = now.clone();
    const endDate = now.clone();

    startDate.set('hours', 16);
    startDate.set('minutes', 0);

    endDate.set('hours', 4);
    endDate.set('minutes', 0);

    endDate.add(1, 'day');

    return getDaysBetweenDates(startDate, endDate, {
      value: 15,
      type: 'minutes',
    });
  }, [now]);

  return (
    <ScrollContainer className={classes.container}>
      {times.map((time) => (
        <Box mr={1} mb={1} key={time.format()}>
          <Chip
            disabled={now.diff(time) > 0}
            onClick={() => {}}
            variant="outlined"
            size="medium"
            label={time.format('HH:mm')}
            color="primary"
            icon={<AccessTime />}
          />
        </Box>
      ))}
    </ScrollContainer>
  );
};
