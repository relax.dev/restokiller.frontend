import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  TextField,
  Dialog,
  Grid,
  Box,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  InputAdornment,
  Typography,
  LinearProgress,
} from '@material-ui/core';
import { useEffect, useState } from 'react';

import usePlacesService from 'react-google-autocomplete/lib/usePlacesAutocompleteService';
import { LocationOn, PinDrop } from '@material-ui/icons';
import { useTranslation } from 'next-i18next';
import { ILocation } from 'entities/business/libs/types';
import { Button } from '../button';
import { GoogleMap } from '../google-map';

const useStyles = makeStyles((theme: Theme) => createStyles({}));

export interface IAddressPicker {
  onChange: (address: string) => void;
  onChangeLocation: (location: string) => void;
  value: string;
  city: string;
  disabled: boolean;
  name: string;
}

export const AddressPicker = ({
  onChange,
  onChangeLocation,
  city,
  value,
  disabled,
  name,
  onBlur,
  error,
  helperText,
  locationError,
  locationHelperText,
}: any) => {
  const [selectedAddressLocation, setSelectedAddressLocation] = useState<null | ILocation>(null);

  const [approved, setApproved] = useState<boolean>(false);

  const { t } = useTranslation();

  const {
    placesService,
    placePredictions,
    getPlacePredictions,
    isPlacePredictionsLoading,
  } = usePlacesService({
    googleMapsScriptBaseUrl: process.env.NEXT_STATIC_GOOGLE_MAPS_API_URL,
    options: {
      types: ['address'],
    },
  });

  useEffect(() => {
    if (value.length <= 0) return;
    getPlacePredictions({ input: `${city} ${value}` });
  }, [value, city]);

  const handleApproveAddress = () => {
    onChangeLocation(selectedAddressLocation);
    setSelectedAddressLocation(null);
    setApproved(true);
  };

  const handleCancelAddress = () => {
    setSelectedAddressLocation(null);
    setApproved(false);
  };

  const handleChangeValue = (event: any) => {
    onChange(event.target.value);
    onChangeLocation(null);
    setApproved(false);
  };

  const handleSelectAddress = (place_id: any) => () => {
    placesService?.getDetails(
      {
        placeId: place_id,
      },
      (placeDetails: any) => {
        const location = {
          lat: placeDetails.geometry.location.lat(),
          lng: placeDetails.geometry.location.lng(),
        };
        onChange(placeDetails.formatted_address);
        setSelectedAddressLocation(location);
      },
    );
  };

  if (!process.browser) return null;

  return (
    <div>
      <TextField
        name={name}
        fullWidth
        autoComplete="off"
        label="Адрес"
        placeholder="Укажите адрес вашего заведения"
        value={value}
        onChange={handleChangeValue}
        disabled={disabled}
        onBlur={onBlur}
        error={error}
        helperText={helperText}
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <PinDrop />
            </InputAdornment>
          ),
        }}
      />

      <Dialog open={Boolean(selectedAddressLocation)} maxWidth="md" fullWidth>
        <Box height={500} maxHeight="90vh">
          { !!selectedAddressLocation
            && <GoogleMap markers={[{ position: selectedAddressLocation }]} />}
        </Box>
        <Box p={2}>
          <Grid container spacing={2}>
            <Grid item xs={6} justify="flex-end" container>
              <Button
                variant="outlined"
                color="default"
                onClick={handleCancelAddress}
              >
                Выбрать другой адрес
              </Button>
            </Grid>
            <Grid item xs={6} justify="flex-start" container>
              <Button
                variant="contained"
                color="primary"
                onClick={handleApproveAddress}
              >
                Подтвердить адрес
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Dialog>

      {!approved && value.length ? (
        isPlacePredictionsLoading ? (
          <LinearProgress />
        ) : (
          <>
            {!!placePredictions.length && (
              <Typography variant="body2" color="textSecondary">
                {locationHelperText}
              </Typography>
            )}
            <List>
              {placePredictions.length
                ? placePredictions.map(({ description, place_id }) => (
                  <ListItem
                    button
                    key={place_id}
                    onClick={handleSelectAddress(place_id)}
                  >
                    <ListItemIcon>
                      <LocationOn />
                    </ListItemIcon>
                    <ListItemText
                      primaryTypographyProps={{ variant: 'body2' }}
                      primary={description}
                    />
                  </ListItem>
                ))
                : t('common:Unknown street')}
            </List>
          </>
        )
      ) : null}
    </div>
  );
};
