import {
  FC, forwardRef, useState, useCallback, ChangeEvent,
  useEffect,
  memo,
} from 'react';
import {
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  InputProps,
  Dialog,
  DialogTitle,
  DialogContent,
  Fade,
  TextField,
  DialogActions,
  Typography,
} from '@material-ui/core';
import { Edit } from '@material-ui/icons';
import { useTranslation } from 'react-i18next';
import { TransitionProps } from '@material-ui/core/transitions';
import { Button } from '../button';

export interface IEditaleText {
  label: string;
  name: string;
  value: string;
  onSave: (name: string, value: string, closeHandler: () => void) => void;
  EditInputProps?: InputProps;
  loading?: boolean;
  disabled?: boolean;
}

const Transition = forwardRef((
  props: TransitionProps,
  ref: React.Ref<unknown>,
) => <Fade ref={ref} {...props} />);

export const EditableText: FC<IEditaleText> = memo(({
  label, name, value, onSave, EditInputProps, loading, disabled,
}) => {
  const [editing, setEditing] = useState<boolean>(false);
  const [tempValue, setTempValue] = useState<string>(value);

  const { t } = useTranslation();

  const openEditing = useCallback(() => {
    setEditing(true);
  }, []);

  const closeEditing = useCallback(() => {
    setEditing(false);
  }, []);

  const onChangeValue = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    setTempValue(e.target?.value);
  }, []);

  useEffect(() => {
    setTempValue(value);
  }, [value]);

  return (
    <>
      <ListItem button onClick={openEditing} ContainerComponent="div" disabled={disabled}>
        <ListItemText primary={label} secondary={value} />
        <ListItemSecondaryAction>
          <IconButton onClick={openEditing} disabled={disabled}>
            <Edit />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
      <Dialog
        id={`edit-text-${name}`}
        TransitionComponent={Transition}
        open={editing}
        onClose={closeEditing}
        maxWidth="sm"
        fullWidth
      >
        <DialogTitle>
          { t('common:Editing')}
        </DialogTitle>

        <DialogContent>
          <TextField
            label={label}
            fullWidth
            value={tempValue}
            InputProps={EditInputProps}
            onChange={onChangeValue}
          />
        </DialogContent>

        <DialogActions>
          <Button fullWidth onClick={() => onSave(name, tempValue, closeEditing)} loading={loading}>
            { t('common:Save') }
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
});
