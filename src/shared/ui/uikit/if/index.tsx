import { FC } from 'react';

export interface IIF {
  condition: boolean;
  children: React.ReactElement,
}

export const If: FC<IIF> = ({ condition, children }) => (condition ? children : null);
