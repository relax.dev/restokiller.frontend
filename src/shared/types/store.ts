export interface FetchableData<T> {
  loaded: boolean;
  error: string | null;
  data: T;
  dirty?: boolean;
}
