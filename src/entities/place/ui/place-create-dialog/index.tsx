import {
  Dialog, Box, Typography, DialogTitle, DialogContent,
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { useTranslation } from 'next-i18next';
import React, {
  FC, forwardRef, useMemo, useState,
} from 'react';
import Slide from '@material-ui/core/Slide';
import { TransitionProps } from '@material-ui/core/transitions';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import { validationSchema } from 'shared/ui/forms/partner/management/place/create/validation.schema';
import { PlaceCreateForm } from 'shared/ui/forms/partner/management/place/create';
import { createPlace } from 'shared/api/places';

const Transition = forwardRef((
  props: TransitionProps,
  ref: React.Ref<unknown>,
) => <Slide direction="up" ref={ref} {...props} />);

const useStyles = makeStyles((theme: Theme) => createStyles({}));

export interface IPlaceCreateDialog {
  open: boolean;
  onClose: (e: any) => void;
  onSuccess: () => void;
  businessId: string;
}

export const PlaceCreateDialog: FC<IPlaceCreateDialog> = ({
  open, onClose, onSuccess, businessId,
}) => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const { enqueueSnackbar } = useSnackbar();

  const formikProps = useMemo(
    () => ({
      initialValues: {
        name: null,
        business: businessId,
      },
      validationSchema: validationSchema({ t }),
      onSubmit: (values: any) => {
        setLoading(true);
        createPlace(values)
          .then(() => {
            onClose({ target: null });
            enqueueSnackbar(t('common:Place created successfully'), { variant: 'success' });
            onSuccess();
          })
          .catch((err) => {
            setError(err.response?.message || null);
          })
          .finally(() => {
            setLoading(false);
          });
      },
    }),
    [t, enqueueSnackbar, businessId],
  );

  return (
    <Dialog id="create-place" TransitionComponent={Transition} open={open} onClose={onClose} maxWidth="sm" fullWidth>
      <DialogTitle>
        { t('common:New place') }
      </DialogTitle>

      <DialogContent>
        <Formik {...formikProps}>
          <PlaceCreateForm error={error} loading={loading} />
        </Formik>
      </DialogContent>
    </Dialog>
  );
};
