import { IPlace } from '../../place/libs/types';

export interface ITableBooking {
  startDate: string,
  endDate: string,
  id: string,
}

export interface ITable {
  id: string;
  seats: number;
  photos: string[];
  place: IPlace;
  number: number;
  bookings: ITableBooking[];
}
