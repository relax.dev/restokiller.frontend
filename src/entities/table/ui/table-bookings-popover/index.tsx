import { FC } from 'react';
import {
  Popover, Box, Typography, Chip, Grid,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { AccessTime } from '@material-ui/icons';
import moment from 'moment';
import { formatToHours } from 'shared/helpers/time';
import { ITable } from '../../libs/types';

export interface ITableBookingsPopover {
  data: ITable;
  anchorEl: HTMLButtonElement | null;
  onClose: () => void;
}

export const TableBookingsPopover:FC<ITableBookingsPopover> = ({
  data,
  anchorEl,
  onClose,
}) => {
  const { t } = useTranslation();
  return (
    <Popover id={`popover-bookings-${data.id}`} anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={onClose}>
      <Box p={2}>
        <Typography gutterBottom variant="h6">{ t('common:Bookings after you')}</Typography>
        <Grid container spacing={1}>
          {
                data.bookings.length ? data.bookings.map((booking) => (
                  <Grid item>
                    <Chip
                      icon={<AccessTime />}
                      color="secondary"
                      label={`${formatToHours(booking.startDate)} - ${formatToHours(booking.endDate)} (${moment(booking.startDate).format('dddd')} - ${moment(booking.endDate).format('dddd')})`}
                    />
                  </Grid>
                )) : <Typography>Позже выбранного времени нет бронирований</Typography>
            }
        </Grid>
      </Box>
    </Popover>
  );
};
