import { Typography } from '@material-ui/core';
import { green, red } from '@material-ui/core/colors';
import {
  createStyles,
  fade,
  makeStyles,
  Theme,
} from '@material-ui/core/styles';
import { useCallback, useState, FC } from 'react';

import moment from 'moment';
import { Button } from 'shared/ui/uikit/button';
import { TableBookingsPopover } from '../table-bookings-popover';
import { ITable } from '../../libs/types';

const useStyles = makeStyles((theme: Theme) => createStyles({
  wrapper: {
    borderRadius: 8,
    display: 'flex',
    alignItems: 'center',
  },
  point: {
    width: 10,
    height: 10,
    borderRadius: '50%',
    backgroundColor: green[500],
    margin: theme.spacing(0, 0, 0, 1),
  },
  occupied: {
    backgroundColor: red[500],
  },
}));

export interface ITableStatus {
  data: ITable;
}

export const TableStatus: FC<ITableStatus> = ({ data }) => {
  const classes = useStyles();

  const [bookingsAnchorEl, setBookingsAnchorEl] = useState<HTMLButtonElement | null>(null);

  const onShowBookings = useCallback((event: React.MouseEvent<HTMLButtonElement>) => {
    setBookingsAnchorEl(event.currentTarget);
  }, []);

  const onHideBookings = useCallback(() => {
    setBookingsAnchorEl(null);
  }, []);

  const currentBookings = data.bookings.find(
    (booking) => moment(new Date()).isBetween(moment(booking.startDate), moment(booking.endDate), 's'),
  );

  const isFree = !currentBookings;

  return (
    <>
      <div className={classes.wrapper}>
        <Button variant="text" color="inherit" onClick={onShowBookings}>

          { isFree ? (
            <>
              <Typography>Свободен</Typography>
              <div className={classes.point} />
            </>
          ) : (
            <>
              <Typography>Сейчас занят</Typography>
              <div className={`${classes.point} ${classes.occupied}`} />
            </>
          ) }

        </Button>

      </div>

      <TableBookingsPopover
        data={data}
        anchorEl={bookingsAnchorEl}
        onClose={onHideBookings}
      />
    </>
  );
};
