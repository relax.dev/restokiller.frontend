import {
  Dialog, Box, Typography, DialogTitle, DialogContent,
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { useTranslation } from 'next-i18next';
import React, {
  FC, forwardRef, useMemo, useState,
} from 'react';
import Slide from '@material-ui/core/Slide';
import { TransitionProps } from '@material-ui/core/transitions';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import { TableCreateForm } from 'shared/ui/forms/partner/management/table/create';
import { validationSchema } from 'shared/ui/forms/partner/management/table/create/validation.schema';
import { createTable } from 'shared/api/tables';
import { IPlace } from '../../../place/libs/types';

const Transition = forwardRef((
  props: TransitionProps,
  ref: React.Ref<unknown>,
) => <Slide direction="up" ref={ref} {...props} />);

const useStyles = makeStyles((theme: Theme) => createStyles({}));

export interface ITableCreateDialog {
  open: boolean;
  onClose: (e: any) => void;
  onSuccess: () => void;
  place: IPlace;

}

export const TableCreateDialog: FC<ITableCreateDialog> = ({
  open, onClose, place, onSuccess,
}) => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const { enqueueSnackbar } = useSnackbar();
  const formikProps = useMemo(
    () => ({
      initialValues: {
        seats: null,
        place: place.id,
        photos: [],
      },
      validationSchema: validationSchema({ t }),
      onSubmit: (values: any) => {
        setLoading(true);
        createTable(values)
          .then(() => {
            onClose({ target: null });
            enqueueSnackbar(t('common:Table created successfully'), { variant: 'success' });
            onSuccess();
          })
          .catch((err) => {
            setError(err.response?.message || null);
          })
          .finally(() => {
            setLoading(false);
          });
      },
    }),
    [t, enqueueSnackbar, onClose],
  );

  return (
    <Dialog id={`create-table-${place.id}`} TransitionComponent={Transition} open={open} onClose={onClose} maxWidth="sm" fullWidth>
      <DialogTitle>
        { t('common:New table at {{ place }}', { place: place.name })}
      </DialogTitle>

      <DialogContent>
        <Formik {...formikProps}>
          <TableCreateForm error={error} loading={loading} />
        </Formik>
      </DialogContent>

    </Dialog>
  );
};
