import {
  useEffect, FC, useCallback, useState, ReactNode,
} from 'react';

import {
  Tooltip, Typography, Box, Card, CardContent, CardMedia, Grid, IconButton,
} from '@material-ui/core';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Image from 'next/image';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Delete, ErrorOutline, Person,
} from '@material-ui/icons';
import clsx from 'clsx';
import { Button } from 'shared/ui/uikit/button';
import { formatToHours } from 'shared/helpers/time';
import { TableStatus } from '../table-status';
import { ITable } from '../../libs/types';

export interface ITableCard {
  data: ITable;
  isManagement?: boolean;
  button: {
    label: string,
    callback: (data: ITable) => void;
    disabled?: boolean;
    icon?: ReactNode
  }
}

const useStyles = makeStyles((theme: Theme) => createStyles({
  media: {
    height: '100%',
    position: 'absolute',
    filter: 'brightness(0.3)',
    borderRadius: '8px',
  },
  root: {
    height: 250,
    minWidth: 320,
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    overflow: 'visible',
    '&:hover': {
      '& $deleteButton': {
        display: 'block',
      },
    },
  },
  content: {
    position: 'relative',
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
  },
  status: {
    position: 'absolute',
    top: theme.spacing(1),
    right: theme.spacing(1),
  },
  warning: {
    display: 'flex',
    alignItems: 'center',
  },
  warningIcon: {
    color: theme.palette.warning.main,
    marginRight: theme.spacing(1),
  },
  deleteButton: {
    position: 'absolute',
    border: '2px solid',
    borderColor: theme.palette.error.main,
    top: -15,
    right: -15,
    display: 'none',
  },
}));

export const TableCard: FC<ITableCard> = ({ data, button, isManagement = false }) => {
  const classes = useStyles();
  const { t } = useTranslation();

  console.log(data.photos);

  return (
    <Card className={classes.root} elevation={6}>
      <CardMedia
        component="img"
        alt="Restourant preview"
        height="140"
        image={data.photos?.[0] || 'https://museum-design.ru/wp-content/uploads/brunch-table-01.jpg'}
        title={data.number?.toString()}
        className={classes.media}
      />
      <CardContent className={classes.content}>
        <Grid container alignItems="flex-start" justify="space-between">
          <Grid item>
            <Typography gutterBottom variant="h5">
              { t('common:Table №{{number}}', { number: data.number }) }
            </Typography>
          </Grid>
          <Grid item>
            <TableStatus data={data} />

          </Grid>
        </Grid>

        { Boolean(data.bookings.length) && (
        <Typography gutterBottom variant="body2" color="textSecondary" className={classes.warning}>
          <ErrorOutline className={classes.warningIcon} />

          { `Будет занят с ${formatToHours(data.bookings[0].startDate)} до ${formatToHours(data.bookings[0].endDate)}` }

        </Typography>
        ) }

        <Box flexGrow={1} />

        <Grid container spacing={2} alignItems="center">
          <Grid item container justify="flex-start" xs={6} md={4} alignItems="center">
            <Tooltip arrow title={t('common:Seats number - {{count}}', { count: data.seats }) || ''}>
              <Box display="flex">
                <Person style={{ fontSize: 30, marginRight: 2 }} />
                <Typography variant="h6" color="textPrimary">
                  { data.seats }
                </Typography>
              </Box>

            </Tooltip>
          </Grid>
          <Grid item container justify="flex-end" xs={6} md={8}>
            <Button
              startIcon={button.icon}
              disabled={button.disabled}
              onClick={() => button.callback(data)}
            >
              { button.label }
            </Button>
          </Grid>
        </Grid>
        { isManagement && (
        <IconButton size="small" className={clsx(classes.deleteButton)}>
          <Delete color="error" />
        </IconButton>
        ) }
      </CardContent>
    </Card>
  );
};
