import { Box, Grid, LinearProgress } from '@material-ui/core';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { Data } from '@react-google-maps/api';
import { useState, useCallback, FC } from 'react';

import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { fetchAllTables } from 'features/business/business.slice';
import { ITablesFilters } from 'shared/api/tables';
import { BookingTableDialog } from 'features/business/ui/booking-table-dialog';
import { ErrorContainer } from 'shared/ui/uikit/error-container';
import { LoaderBottom } from 'shared/ui/uikit/loader-bottom';
import { NoContentContainer } from 'shared/ui/uikit/no-content-container';
import { Book } from '@material-ui/icons';
import { LoaderFullScreen } from 'shared/ui/uikit/loader-fullscreen';
import { useRouter } from 'next/router';
import { ITable } from '../../libs/types';
import { TableCard } from '../table-card';

const useStyles = makeStyles((theme: Theme) => createStyles({}));

interface ITablesCardsContainer {
  tables: {
    loaded: boolean;
    error: string | null;
    data: ITable[];
    dirty?: boolean;
  };
  filters: ITablesFilters;
}

export const TablesCardsContainer: FC<ITablesCardsContainer> = ({ tables, filters }) => {
  const { t } = useTranslation();

  const [selected, setSelected] = useState<string>('');
  const dispatch = useDispatch();

  const router = useRouter();

  const onOpenBooking = useCallback((data: ITable) => {
    setSelected(data.id);
  }, []);

  const onCloseBooking = useCallback(() => {
    router.push({
      pathname: '/profile',
      query: {
        section: 'history',
      },
    });
  }, [filters]);

  const isCanBooking = filters.startDate && filters.endDate && filters.seats;

  if (tables.error) {
    return <ErrorContainer />;
  }

  if (!tables.dirty) {
    return <LoaderFullScreen />;
  }

  return (
    <>
      { !tables.loaded && <LoaderBottom /> }

      {
        !tables.data.length && (
        <Box display="flex" justifyContent="center">
          <NoContentContainer />
        </Box>
        )
      }

      <Grid container spacing={4}>
        {
        tables.data.map((table: any, index: number) => (
          <Grid item key={table?.id || index} xs={12} md={6} lg={6}>
            <TableCard
              data={table}
              button={{
                label: t('common:Booking'),
                callback: onOpenBooking,
                disabled: !isCanBooking,
                icon: <Book />,
              }}
            />
          </Grid>
        ))
      }
      </Grid>

      <BookingTableDialog
        filters={filters}
        open={!!selected}
        onClose={onCloseBooking}
        tableId={selected}
      />
    </>
  );
};
