export enum BusinessRole { // need to sync with server
  HOSTES = 10,
  ADMINISTRATOR = 20,
  MANAGER = 30,
  OWNER = 999,
}
export interface IUser {
  id: string;
  email: string,
  firstName: string,
  lastName: string,
  phone: string,
  photo: string,
}

export interface IWorker {
  id: string;
  role: BusinessRole
}
