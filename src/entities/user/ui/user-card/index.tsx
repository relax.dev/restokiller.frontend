import { BusinessRole, IUser } from 'entities/user/libs/types';
import { FC } from 'react';
import {
  Card, Avatar, Typography, CardActions, Grid, Box,
} from '@material-ui/core';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { Button } from 'shared/ui/uikit/button';
import clsx from 'clsx';
import { AccountCircle, HighlightOff } from '@material-ui/icons';
import { useTranslation } from 'next-i18next';

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  avatar: {
    width: theme.spacing(13),
    height: theme.spacing(13),
    marginBottom: theme.spacing(1),
  },
}));

export interface IUserCard {
  user: IUser,
  role: BusinessRole,
  onDelete?: () => void;
}

export const UserCard: FC<IUserCard> = ({ user, role, onDelete }) => {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <Card elevation={3}>
      <Box className={classes.root} p={2}>
        <Avatar
          alt="Profile avatar"
          src={user.photo}
          className={clsx(classes.avatar)}
        />

        <Typography variant="h5">
          {`${user.firstName} ${user.lastName}`}
        </Typography>

        <Typography gutterBottom variant="body1" color="primary">
          <b>{ t(`roles:${BusinessRole[role]}`) }</b>
        </Typography>
      </Box>

      <CardActions>
        <Grid container spacing={1}>
          <Grid item xs={6}>
            <Button startIcon={<AccountCircle />} fullWidth size="small" color="primary">Профиль</Button>
          </Grid>
          <Grid item xs={6}>
            <Button disabled={!onDelete} startIcon={<HighlightOff />} fullWidth size="small" color="secondary" onClick={onDelete}>Исключить</Button>
          </Grid>
        </Grid>
      </CardActions>

    </Card>
  );
};
