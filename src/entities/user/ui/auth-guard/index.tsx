import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useSession } from 'shared/hooks/useSession';

export const AuthGuard = ({ children }: any) => {
  const router = useRouter();
  const { isAuth } = useSession();

  useEffect(() => {
    if (!isAuth) {
      router.push('/login').catch(() => {
        console.log('Router redirect failed');
      });
    }
  }, [isAuth]);

  if (typeof window === undefined) {
    return null;
  }

  if (!isAuth) {
    return null;
  }

  return children;
};
