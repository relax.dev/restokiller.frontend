import {
  IconButton, Menu, MenuItem, Button, Avatar,
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import { AccountCircle, AssignmentInd } from '@material-ui/icons';
import { useState, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import Link from 'next/link';
import { useSession } from 'shared/hooks/useSession';
import { useProfile } from 'shared/hooks/useProfile';

const useStyles = makeStyles((theme: Theme) => createStyles({
  profilePhoto: {
    width: '32px',
    height: '32px',
  },
}));

export const HeaderProfile = ({ children }: any) => {
  const classes = useStyles();
  const { logout, isAuth } = useSession();
  const { t } = useTranslation();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const open = !!anchorEl;

  const { profile } = useProfile();

  const handleOpenMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setAnchorEl(null);
  };

  const handleLogout = useCallback(() => {
    handleCloseMenu();
    logout();
  }, []);

  if (!isAuth) {
    return (
      <Link href="/login" passHref>
        <Button startIcon={<AssignmentInd />} variant="text" color="default">
          {t('common:Log In')}
        </Button>
      </Link>
    );
  }

  if (!profile.dirty) {
    return <div>loading...</div>;
  }

  return (
    <>
      <IconButton
        aria-label="account of current user"
        aria-controls="menu-appbar"
        aria-haspopup="true"
        onClick={handleOpenMenu}
        color="inherit"
      >
        <Avatar className={classes.profilePhoto} src={profile.data?.photo} />
      </IconButton>
      <Menu
        id="menu-appbar"
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={open}
        onClose={handleCloseMenu}
      >
        <Link href="/profile">
          <MenuItem>{t('common:Profile')}</MenuItem>
        </Link>
        <MenuItem onClick={handleLogout}>{t('common:Log Out')}</MenuItem>
      </Menu>
    </>
  );
};
