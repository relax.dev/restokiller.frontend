import { IBusiness } from 'entities/business/libs/types';
import { ITable } from 'entities/table/libs/types';

export interface IBooking {
  id: string;
  startDate: string;
  endDate: string;
  status: string;
  table: ITable;
  email: string;
  description: string;
  persons: number;
  user: {
    firstName: string;
    photo: string;
  },
}

export interface IBookingInHistory extends IBooking {
  business: IBusiness,
}

export interface IBusinessBooking extends IBooking {
  data?: number;
}

export interface IBookingHistoryGroup {
  bookings: IBookingInHistory[],
  date: string,
}
