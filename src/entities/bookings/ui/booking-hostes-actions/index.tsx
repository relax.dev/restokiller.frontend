import { useTranslation } from 'react-i18next';
import {
  Typography, Grid, Box, IconButton, Popover, Container,
} from '@material-ui/core';
import { Button } from 'shared/ui/uikit/button';
import {
  FC, useState, useCallback, useRef,
} from 'react';
import { IBusinessBooking } from 'entities/bookings/libs/types';
import {
  AssignmentTurnedIn, Block, QueryBuilder, ExitToApp, MoreHoriz, Phone, SwapHoriz,
} from '@material-ui/icons';

export interface IBookingHostesActions {
  booking: IBusinessBooking
  onCancelBooking: (id: string) => void;
  onApproveBooking: (id: string) => void;
  onDoneBooking: (id: string) => void
}

export const BookingHostesActions: FC<IBookingHostesActions> = ({
  booking, onCancelBooking, onApproveBooking, onDoneBooking,
}) => {
  const { t } = useTranslation();

  const [openedActions, setOpenedActions] = useState(false);

  const buttonRef = useRef(null);

  const handleOpenActions = useCallback(() => {
    setOpenedActions(true);
  }, []);

  const handleCloseActions = useCallback(() => {
    setOpenedActions(false);
  }, []);

  return (
    <>
      <Popover
        id={`actions-menu-${booking.id}`}
        anchorEl={buttonRef.current}
        keepMounted
        elevation={12}
        open={openedActions}
        onClose={handleCloseActions}
        BackdropProps={{
          invisible: false,
        }}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
      >
        <Container disableGutters maxWidth="sm">
          <Box m={2}>
            <Typography gutterBottom variant="h4">{t('common:Choose action')}</Typography>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Button startIcon={<Phone />} color="default" fullWidth>Позвонить</Button>
              </Grid>
              <Grid item xs={6}>
                <Button startIcon={<AssignmentTurnedIn />} color="primary" fullWidth onClick={() => onApproveBooking(booking.id)}>Гость пришел</Button>
              </Grid>

              <Grid item xs={6}>
                <Button startIcon={<QueryBuilder />} color="default" fullWidth>Продлить</Button>
              </Grid>

              <Grid item xs={6}>
                <Button startIcon={<ExitToApp />} color="secondary" fullWidth onClick={() => onDoneBooking(booking.id)}>Гость ушел</Button>
              </Grid>

              <Grid item xs={6}>
                <Button startIcon={<SwapHoriz />} color="default" fullWidth>Сменить столик</Button>
              </Grid>

              <Grid item xs={6}>
                <Button startIcon={<Block />} color="secondary" fullWidth onClick={() => onCancelBooking(booking.id)}>Отменить бронь</Button>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </Popover>

      <Grid container justify="flex-end">
        <Grid item>
          <IconButton onClick={handleOpenActions}>
            <MoreHoriz ref={buttonRef} />
          </IconButton>
        </Grid>
      </Grid>
    </>
  );
};
