import { IBooking, IBookingHistoryGroup } from 'entities/bookings/libs/types';
import { FC } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Avatar, List, ListItem, ListItemAvatar, ListItemText, Box, Grid, Typography, ListSubheader, Paper,
} from '@material-ui/core';
import moment from 'moment';
import { localizedMoment } from 'shared/helpers/time';
import { CalendarToday } from '@material-ui/icons';
import { useMobile } from 'shared/hooks/useMobile';
import { BookingCard } from '../booking-card';

export interface IBookingsHistory {
  bookingsHistory: IBookingHistoryGroup[];
}

export interface IBookingsHistoryUseStyles {
  isMobile: boolean;
}

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    position: 'relative',
    overflow: 'auto',
    maxHeight: 500,
    padding: 0,
  },
  listSection: {
    backgroundColor: 'inherit',
  },
  ul: {
    backgroundColor: 'inherit',
    padding: 0,
    marginBottom: theme.spacing(2),
  },
  subheader: {
    color: '#fff',
    fontSize: '32px',
    backgroundColor: theme.palette.primary.main,
  },

  group: {
    position: 'relative',
    marginBottom: theme.spacing(8),
  },
  sticky: {
    position: 'sticky',
    top: ({ isMobile }: IBookingsHistoryUseStyles) => theme.spacing(isMobile ? 6 : 10),
    alignSelf: 'flex-start',
    zIndex: 1,
  },
  list: {
    flexGrow: 1,
  },
  date: {
    display: 'flex',
    backgroundColor: theme.palette.primary.main,
    backdropFilter: ({ isMobile }: IBookingsHistoryUseStyles) => (isMobile ? 'blur(4px)' : 'none'),
    borderRadius: ({ isMobile }: IBookingsHistoryUseStyles) => (isMobile ? 2 : 8),
  },
  dateText: {
    display: 'flex',
    alignItems: 'center',
  },
  month: {
    marginLeft: theme.spacing(0.5),
  },
}));

export const BookingsHistory: FC<IBookingsHistory> = ({ bookingsHistory }) => {
  const { isMobile } = useMobile();
  const classes = useStyles({ isMobile });

  return (
    <div>
      {
        bookingsHistory.map((group) => (
          <Grid container className={classes.group} spacing={2} justify="center">
            <Grid item xs={12} className={classes.sticky} md={2}>
              <Paper className={classes.date} elevation={0}>
                <Box p={1}>
                  <CalendarToday />
                </Box>
                <Box className={classes.dateText}>
                  <Typography variant="body1">
                    {localizedMoment(group.date).format('DD')}
                  </Typography>
                  <Typography variant="body2" color="textSecondary" className={classes.month}>
                    {localizedMoment(group.date).format('MMMM')}
                  </Typography>
                </Box>
              </Paper>
            </Grid>
            <Grid item xs={12} md={10}>
              <div className={classes.list}>
                <Grid container spacing={2}>
                  {
                  group.bookings.map((booking) => (
                    <Grid key={booking.id} item xs={12} md={6} lg={4}>
                      <BookingCard booking={booking} />
                    </Grid>
                  ))
                }
                </Grid>
              </div>
            </Grid>

          </Grid>
        ))
      }
    </div>
  );
};
