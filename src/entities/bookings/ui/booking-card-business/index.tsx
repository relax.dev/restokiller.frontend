import { useTranslation } from 'react-i18next';
import {
  CardContent,
  CardHeader, CardActions, Card, Typography, Grid, Box,
} from '@material-ui/core';
import { Button } from 'shared/ui/uikit/button';
import {
  FC, useState, useCallback, useRef,
} from 'react';
import { IBusinessBooking } from 'entities/bookings/libs/types';
import { BookingStatus } from '../booking-status';
import { BookingHostesActions } from '../booking-hostes-actions';

export interface IBookingCardBusiness {
  booking: IBusinessBooking
  onCancelBooking: (id: string) => void;
  onApproveBooking: (id: string) => void;
  onDoneBooking: (id: string) => void
}

export const BookingCardBusiness: FC<IBookingCardBusiness> = ({
  booking, onCancelBooking, onApproveBooking, onDoneBooking,
}) => {
  const { t } = useTranslation();

  const [openedActions, setOpenedActions] = useState(false);

  const buttonRef = useRef(null);

  const handleOpenActions = useCallback(() => {
    setOpenedActions(true);
  }, []);

  const handleCloseActions = useCallback(() => {
    setOpenedActions(false);
  }, []);

  return (
    <>
      <Card elevation={6}>
        <Box p={1}>
          <CardHeader
            titleTypographyProps={{
              variant: 'h4',
            }}
            title={`Table №${booking.table.number}`}
            subheader={booking.table.place.name}
            action={(
              <Box m={0}>
                <BookingStatus booking={booking} />
              </Box>
            )}
          />
          <CardContent>
            <Grid container>
              <Grid item xs={12}>
                <Typography variant="body1" color="textSecondary" component="p">
                  <span>
                    Гость -
                    {' '}
                    { booking.user.firstName }
                    {' '}
                  </span>
                  <b>
                    (
                    { `${booking.persons} чел. `}
                    )
                  </b>
                </Typography>
                <Typography gutterBottom variant="body1" color="textSecondary" component="p">
                  { booking.email}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  <b>Гость оставил сообщение:</b>
                  {' '}
                  { booking.description}
                </Typography>
              </Grid>
            </Grid>

          </CardContent>
          <CardActions>
            <BookingHostesActions
              booking={booking}
              onCancelBooking={onCancelBooking}
              onApproveBooking={onApproveBooking}
              onDoneBooking={onDoneBooking}
            />
          </CardActions>
        </Box>
      </Card>
    </>
  );
};
