import { useTranslation } from 'react-i18next';
import {
  CardHeader, CardActions, Card, CardMedia, Avatar, IconButton,
} from '@material-ui/core';
import {
  FC,
} from 'react';
import { IBookingInHistory } from 'entities/bookings/libs/types';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  MoreVert, Replay,
} from '@material-ui/icons';
import { Button } from 'shared/ui/uikit/button';
import Link from 'next/link';

export interface IBookingCard {
  booking: IBookingInHistory
}

const useStyles = makeStyles((theme: Theme) => createStyles({
  media: {
    height: 150,
  },
}));

export const BookingCard: FC<IBookingCard> = ({
  booking,
}) => {
  const { t } = useTranslation();
  const classes = useStyles();

  return (
    <>
      <Card elevation={6}>
        <CardHeader
          avatar={(
            <Avatar aria-label="Business photo" src={booking.business.photos[0]}>
              B
            </Avatar>
          )}
          subheader={booking.business.name}
          title={`Столик №${booking.table.number}`}
          action={(
            <IconButton disabled aria-label="settings">
              <MoreVert />
            </IconButton>
          )}
        />
        <CardMedia
          className={classes.media}
          image={booking.table.photos[0]}
          title="Table photo"
        />
        <CardActions>
          <Link
            href={`/business/${booking.business.id}`}
          >
            <Button
              startIcon={<Replay />}
              variant="text"
            >
              {t('common:Booking again')}
            </Button>
          </Link>
        </CardActions>
      </Card>
    </>
  );
};
