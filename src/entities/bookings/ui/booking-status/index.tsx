import { IBooking } from 'entities/bookings/libs/types';
import { FC } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import clsx from 'clsx';
import moment from 'moment';
import { Typography, Box } from '@material-ui/core';
import { BookingStatusEnum } from 'shared/api/bookings';

export interface IBookingStatus {
  booking: IBooking;
}

const useStyles = makeStyles((theme: Theme) => createStyles({
  status: {
    padding: theme.spacing(2),
    textAlign: 'center',
    borderRadius: 8,
    fontWeight: 'bold',
  },
  pending: {
    background: '#fca130',
  },
  cenceled: {
    background: theme.palette.error.main,
  },
  done: {
    background: theme.palette.success.main,
  },
  approved: {
    background: '#161d24',
  },
}));

export const BookingStatus: FC<IBookingStatus> = ({ booking }) => {
  const classes = useStyles();

  return (
    <div className={clsx(classes.status, {
      [classes.pending]: booking.status === BookingStatusEnum.PENDING,
      [classes.cenceled]: booking.status === BookingStatusEnum.CANCELED,
      [classes.approved]: booking.status === BookingStatusEnum.APPROVED,
      [classes.done]: booking.status === BookingStatusEnum.DONE,
    })}
    >
      <Typography variant="h6">
        {
          booking.status
        }
      </Typography>
      <Box>
        {
          moment(booking.startDate).format('HH:mm')
        }
        { ' - '}
        {
          moment(booking.endDate).format('HH:mm')
        }
      </Box>

    </div>
  );
};
