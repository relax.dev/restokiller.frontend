import { IWorker } from 'entities/user/libs/types';
import { IBusinessFilters } from 'features/businesses/businesses.slice';

export interface IOwner {
  firstName: string;
  lastName: string;
  photo: string | null;
}

export interface IPrice {
  currency: string;
  value: number;
}

export interface ILocation {
  lat: number;
  lng: number;
}

export interface IPurpose {
  name: string;
}

export interface IFeature {
  name: string;
}

export interface IKitchen {
  name: string;
}

export interface ICollection {
  name: string;
  description: string;
  filters: IBusinessFilters;
  image: string;
}
export interface ICategory {
  name: string;
  collections: ICollection[];
}

export interface IBusiness {
  name: string;
  id: string;
  type: number;
  description: string;
  workingHours: IWorkingHours;
  address: string;
  city: string;
  distance: number;
  features: string[];
  kitchen: IKitchen[];
  location: ILocation;
  photos: string[];
  headerPhoto: string;
  purposes: IPurpose[];
  averageCheck: IPrice;
  isVerified: boolean;
  users: IWorker[];
  owner: string;
}

export interface IWorkingHour {
  active: boolean;
  start: string;
  end: string;
}

export enum IDay {
  monday = 'monday',
  tuesday = 'tuesday',
  wednesday = 'wednesday',
  thursday = 'thursday',
  friday = 'friday',
  saturday = 'saturday',
  sunday = 'sunday',
}

export type IWorkingHours = Record<IDay, IWorkingHour>;
