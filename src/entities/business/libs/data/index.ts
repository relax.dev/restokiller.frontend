import { IBusiness } from '../types';

export const defaultWorkingHours = {
  monday: {
    active: false,
    start: '00:00',
    end: '00:00',
  },
  tuesday: {
    active: false,
    start: '00:00',
    end: '00:00',
  },
  wednesday: {
    active: false,
    start: '00:00',
    end: '00:00',
  },
  thursday: {
    active: false,
    start: '00:00',
    end: '00:00',
  },
  friday: {
    active: false,
    start: '00:00',
    end: '00:00',
  },
  saturday: {
    active: false,
    start: '00:00',
    end: '00:00',
  },
  sunday: {
    active: false,
    start: '00:00',
    end: '00:00',
  },
};

export const defaultBusinessData: IBusiness = {
  name: '',
  id: '',
  type: 0,
  owner: '',
  description: '',
  photos: [],
  address: '',
  workingHours: defaultWorkingHours,
  features: [],
  purposes: [],
  kitchen: [],
  city: '',
  headerPhoto: '',
  location: {
    lat: 0,
    lng: 0,
  },
  distance: 0,
  averageCheck: {
    value: 0,
    currency: 'RUB',
  },
  isVerified: false,
  users: [],
};
