import {
  CardMedia,
  Container,
  Card,
  Typography,
  Box,
  Grid,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@material-ui/core';
import { Rating } from '@material-ui/lab';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  ExpandMore, InfoOutlined, LocationCity, Phone,
} from '@material-ui/icons';
import { FC } from 'react';
import Image from 'next/image';
import { useTranslation } from 'react-i18next';
import { RootState } from 'shared/modules/store';
import { getCurrentDay } from 'shared/helpers/time';
import { IBusiness, IDay } from '../../libs/types';

const useStyles = makeStyles((theme: Theme) => createStyles({
  relative: {
    position: 'relative',
  },

  container: {
    position: 'absolute',
    textAlign: 'center',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    backdropFilter: 'blur(4px) brightness(50%)',
  },
}));

interface IBusinessMediaHeader {
  business: {
    loaded: boolean;
    error: string | null;
    data: IBusiness;
  };
}

export const BusinessMediaHeader: FC<IBusinessMediaHeader> = ({ business }) => {
  const classes = useStyles();

  const currentDay: IDay = getCurrentDay();

  const startHour = business.data.workingHours?.[currentDay]?.start;
  const endHour = business.data.workingHours?.[currentDay]?.end;

  const { t } = useTranslation();

  return (
    <>
      <div className={classes.relative}>
        <CardMedia
          component="img"
          image={business.data.headerPhoto || 'http://sun9-61.userapi.com/c850628/v850628167/b47a5/WIR4TiBoK4g.jpg'}
          height="250"
        />
        <div className={classes.container}>
          <Container maxWidth="md">
            <Typography gutterBottom variant="h3">
              {business.loaded ? business.data.name : 'loading'}
            </Typography>
            <Typography variant="body2">
              Время работы:
              {' '}
              {business.loaded ? `${startHour} - ${endHour}` : 'loading'}
            </Typography>
          </Container>
        </div>
      </div>
      {/* <Container maxWidth="md">
        <Box mt={4}>
          <Grid container spacing={2}>
            <Grid item xs={12} md={7}>
              <Typography gutterBottom>
                {business.loaded ? business.data.description : 'loading'}
              </Typography>
              <Rating value={2} readOnly />
            </Grid>
            <Grid item xs={12} md={5}>
              <Card>
                <List>
                  <ListItem>
                    <ListItemIcon>
                      <LocationCity />
                    </ListItemIcon>
                    <ListItemText primary={business.data.address} />
                  </ListItem>
                  <ListItem button component="a" href="tel://+79297660409">
                    <ListItemIcon>
                      <Phone />
                    </ListItemIcon>
                    <ListItemText primary="+89297660409" />
                  </ListItem>
                </List>
              </Card>
            </Grid>
          </Grid>

        </Box>
      </Container> */}

    </>
  );
};
