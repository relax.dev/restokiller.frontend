import { Box, Grid } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { FC } from 'react';
import { ErrorContainer } from 'shared/ui/uikit/error-container';

import { LoaderBottom } from 'shared/ui/uikit/loader-bottom';
import { NoContentContainer } from 'shared/ui/uikit/no-content-container';
import { LoaderFullScreen } from 'shared/ui/uikit/loader-fullscreen';
import { BusinessCard } from '../business-card';
import { IBusiness } from '../../libs/types';

export interface IBusinessesGrid {
  businesses: {
    loaded: boolean;
    error: string | null;
    data: IBusiness[];
    dirty?: boolean;
  };
  isManagement?: boolean;
}

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
}));

export const BusinessesGrid: FC<IBusinessesGrid> = ({ businesses, isManagement = false }) => {
  const classes = useStyles();

  if (businesses.error) {
    return <ErrorContainer />;
  }

  if (!businesses.dirty) {
    return <LoaderFullScreen />;
  }

  return (
    <>
      { !businesses.loaded && <LoaderBottom /> }

      {
        !businesses.data.length && (
        <Box display="flex" justifyContent="center">
          <NoContentContainer />
        </Box>
        )
      }

      <Grid container spacing={4}>
        {businesses.data.map((business) => (
          <Grid item key={business.id} xs={12} md={12} lg={4} xl={4}>
            <BusinessCard business={business} isManagement={isManagement} />
          </Grid>
        ))}
      </Grid>
    </>
  );
};
