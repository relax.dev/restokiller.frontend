import {
  Box,
  ButtonBase,
  Card,
  CardActionArea,
  CardActions,
  Typography,
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Map } from '@material-ui/icons';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Button } from 'shared/ui/uikit/button';

const useStyles = makeStyles((theme: Theme) => createStyles({
  content: {
    height: 200,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backdropFilter: 'brightness(0.8)',
    overflow: 'hidden',
    position: 'relative',
  },
  background: {
    backgroundImage: 'url(https://sun9-8.userapi.com/impg/j0UW-AYncvJN6UfKeuR-4BAAnZ3axZX4n_n_Pw/3IRC4X7Kakc.jpg?size=790x464&quality=96&sign=af344aeb08aa8a07feb98cb41eb926b2&type=album)',
    backgroundSize: '100%',
    backgroundPosition: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    transition: '1s',
    '&:hover': {
      backgroundSize: '125%',
    },
  },
}));

interface IBusinessesMapButton {
  onClick?: () => void;
}

export const BusinessesMapButton: FC<IBusinessesMapButton> = ({ onClick }) => {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <Card className={classes.content} elevation={6}>
      <div className={classes.background}>
        <Box mb={2}>
          <Button startIcon={<Map />} onClick={onClick}>
            {t('common:Show on map')}
          </Button>
        </Box>
      </div>
    </Card>
  );
};
