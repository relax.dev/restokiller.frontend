import {
  Card,
  Typography,
  Box,
} from '@material-ui/core';

import React, {
  FC, useCallback, useState,
  memo,
} from 'react';
import { useMobile } from 'shared/hooks/useMobile';
import { Rating } from 'shared/ui/uikit/rating';
import clsx from 'clsx';
import { If } from 'shared/ui/uikit/if';
import { useStyles } from './styles';
import { OverloadingStatus } from './overloading-status';
import { IBusiness } from '../../libs/types';
import { FeaturesList } from './features-list';
import { PreviewPhotos } from './preview-photos';
import { BusinessCardActionsDefault } from './actions-default';
import { BusinessCardActionsManagement } from './actions-management';

export interface IBusinessCard {
  business: IBusiness;
  isManagement?: boolean;
}

export interface OccupiedTable {
  table: string;
}

export interface IWorkload {
  occupiedTables: OccupiedTable[],
  tablesCount: number,
}

export const BusinessCard: FC<IBusinessCard> = memo(({ business, isManagement = false }) => {
  const { isMobile } = useMobile();

  const classes = useStyles({ isMobile });
  const [openedBooking, setOpenedBooking] = useState(false);

  const handleOpenActions = useCallback((e) => {
    e.stopPropagation();
    setOpenedBooking(true);
  }, []);

  const handleCloseActions = useCallback((e) => {
    e.stopPropagation();
    setOpenedBooking(false);
  }, []);

  return (
    <Card
      elevation={12}
      className={clsx(classes.root, {
        [classes.selectedCard]: openedBooking,
      })}
      onClick={openedBooking ? handleCloseActions : handleOpenActions}
      onTouchCancel={openedBooking ? handleCloseActions : handleOpenActions}
      onMouseLeave={handleCloseActions}
    >
      <PreviewPhotos photos={business.photos} />

      <Box position="relative">
        <div className={classes.heading}>
          <Typography
            variant="h4"
            component="h2"
            className={classes.title}
          >
            { business.name }
          </Typography>

          <Box ml={1}>
            <Rating name="name" readOnly value={4} />
          </Box>
        </div>

        <Box className={classes.footer}>
          <FeaturesList features={business.features} />
          <Box ml={1}>
            <OverloadingStatus
              businessId={business.id}
            />
          </Box>
        </Box>

        <Box
          className={clsx(classes.bookingButtonContainer, {
            [classes.openedButtonContainer]: openedBooking,
          })}
        >
          <Box className={classes.bookingTitle}>
            <Typography className={classes.title} variant="h5" gutterBottom>
              «
              {business.name}
              »
            </Typography>
          </Box>

          <If condition={!isManagement}>
            <BusinessCardActionsDefault businessId={business.id} />
          </If>

          <If condition={isManagement}>
            <BusinessCardActionsManagement businessId={business.id} />
          </If>
        </Box>
      </Box>

    </Card>
  );
});
