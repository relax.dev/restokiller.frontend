import {
  createStyles, makeStyles, Theme,
} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    width: '100%',
    height: '100%',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    willChange: 'transform',
    transition: '.3s transform',
    cursor: 'pointer',
    '@media (hover: hover) and (pointer: fine)': {
      '&:hover': {
        transform: 'scale(1.04)',
      },
    },
  },
  selectedCard: {
    transform: 'scale(1.04)',
    cursor: 'pointer',
  },
  heading: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(2),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  footer: {
    margin: `${theme.spacing(2)}px ${theme.spacing(2)}px ${theme.spacing(3)}px ${theme.spacing(2)}px`,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  card: {
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    zIndex: 1,
  },
  title: {
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
  },
  bookingButtonContainer: {
    position: 'absolute',
    zIndex: 0,
    transition: '.3s',
    bottom: '-100%',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    left: 0,
    width: '100%',
    height: '100%',
    background: '#151a21a6',
    backdropFilter: 'blur(4px)',
    flexDirection: 'column',
    textAlign: 'center',
    '&:before': {
      content: '""',
      top: -12,
      height: 4,
      width: '30%',
      background: '#fff',
      borderRadius: 5,
      position: 'absolute',
      left: 'calc(50% - 15%)',
      zIndex: 2,
    },
  },
  openedButtonContainer: {
    bottom: 0,
  },
  bookingTitle: {
    margin: `0 ${theme.spacing(2)}px`,
    overflow: 'hidden',
    position: 'relative',
    maxWidth: '90%',
  },
}));
