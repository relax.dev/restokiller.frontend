import {
  FC,
  memo,
} from 'react';

import {
  Settings,
} from '@material-ui/icons';

import { useTranslation } from 'next-i18next';
import { Button } from 'shared/ui/uikit/button';
import Link from 'next/link';

export interface IBusinessCardActionsManagement {
  businessId: string,
}

export const BusinessCardActionsManagement: FC<IBusinessCardActionsManagement> = memo(
  ({ businessId }) => {
    const { t } = useTranslation();

    return (
      <>
        <Link
          href={{
            pathname: '/partner/management/businesses/[id]',
            query: { id: businessId, section: 'common' },
          }}
        >
          <Button
            size="large"
            color="primary"
            startIcon={<Settings />}
          >
            { t('common:Settings')}
          </Button>
        </Link>
      </>
    );
  },
);
