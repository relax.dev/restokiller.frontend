import {
  createStyles, makeStyles, Theme,
} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => createStyles({
  image: {
    objectFit: 'cover',
    display: 'block',
    width: '100%',
    height: '100%',
  },
  slide: {
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  swiper: {
    height: 250,
    borderRadius: '8px 8px 0 0',
  },
}));
