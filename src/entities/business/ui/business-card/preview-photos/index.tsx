import { Box } from '@material-ui/core';
import { memo, FC } from 'react';

import { Swiper, SwiperSlide } from 'swiper/react';
import { useStyles } from './styles';

export interface IPreviewPhotos {
  photos: string[],
}

export const PreviewPhotos: FC<IPreviewPhotos> = memo(({ photos }) => {
  const classes = useStyles();
  return (
    <Box>
      <Swiper
        pagination
        height={250}
        speed={600}
        className={classes.swiper}
        loop
      >
        { photos.map((url) => (
          <SwiperSlide key={url} className={classes.slide}>
            <img alt="url" src={url} className={classes.image} />
          </SwiperSlide>
        ))}
      </Swiper>
    </Box>
  );
});
