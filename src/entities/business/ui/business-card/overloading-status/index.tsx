import {
  Typography,
  Box,
  CircularProgressProps,
  CircularProgress,
  IconButton,
  Popover,
} from '@material-ui/core';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { HelpOutline } from '@material-ui/icons';
import {
  useState,
  useEffect,
  useMemo,
} from 'react';
import { getWorkload } from 'shared/api/businesses';
import { localizedMoment } from 'shared/helpers/time';
import { IWorkload } from '../index';

const useStylesFacebook = makeStyles((theme: Theme) => createStyles({
  root: ({ size }: { size: number }) => ({
    position: 'relative',
    height: size,
  }),
  bottom: {
    color: theme.palette.grey[700],
  },
  top: {
    color: '#51ff27',
    animationDuration: '550ms',
    position: 'absolute',
    left: 0,
  },
  circle: {
    strokeLinecap: 'round',
  },
  popover: {
    pointerEvents: 'none',
  },
  paper: {
    padding: theme.spacing(1),
  },
}));

export interface IOverloadingStatus extends CircularProgressProps {
  businessId: string
}

export function OverloadingStatus({ businessId, ...props }: IOverloadingStatus) {
  const size = 48;

  const [workload, setWorkload] = useState<IWorkload>({
    occupiedTables: [],
    tablesCount: 0,
  });
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<null | string>(null);

  const classes = useStylesFacebook({ size });
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const handlePopoverOpen = (
    event: React.MouseEvent<HTMLElement, MouseEvent>,
  ) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  const now = localizedMoment();
  const endTime = now.clone();
  endTime.add(1, 'hours');

  useEffect(() => {
    setLoading(true);
    getWorkload({
      id: businessId,
      filters: {
        startDate: now.toDate(),
        endDate: endTime.toDate(),
      },
    }).then((response: any) => {
      setWorkload(response.data);
    }).catch(() => {
      setError('error');
    }).finally(() => {
      setLoading(false);
    });
  }, [businessId]);

  const workloadPercent = useMemo(
    () => (workload.occupiedTables.length / (workload.tablesCount || 1)) * 100,
    [workload],
  );

  const freeTables = useMemo(
    () => (workload.tablesCount || 1) - workload.occupiedTables.length,
    [workload],
  );

  return (
    <Box className={classes.root}>
      <CircularProgress
        variant="determinate"
        className={classes.bottom}
        size={size}
        thickness={4}
        {...props}
        value={100}
      />
      <CircularProgress
        variant="determinate"
        className={classes.top}
        classes={{
          circle: classes.circle,
        }}
        size={size}
        thickness={4}
        value={workloadPercent}
        {...props}
      />

      <Box
        width={size}
        height={size}
        position="absolute"
        display="flex"
        justifyContent="center"
        alignItems="center"
        top={0}
        left={0}
        aria-owns={open ? 'mouse-over-popover' : undefined}
        aria-haspopup="true"
        onMouseEnter={handlePopoverOpen}
        onMouseLeave={handlePopoverClose}
      >
        <IconButton>
          <HelpOutline />
        </IconButton>
        <Popover
          id="popover-overloading"
          className={classes.popover}
          classes={{
            paper: classes.paper,
          }}
          open={open}
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'left',
          }}
          onClose={handlePopoverClose}
          disableRestoreFocus
        >
          <Typography>
            {`Загруженность ${workloadPercent}%. Осталось ${freeTables} свободных столика.`}
          </Typography>
        </Popover>
      </Box>
    </Box>
  );
}
