import {
  FC,
  memo,
  useCallback,
} from 'react';

import {
  AccountBox,
  Search,
} from '@material-ui/icons';

import { useTranslation } from 'next-i18next';
import { Button } from 'shared/ui/uikit/button';
import { useSession } from 'shared/hooks/useSession';
import { If } from 'shared/ui/uikit/if';
import Link from 'next/link';

import { useRouter } from 'next/router';

export interface IBusinessCardActionsDefault {
  businessId: string,
}

export const BusinessCardActionsDefault: FC<IBusinessCardActionsDefault> = memo(
  ({ businessId }) => {
    const { isAuth, handleOpenLoginPopup, handleCloseLoginPopup } = useSession();

    const { t } = useTranslation();
    const router = useRouter();

    const onSuccessLogin = () => {
      handleCloseLoginPopup();

      router.push({
        pathname: '/business/[id]',
        query: { id: businessId },
      });
    };

    const handleAuth = useCallback(() => {
      handleOpenLoginPopup({
        onSuccessLogin,
      });
    }, [businessId]);

    return (
      <>
        <If condition={isAuth}>
          <Link
            href={{
              pathname: '/business/[id]',
              query: { id: businessId },
            }}
          >

            <Button startIcon={<Search />} variant="contained">
              Выбрать столик
            </Button>
          </Link>
        </If>
        <If condition={!isAuth}>
          <Button startIcon={<AccountBox />} variant="contained" onClick={handleAuth}>
            Авторизироваться
          </Button>
        </If>
      </>
    );
  },
);
