import { Box, Chip } from '@material-ui/core';
import { HelpOutline } from '@material-ui/icons';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';

export interface IFeaturesList {
  features: string[];
}

export const FeaturesList: FC<IFeaturesList> = ({ features }) => {
  const { t } = useTranslation();

  return (
    <Box position="relative" display="flex" overflow="auto">
      {
            features?.length ? features?.map((feature: string) => (
              <Box mr={1} key={feature}>
                <Chip
                  size="medium"
                  color="primary"
                  label={t(`features:${feature}`)}
                  onDelete={() => {}}
                  deleteIcon={<HelpOutline />}
                />
              </Box>
            )) : (
              <Chip
                size="medium"
                color="primary"
                label={t('features:Good place')}
                onDelete={() => {}}
                deleteIcon={<HelpOutline />}
              />
            )
          }
    </Box>
  );
};
