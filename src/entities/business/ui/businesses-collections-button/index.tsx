import {
  Box,
  ButtonBase,
  Card,
  CardActionArea,
  CardActions,
  Typography,
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { CollectionsBookmark } from '@material-ui/icons';
import Link from 'next/link';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Button } from 'shared/ui/uikit/button';

const useStyles = makeStyles((theme: Theme) => createStyles({
  content: {
    height: 200,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backdropFilter: 'brightness(0.4)',
    overflow: 'hidden',
    position: 'relative',
  },
  background: {
    backgroundImage: 'url(https://static6.depositphotos.com/1014511/575/i/600/depositphotos_5755752-stock-photo-healthy-eating.jpg)',
    backgroundSize: '100%',
    backgroundPosition: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    transition: '1s',
    '&:hover': {
      backgroundSize: '125%',
    },
  },
}));

interface IBusinessesCollectionsButton {
  variant?: string;
}

export const BusinessesCollectionsButton: FC<IBusinessesCollectionsButton> = ({ variant }) => {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <Link href="/collections">
      <Card className={classes.content} elevation={6}>
        <div className={classes.background}>
          <Box mb={2}>
            <Button startIcon={<CollectionsBookmark />}>
              {t('common:Collections')}
            </Button>
          </Box>
        </div>
      </Card>
    </Link>
  );
};
