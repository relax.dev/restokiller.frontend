const nextRuntimeDotenv = require('next-runtime-dotenv');
const dotenvLoad = require('dotenv-load');
const { i18n } = require('./next-i18next.config');

dotenvLoad();

const withConfig = nextRuntimeDotenv({
  // path: '.env',
  public: [
    'API_URL',
  ],
  server: [
    'API_URL',
  ],
});

module.exports = withConfig({
  i18n,
  publicRuntimeConfig: {
    API_URL: process.env.API_URL,
  },
  webpack(config, { dev }) {
    if (dev) {
      config.devtool = 'cheap-module-source-map';
    }

    config.module.rules.push({
      test: /\.svg$/,
      issuer: {
        test: /\.(js|ts)x?$/,
      },
      use: ['@svgr/webpack'],
    });

    return config;
  },
});
