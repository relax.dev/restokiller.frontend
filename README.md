# Roadmap
[https://relax-dev.atlassian.net/](https://relax-dev.atlassian.net/jira/software/projects/RES/boards/1/roadmap)

# Enviroment
.env file:

```
API_URL = http://localhost:8080
NEXT_STATIC_COOKIE_TOKEN_NAME = token
```

# Docker:
Build container:

```bash
docker login $REGISTRY -u $USER -p $TOKEN
docker build --no-cache --build-arg API_URL=$API_URL -t $REGISTRY/$PROJECT_NAME ../
```

# Start
Run server:

```bash
yarn dev
```

#  w/o 💩
- Imports sequence:
  1) libs
  2) @material-ui 
  3) shared
  4) features
  5) entities
